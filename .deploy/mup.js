// production
module.exports = {
  servers: {
    one: {
      // TODO: set host address, username, and authentication method
      host: '000.000.000.000',
      username: 'root',
      // pem: './path/to/pem'
      password: 'password'
      // or neither for authenticate from ssh-agent
    }
  },

  meteor: {
    // TODO: change app name and path
    name: 'art',
    path: '../.',

    servers: {
      one: {},
    },

    buildOptions: {
      serverOnly: true,
    },

    env: {
      // TODO: Change to your app's url
      // If you are using ssl, it needs to start with https://
      ROOT_URL: 'http://000.000.000.000',
      MONGO_URL: 'mongodb://localhost/sam',
      //PORT: 9090
    },

    docker: {
      // change to 'kadirahq/meteord' if your app is not using Meteor 1.4
      image: 'abernix/meteord:node-8.15.1-base',
      // image: 'abernix/meteord:base',
      // imagePort: 90
      // imagePort: 80, // (default: 80, some images EXPOSE different ports)
        // 'RUN docker run --name=art-testing --net=bridge --restart=always -p 9090:80 -e MONGO_URL=mongodb://172.17.0.2:27017/art-testing -d mup-art:latest'
    },
    deployCheckWaitTime: 60,
    enableUploadProgressBar: true
  },

  mongo: {
    port: 27017,
    version: '4.0.4',
    servers: {
      one: {}
    }
  }
};