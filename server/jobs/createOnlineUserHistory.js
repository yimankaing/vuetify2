import {Meteor} from 'meteor/meteor';

SyncedCron.add({
  name: 'core_createOnlineUserHistory',
  schedule: function (parser) {
    // parser is a later.parse object
    // return parser.text('every 1 minute');
    if (Meteor.isProduction)
      return parser.text('at 10:30 am'); //5:30pm GMT +7
    return parser.text('at 07:00 am');
    // return parser.text('every 1 minute');

  },
  job: function () {
    Meteor.call('core_insertOnlineUserHistory');
  }
});