import {SAM_StudentHelper} from "../../imports/collections/SAM/student";

Meteor.publish('sam_studentHelper', function () {
  if (this.userId) {
    return SAM_StudentHelper.find({});
  }
  return this.ready();
});
