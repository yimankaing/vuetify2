import {Meteor} from 'meteor/meteor';
//lang
import {t} from "../../../imports/libs/constants";
import _ from 'lodash';
import ValidateUser from '../../../imports/libs/validate-user';
import {SAM_Student, SAM_StudentHelper, SAM_StudentLogs} from "../../../imports/collections/SAM/student";
import {CheckRoles} from "../../../imports/libs/checkRoles";
import {collectionHelper} from "../../../imports/libs/collectionHelper";
import moment from "moment";

Meteor.methods({
  sam_findStudent({userId, q = "", selector = {}, filter = "", options = {limit: 10, skip: 0}, module}) {
    ValidateUser.ifUserNotSignedIn({msg: 'Must be logged in to query student'});
    let currentUser = Meteor.users.findOne({_id: userId});
    let language = 'en';
    if (currentUser) {
      language = currentUser.profile.language;
    }
    let roles = CheckRoles({userId, roles: ["super", "admin", "read"], module});
    if (!roles) {
      throw new Meteor.Error(`${t[language].unauthenticated}! ${t[language].contactAdmin}`);
    }

    //sort
    let sort = {gId: 1};
    if (!_.isEmpty(selector.sort)) {
      sort = selector.sort;
    }

    let text = '';
    if (!!q) {
      text = q.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
    }
    let reg = new RegExp(text, 'i');

    let data = {
      content: [],
      count: 0,
    };

    let qSelector = {};
    //filtering
    if (!!filter) {
      qSelector[filter] = {$regex: reg};
    } else {
      qSelector.$or = [
        {name: {$regex: reg}},
      ];
    }

    let levelSelector = {};
    if (!!selector.province) {
      levelSelector['provinceCode'] = selector.province;
    }
    if (!!selector.site) {
      levelSelector['siteCode'] = selector.site;
    }

    let students = SAM_Student.aggregate([
      {$match: qSelector},
      {$sort: sort},
      {$skip: options.skip},
      {$limit: options.limit},
    ]);
    if (students.length > 0) {
      data.content = students;
      data.count = SAM_Student.aggregate([
        {$match: qSelector},
        {$count: 'total'}
      ])[0].total;
    }
    return data;
  },
  sam_findOneStudent({userId, selector = {}, module}) {
    ValidateUser.ifUserNotSignedIn({msg: 'Must be logged in to query student'});
    let currentUser = Meteor.users.findOne({_id: userId});
    let language = 'en';
    if (currentUser) {
      language = currentUser.profile.language;
    }
    let roles = CheckRoles({userId, roles: ["super", "admin", "read"], module});
    if (!roles) {
      throw new Meteor.Error(`${t[language].unauthenticated}! ${t[language].contactAdmin}`);
    }
    return SAM_Student.findOne(selector);
  },
  sam_insertStudent({userId, doc = {}, module}) {
    ValidateUser.ifUserNotSignedIn({msg: 'Must be logged in to insert student'});
    let currentUser = Meteor.users.findOne({_id: userId});
    let language = 'en';
    if (currentUser) {
      language = currentUser.profile.language;
    }
    let roles = CheckRoles({userId, roles: ["super", "admin", "create"], module});
    if (!roles) {
      throw new Meteor.Error(`${t[language].unauthenticated}! ${t[language].contactAdmin}`);
    }
    let errorMessage = '';

    return {
      _id: SAM_Student.insert(doc, (err, res) => {
        if (!err)
          return collectionHelper(SAM_StudentHelper, res);
        throw new Meteor.Error(err.message);
      }),
      errorMessage
    };
  },
  sam_updateStudent({userId, selector = {}, updateDoc = {}, module}) {
    ValidateUser.ifUserNotSignedIn({msg: 'Must be logged in to update student'});
    let currentUser = Meteor.users.findOne({_id: userId});
    let language = 'en';
    if (currentUser) {
      language = currentUser.profile.language;
    }
    let roles = CheckRoles({userId, roles: ["super", "admin", "edit"], module});
    if (!roles) {
      throw new Meteor.Error(`${t[language].unauthenticated}! ${t[language].contactAdmin}`);
    }
    let errorMessage = '';

    return {
      _id: SAM_Student.update(selector, {$set: updateDoc}, {}, (err, res) => {
        if (!err)
          return collectionHelper(SAM_StudentHelper, selector._id);
        throw new Meteor.Error(err.message);
      }),
      errorMessage
    };
  },
  sam_removeStudent({userId, selector, module}) {
    ValidateUser.ifUserNotSignedIn({msg: 'Must be logged in to remove student'});
    let currentUser = Meteor.users.findOne({_id: userId});
    let language = 'en';
    if (currentUser) {
      language = currentUser.profile.language;
    }
    let roles = CheckRoles({userId, roles: ["super", "admin", "delete"], module});
    if (!roles) {
      throw new Meteor.Error(`${t[language].unauthenticated}! ${t[language].contactAdmin}`);
    }
    return SAM_Student.remove(selector, (err, res) => {
      if (!err)
        return collectionHelper(SAM_StudentHelper, selector._id);
      throw new Meteor.Error(err.message);
    });
  },
  sam_studentIsUsed({userId, selector = {}, options = {limit: 10, skip: 0}, module}) {
    ValidateUser.ifUserNotSignedIn({msg: 'Must be logged in to remove student'});
    let currentUser = Meteor.users.findOne({_id: userId});
    let language = 'en';
    if (currentUser) {
      language = currentUser.profile.language;
    }
    let roles = CheckRoles({userId, roles: ["super", "admin", "read"], module});
    if (!roles) {
      throw new Meteor.Error(`${t[language].unauthenticated}! ${t[language].contactAdmin}`);
    }

    return false;
  },
  sam_removeStudentLogs() {
    // SAM_StudentLogs.remove({
    //   tranDate: {
    //     $lte: moment().subtract(1, 'months').startOf('day').toDate()
    //   }
    // });
  }
});
