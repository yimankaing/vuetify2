import {Meteor} from 'meteor/meteor';
//lang
import {t} from "../../../imports/libs/constants";
import ValidateUser from '../../../imports/libs/validate-user';
import {SAM_Province} from "../../../imports/collections/SAM/province";
import {CheckRoles} from "../../../imports/libs/checkRoles";

Meteor.methods({
  sam_findProvince({userId, q = "", selector = {}, filter = "", options = {limit: 10, skip: 0}, module}) {
    ValidateUser.ifUserNotSignedIn({msg: 'Must be logged in to query province'});
    let currentUser = Meteor.users.findOne({_id: userId});
    let language = 'en';
    if (currentUser) {
      language = currentUser.profile.language;
    }
    let roles = CheckRoles({userId, roles: ["super", "admin", "read"], module});
    if (!roles) {
      throw new Meteor.Error(`${t[language].unauthenticated}! ${t[language].contactAdmin}`);
    }

    //sort
    let sort = {code: 1};
    if (!!selector.sort && selector.sort.length > 0) {
      sort = {[selector.sort[1]]: selector.sort[0] === 'descending' ? -1 : 1}
    }

    let text = '';
    if (!!q) {
      text = q.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
    }
    let reg = new RegExp(text, 'i');

    let data = {
      content: [],
      count: 0,
    };

    let qSelector = {};
    //filtering
    if (!!filter) {
      qSelector[filter] = {$regex: reg};
    } else {
      qSelector.$or = [
        {name: {$regex: reg}},
        {id: {$regex: reg}},
        {_id: {$regex: reg}},
      ];
    }

    let provinces = SAM_Province.find(qSelector, {
      sort: sort,
      skip: options.skip,
      limit: options.limit
    }).fetch();
    if (provinces.length > 0) {
      data.content = provinces;
      data.count = SAM_Province.find({}).count();
    }
    return data;
  },
});
