import {Meteor} from 'meteor/meteor';
import moment from 'moment';
import ValidateUser from '../../../../imports/libs/validate-user';
import {CheckRoles} from "../../../../imports/libs/checkRoles";
import {t} from "../../../../imports/libs/constants";
import {SAM_Student} from "../../../../imports/collections/SAM/student";
import {status, patientStatus} from "../../../../imports/rawData/selectOpt";
import {provinces, sites} from "../../../../imports/rawData";

Meteor.methods({
  sam_studentStatusReport({userId, params = {}, module}) {
    ValidateUser.ifUserNotSignedIn({msg: 'Must be logged in to view report'});
    let currentUser = Meteor.users.findOne({_id: userId});
    let language = 'en';
    if (currentUser) {
      language = currentUser.profile.language;
    }
    let roles = CheckRoles({userId, roles: ["super", "admin", "report"], module});
    if (!roles) {
      throw new Meteor.Error(`${t[language].unauthenticated}! ${t[language].contactAdmin}`);
    }

    let rptTitle, rptHeader, rptContent, rptFooter;
    rptTitle = {};//App_setup.findOne({});
    rptHeader = params;

    rptHeader.text = "national";

    let selector = {};
    const fDate = moment(params.fDate).startOf('day').toDate();
    const tDate = moment(params.tDate).endOf('day').toDate();

    selector['data.statusDate'] = {$gte: fDate, $lte: tDate};

    let levelSelector = {};
    if (!!params.province) {
      levelSelector['provinceCode'] = params.province;
      rptHeader.text = params.province;
    }
    if (!!params.site) {
      levelSelector['siteCode'] = params.site;
      rptHeader.text = params.site;
    }

    rptContent = SAM_Student.aggregate([])[0];

    return {rptTitle, rptHeader, rptContent};
  },
});

// const moduleFormatter = (m) => {
//   if (!!m) {
//     if (m.length === 2) {
//       return provinces.filter((o => o.code === m))[0].name;
//     } else if (m.length === 4) {
//       return sites.filter((o => o.code === m))[0].name;
//     }
//     return m.toUpperCase();
//   }
// };