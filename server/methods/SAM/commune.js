import {Meteor} from 'meteor/meteor';
//lang
import {t} from "../../../imports/libs/constants";
import ValidateUser from '../../../imports/libs/validate-user';
import {SAM_Commune} from "../../../imports/collections/SAM/commune";
import {CheckRoles} from "../../../imports/libs/checkRoles";

Meteor.methods({
  sam_findCommune({userId, q = "", selector = {}, filter = "", options = {limit: 10, skip: 0}, module}) {
    ValidateUser.ifUserNotSignedIn({msg: 'Must be logged in to query commune'});
    let currentUser = Meteor.users.findOne({_id: userId});
    let language = 'en';
    if (currentUser) {
      language = currentUser.profile.language;
    }
    let roles = CheckRoles({userId, roles: ["super", "admin", "read"], module});
    if (!roles) {
      throw new Meteor.Error(`${t[language].unauthenticated}! ${t[language].contactAdmin}`);
    }

    //sort
    let sort = {code: 1};
    if (!!selector.sort && selector.sort.length > 0) {
      sort = {[selector.sort[1]]: selector.sort[0] === 'descending' ? -1 : 1}
    }

    let text = '';
    if (!!q) {
      text = q.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
    }
    let reg = new RegExp(text, 'i');

    let data = {
      content: [],
      count: 0,
    };

    let qSelector = {};
    //filtering
    if (!!filter) {
      qSelector[filter] = {$regex: reg};
    } else {
      qSelector.$or = [
        {name: {$regex: reg}},
        {id: {$regex: reg}},
        {_id: {$regex: reg}},
      ];
    }

    let communes = SAM_Commune.aggregate([
      {$match: selector},
      {$match: qSelector},
      {$sort: sort},
      {$skip: options.skip},
      {$limit: options.limit},
    ]);
    if (communes.length > 0) {
      data.content = communes;
      data.count = SAM_Commune.find(selector).count();
    }
    return data;
  },
});
