import {Meteor} from 'meteor/meteor';
import {DDPRateLimiter} from 'meteor/ddp-rate-limiter';

const LISTS_METHODS = [
  //user
  'core_findUser',
  'core_insertUser',
  'core_updateUser',
  'core_removeUser',
  'core_userIsUsed',
  //student
  'sam_findStudent',
  'sam_findOneStudent',
  'sam_insertStudent',
  'sam_updateStudent',
  'sam_removeStudent',
  'sam_studentIsUsed',
  //address
  'sam_findProvince',
  'sam_findDistrict',
  'sam_findCommune',
  'sam_findVillage',
  //report
  //dashboard
];

if (Meteor.isServer) {
  DDPRateLimiter.addRule({
    name(name) {
      return LISTS_METHODS.includes(name);
    },
    // Rate limit per connection ID
    connectionId() {
      return true;
    }
  }, 5, 1000);
  DDPRateLimiter.setErrorMessage('Too many requests. Please wait for a second then try again');
}