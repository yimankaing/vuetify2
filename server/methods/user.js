import {Meteor} from 'meteor/meteor';
import {Accounts} from "meteor/accounts-base";
//lang
import {t} from '../../imports/libs/constants'
import _ from 'lodash';
import ValidateUser from '../../imports/libs/validate-user';
import {CheckRoles} from "../../imports/libs/checkRoles";
import {SAM_Student} from "../../imports/collections/SAM/student";
import {CORE_OnlineUserHistory} from "../../imports/collections/onlineUserHistory";
import moment from "moment";

Meteor.methods({
  core_findUser({userId, q = "", selector = {}, filter = "", options = {limit: 10, skip: 0}, module}) {
    ValidateUser.ifUserNotSignedIn({msg: 'Must be logged in to query user'});
    let currentUser = Meteor.users.findOne({_id: userId});
    let language = 'en';
    if (currentUser) {
      language = currentUser.profile.language;
    }
    let roles = CheckRoles({userId, roles: ["super", "admin"], module});
    if (!roles) {
      throw new Meteor.Error(`${t[language].unauthenticated}! ${t[language].contactAdmin}`);
    }

    //sort
    let sort = {username: 1};
    if (!_.isEmpty(selector.sort)) {
      sort = selector.sort;
    }

    let text = '';
    if (!!q) {
      text = q.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
    }
    let reg = new RegExp(text, 'i');

    let data = {
      content: [],
      count: 0,
    };

    let qSelector = {};
    //filtering
    if (!!filter) {
      qSelector[filter] = {$regex: reg};
    } else {
      qSelector.$or = [
        {_id: {$regex: reg}},
        {username: {$regex: reg}},
      ];
    }

    data.content = Meteor.users.find(qSelector, {sort, limit: options.limit, skip: options.skip}).fetch();
    data.count = Meteor.users.find(qSelector).count();
    return data;
  },
  core_insertUser({userId, doc = {}, module}) {
    ValidateUser.ifUserNotSignedIn({msg: 'Must be logged in to insert user'});
    let currentUser = Meteor.users.findOne({_id: userId});
    let language = 'en';
    if (currentUser) {
      language = currentUser.profile.language;
    }
    let roles = CheckRoles({userId, roles: ["super", "admin"], module});
    if (!roles) {
      throw new Meteor.Error(`${t[language].unauthenticated}! ${t[language].contactAdmin}`);
    }
    try {
      let id = Accounts.createUser({
        username: doc.username,
        email: doc.email,
        password: doc.password,
        profile: doc.profile,
        roles: doc.roles
      });

      for (let key in doc.roles) {
        if (doc.roles.hasOwnProperty(key)) {
          Roles.addUsersToRoles(id, doc.roles[key], key);
        }
      }
    } catch (e) {
      console.log(e);
      throw new Meteor.Error(e.message);
    }
  },
  core_updateUser({userId, selector = {}, updateDoc = {}, module}) {
    ValidateUser.ifUserNotSignedIn({msg: 'Must be logged in to update user'});
    let currentUser = Meteor.users.findOne({_id: userId});
    let language = 'en';
    if (currentUser) {
      language = currentUser.profile.language;
    }
    let roles = CheckRoles({userId, roles: ["super", "admin", "read"], module});
    if (!roles) {
      throw new Meteor.Error(`${t[language].unauthenticated}! ${t[language].contactAdmin}`);
    }
    let userUpdateDoc = Object.assign({}, updateDoc);
    delete userUpdateDoc.password;
    try {
      Meteor.users.update(
        selector,
        {$set: userUpdateDoc}
      );
      // Update password
      !!updateDoc.password ? Accounts.setPassword(selector._id, updateDoc.password) : null;
      // Add roles
      if (!!updateDoc.roles) {
        for (let key in updateDoc.roles) {
          if (updateDoc.roles.hasOwnProperty(key)) {
            Roles.addUsersToRoles(updateDoc._id, updateDoc.roles[key], key);
          }
        }
      }
    } catch (e) {
      console.log(e);
      throw new Meteor.Error(e.message);
    }
  },
  core_removeUser({userId, selector, module}) {
    ValidateUser.ifUserNotSignedIn({msg: 'Must be logged in to remove user'});
    let currentUser = Meteor.users.findOne({_id: userId});
    let language = 'en';
    if (currentUser) {
      language = currentUser.profile.language;
    }
    let roles = CheckRoles({userId, roles: ["super", "admin"], module});
    if (!roles) {
      throw new Meteor.Error(`${t[language].unauthenticated}! ${t[language].contactAdmin}`);
    }
    if (Meteor.users.findOne(selector).username === 'super') {
      throw new Meteor.Error('អ្នកមិនអាចលុប គណនីនេះបានទេ');
    }
    try {
      return Meteor.users.remove(selector);
    } catch (e) {
      console.log(e);
      throw new Meteor.Error(e.message);
    }
  },
  core_userIsUsed({userId, selector = {}, options = {limit: 10, skip: 0}, module}) {
    ValidateUser.ifUserNotSignedIn({msg: 'Must be logged in to remove user'});
    let currentUser = Meteor.users.findOne({_id: userId});
    let language = 'en';
    if (currentUser) {
      language = currentUser.profile.language;
    }
    let roles = CheckRoles({userId, roles: ["super", "admin", "read"], module});
    if (!roles) {
      throw new Meteor.Error(`${t[language].unauthenticated}! ${t[language].contactAdmin}`);
    }

    return SAM_Student.find({
      $or: [
        {createdUser: {$eq: selector.userId}},
        {updatedUser: {$eq: selector.userId}},
        {deletedUserId: {$eq: selector.userId}},
        {coordinatorUser: {$eq: selector.userId}},
      ]
    }).count() > 0;
  },
  core_insertOnlineUserHistory() {
    let onlineUsers = Meteor.users.find({
      'status.lastLogin.date': {
        $gte: moment().startOf('day').toDate(),
        $lte: moment().endOf('day').toDate()
      }
    }).fetch();

    if (!!onlineUsers && onlineUsers.length > 0) {
      let users = onlineUsers.map(o => {
        return {
          userId: o._id,
          username: o.username,
          lastLogin: o.status.lastLogin, //string.split("(")[1].split(")")[0];
        }
      });

      CORE_OnlineUserHistory.insert({users});
    }
  }
});
