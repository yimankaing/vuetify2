import 'meteor/matb33:collection-hooks';

import {GeneralFunction} from '../../../imports/libs/generalFunction';
// Collection
import {SAM_Student} from "../../../imports/collections/SAM/student";
import moment from "moment";

SAM_Student.before.insert(function (userId, doc) {
  const year = moment().format('YY');
  doc.gId = GeneralFunction.generatePrefixId({
    prefix: year,
    length: 7,
    collection: SAM_Student,
    field: 'gId'
  });
});
