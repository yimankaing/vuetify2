import {Meteor} from 'meteor/meteor';
import {EJSON} from "meteor/ejson";
import {Roles} from "meteor/alanning:roles";
import {Accounts} from "meteor/accounts-base";
import momentTz from 'moment-timezone';

momentTz.tz.setDefault("Asia/Bangkok");
import './account';
import './collection_hooks';
import './methods';
import './publications';
import './jobs';
import moment from "moment";
//collection
import {SAM_Province} from "../imports/collections/SAM/province";
import {SAM_District} from "../imports/collections/SAM/district";
import {SAM_Commune} from "../imports/collections/SAM/commune";
import {SAM_Village} from "../imports/collections/SAM/village";
//data
import {sites as levelS, provinces as levelP} from "../imports/rawData";
import {provinces} from "../imports/rawData/address/province";
import {districts} from "../imports/rawData/address/district";
import {communes} from "../imports/rawData/address/commune";
import {villages} from "../imports/rawData/address/village";
import {SAM_LevelProvince} from "../imports/collections/SAM/levelProvince";
import {SAM_LevelSite} from "../imports/collections/SAM/levelSite";

Meteor.startup(() => {
  SyncedCron.start();
  // User
  if (Meteor.users.find({}).count() === 0) {
    const data = EJSON.parse(Assets.getText("user-account.json"));
    data.forEach(({username, email, password, profile, roles}) => {
      const userExists = Accounts.findUserByUsername(username);
      if (!userExists) {
        const userId = Accounts.createUser({
          username,
          email,
          password,
          profile
        });

        Roles.addUsersToRoles(userId, roles, "national");
      }
    });
  }

  //insert raw data
  // province
  if (SAM_Province.find({}).count() === 0) {
    provinces.forEach(o => SAM_Province.insert(o));
  }
  //district
  if (SAM_District.find({}).count() === 0) {
    districts.forEach(o => SAM_District.insert(o));
  }
  //commune
  if (SAM_Commune.find({}).count() === 0) {
    communes.forEach(o => SAM_Commune.insert(o));
  }
  //village
  if (SAM_Village.find({}).count() === 0) {
    villages.forEach(o => SAM_Village.insert(o));
  }
  //level
  //province
  if (SAM_LevelProvince.find({}).count() === 0) {
    levelP.forEach(o => SAM_LevelProvince.insert(o));
  }
  //site
  if (SAM_LevelSite.find({}).count() === 0) {
    levelS.forEach(o => SAM_LevelSite.insert(o));
  }


  //student
  // if(SAM_Student.find({}).count()===0){
  //   for(let i = 1; i++; i>=100){
  //     SAM_Student.insert({name: 'name'+i})
  //   }
  // }
});
