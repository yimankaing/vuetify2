export const sites = [
  {
    _id: "0101",
    provinceId: "01",
    name: "Serey Sophon Health Center",
    code: "0101"
  },
  {
    _id: "0102",
    provinceId: "01",
    name: "Mongkul Borey Hospital",
    code: "0102"
  },
  {
    _id: "0103",
    provinceId: "01",
    name: "Poi Pet Hospital",
    code: "0103"
  },
  {
    _id: "0104",
    provinceId: "01",
    name: "Thmor Pouk RH",
    code: "0104"
  },
  {
    _id: "0105",
    provinceId: "01",
    name: "Preah Neth Preah",
    code: "0105"
  },
  {
    _id: "0201",
    provinceId: "02",
    name: "Maung Russey Hospital",
    code: "0201"
  },
  {
    _id: "0202",
    provinceId: "02",
    name: "Battambang Hospital",
    code: "0202"
  },
  {
    _id: "0205",
    provinceId: "02",
    name: "R5 Military Hospital",
    code: "0205"
  },
  {
    _id: "0203",
    provinceId: "02",
    name: "Sampov Loun RH",
    code: "0203"
  },
  {
    _id: "0204",
    provinceId: "02",
    name: "Thmor Kol RH",
    code: "0204"
  },
  {
    _id: "0206",
    provinceId: "02",
    name: "Roka RH",
    code: "0206"
  },
  {
    _id: "0301",
    provinceId: "03",
    name: "Kampong Cham Hospital",
    code: "0301"
  },
  {
    _id: "0304",
    provinceId: "03",
    name: "Choeung Prey Hospital",
    code: "0304"
  },
  {
    _id: "0306",
    provinceId: "03",
    name: "Chamkar Loeu RH",
    code: "0306"
  },
  {
    _id: "0305",
    provinceId: "03",
    name: "Srey Santhor RH",
    code: "0305"
  },
  {
    _id: "0307",
    provinceId: "03",
    name: "Batheay RH",
    code: "0307"
  },
  {
    _id: "0302",
    provinceId: "25",
    name: "Memot Hospital",
    code: "0302"
  },
  {
    _id: "0303",
    provinceId: "25",
    name: "Tbaung Khmum Hospital",
    code: "0303"
  },
  {
    _id: "0401",
    provinceId: "04",
    name: "Kampong Chhnang Hospital",
    code: "0401"
  },
  {
    _id: "0501",
    provinceId: "05",
    name: "Kampong Speu Hospital",
    code: "0501"
  },
  {
    _id: "0502",
    provinceId: "05",
    name: "Oudong RH",
    code: "0502"
  },
  {
    _id: "0503",
    provinceId: "05",
    name: "Kong Pisey",
    code: "0503"
  },
  {
    _id: "0601",
    provinceId: "06",
    name: "Kampong Thom Hospital",
    code: "0601"
  },
  {
    _id: "0602",
    provinceId: "06",
    name: "Baray Santok",
    code: "0602"
  },
  {
    _id: "0701",
    provinceId: "07",
    name: "Kampot Hospital",
    code: "0701"
  },
  {
    _id: "0703",
    provinceId: "07",
    name: "Kampong Trach Hospital",
    code: "0703"
  },
  {
    _id: "0801",
    provinceId: "08",
    name: "Chey Chum Neah Hospital",
    code: "0801"
  },
  {
    _id: "0802",
    provinceId: "08",
    name: "Koh Thom Hospital",
    code: "0802"
  },
  {
    _id: "0803",
    provinceId: "08",
    name: "Kien Svay RH",
    code: "0803"
  },
  {
    _id: "0901",
    provinceId: "09",
    name: "Smach Meanchey Hospital",
    code: "0901"
  },
  {
    _id: "0902",
    provinceId: "09",
    name: "Sre Ambil RH",
    code: "0902"
  },
  {
    _id: "1001",
    provinceId: "10",
    name: "Kratie RH",
    code: "1001"
  },
  {
    _id: "1201",
    provinceId: "12",
    name: "Khmero-Soviet Friendship Hospital",
    code: "1201"
  },
  {
    _id: "1202",
    provinceId: "12",
    name: "Calmatte Hospital",
    code: "1202"
  },
  {
    _id: "1203",
    provinceId: "12",
    name: "Preah Kossamak Hospital",
    code: "1203"
  },
  {
    _id: "1204",
    provinceId: "12",
    name: "National Pediatric Hospital",
    code: "1204"
  },
  {
    _id: "1205",
    provinceId: "12",
    name: "Hope Center",
    code: "1205"
  },
  {
    _id: "1207",
    provinceId: "12",
    name: "Preahketomealea Hospital",
    code: "1207"
  },
  {
    _id: "1208",
    provinceId: "12",
    name: "Social Health Clinic",
    code: "1208"
  },
  {
    _id: "1209",
    provinceId: "12",
    name: "Chhouk Sar 1",
    code: "1209"
  },
  {
    _id: "1210",
    provinceId: "12",
    name: "Chhouk Sar 2",
    code: "1210"
  },
  {
    _id: "1211",
    provinceId: "12",
    name: "Meanchey RH",
    code: "1211"
  },
  {
    _id: "1212",
    provinceId: "12",
    name: "Sam Dach Ov RH",
    code: "1212"
  },
  {
    _id: "1213",
    provinceId: "12",
    name: "Chamkar Doung RH",
    code: "1213"
  },
  {
    _id: "1214",
    provinceId: "12",
    name: "Pochin Tong RH",
    code: "1214"
  },
  {
    _id: "1401",
    provinceId: "14",
    name: "Neak Loeung Hospital",
    code: "1401"
  },
  {
    _id: "1402",
    provinceId: "14",
    name: "Prey Veng Hospital",
    code: "1402"
  },
  {
    _id: "1403",
    provinceId: "14",
    name: "Pearaing RH",
    code: "1403"
  },
  {
    _id: "1501",
    provinceId: "15",
    name: "Sampov Meas Hospital",
    code: "1501"
  },
  {
    _id: "1502",
    provinceId: "15",
    name: "Bakan RH",
    code: "1502"
  },
  {
    _id: "1503",
    provinceId: "15",
    name: "Veal Veng RH",
    code: "1503"
  },
  {
    _id: "1702",
    provinceId: "17",
    name: "Siem Reap Hospital",
    code: "1702"
  },
  {
    _id: "1703",
    provinceId: "17",
    name: "Komar Angkor Hospital",
    code: "1703"
  },
  {
    _id: "1704",
    provinceId: "17",
    name: "Soth Nikum Hospital",
    code: "1704"
  },
  {
    _id: "1705",
    provinceId: "17",
    name: "Kralanh RH",
    code: "1705"
  },
  {
    _id: "1801",
    provinceId: "18",
    name: "Sihanouk Ville Hospital",
    code: "1801"
  },
  {
    _id: "1901",
    provinceId: "19",
    name: "Stung Treng RH",
    code: "1901"
  },
  {
    _id: "2001",
    provinceId: "20",
    name: "Svay Rieng Hospital",
    code: "2001"
  },
  {
    _id: "2002",
    provinceId: "20",
    name: "Romeas Hek RH",
    code: "2002"
  },
  {
    _id: "2101",
    provinceId: "21",
    name: "Daun Keo Hospital",
    code: "2101"
  },
  {
    _id: "2102",
    provinceId: "21",
    name: "Kirivong Hospital",
    code: "2102"
  },
  {
    _id: "2103",
    provinceId: "21",
    name: "Ang Roka Hospital",
    code: "2103"
  },
  {
    _id: "2104",
    provinceId: "21",
    name: "Prey Kabas RH",
    code: "2104"
  },
  {
    _id: "2201",
    provinceId: "22",
    name: "Samrong RH",
    code: "2201"
  },
  {
    _id: "2202",
    provinceId: "22",
    name: "Anlong Veng RH",
    code: "2202"
  },
  {
    _id: "2401",
    provinceId: "24",
    name: "Pailin Hospital",
    code: "2401"
  },
  {
    _id: "1301",
    provinceId: "13",
    name: "16 Makara Hospital",
    code: "1301"
  },
  {
    _id: "2301",
    provinceId: "23",
    name: "Kep PH",
    code: "2301"
  },
  {
    _id: "1601",
    provinceId: "16",
    name: "Ratanakiri PH",
    code: "1601"
  },
  {
    _id: "1101",
    provinceId: "11",
    name: "Mondolkiri PH",
    code: "1101"
  }
];