import {keyPopulations, occupations, patientStatus, sex, yesNo, yesNoEligibleForVl, yesNoTPT} from "./selectOpt";

export const occupationCode = (val) => {
  return occupations.filter(o => o.name === val)[0] ? parseInt(occupations.filter(o => o.name === val)[0].id) : '';
};
export const yesNoTPTCode = (val) => {
  return yesNoTPT.filter(o => o.name === val)[0] ? parseInt(yesNoTPT.filter(o => o.name === val)[0].id) : '';
};
export const patientStatusCode = (val) => {
  return patientStatus.filter(o => o.name === val)[0] ? parseInt(patientStatus.filter(o => o.name === val)[0].id) : '';
};
export const yesNoCode = (val) => {
  return yesNo.filter(o => o.name === val)[0] ? parseInt(yesNo.filter(o => o.name === val)[0].id) : '';
};
export const keyPopulationsCode = (val) => {
  return keyPopulations.filter(o => o.name === val)[0] ? parseInt(keyPopulations.filter(o => o.name === val)[0].id) : '';
};
export const sexCode = (val) => {
  return sex.filter(o => o.name === val)[0] ? parseInt(sex.filter(o => o.name === val)[0].id) : '';
};
export const yesNoEligibleForVlCode = (val) => {
  return yesNoEligibleForVl.filter(o => o.name === val)[0] ? parseInt(yesNoEligibleForVl.filter(o => o.name === val)[0].id) : '';
};