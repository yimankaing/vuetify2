export const provinces = [
  {
    name: "Banteay Meanchey",
    _id: "01",
    code: "01"
  },
  {
    name: "Battambang",
    _id: "02",
    code: "02"
  },
  {
    name: "Kampong Cham",
    _id: "03",
    code: "03"
  },
  {
    name: "Tbaung Khmum",
    _id: "25",
    code: "25"
  },
  {
    name: "Kampong Chhnang",
    _id: "04",
    code: "04"
  },
  {
    name: "Kampong Speu",
    _id: "05",
    code: "05"
  },
  {
    name: "Kampong Thom",
    _id: "06",
    code: "06"
  },
  {
    name: "Kampot",
    _id: "07",
    code: "07"
  },
  {
    name: "Kandal",
    _id: "08",
    code: "08"
  },
  {
    name: "Koh Kong",
    _id: "09",
    code: "09"
  },
  {
    name: "Kratie",
    _id: "10",
    code: "10"
  },
  {
    name: "Phnom Penh Municipal",
    _id: "12",
    code: "12"
  },
  {
    name: "Prey Veng",
    _id: "14",
    code: "14"
  },
  {
    name: "Pursat",
    _id: "15",
    code: "15"
  },
  {
    name: "Siem Reap",
    _id: "17",
    code: "17"
  },
  {
    name: "Sihanouk Ville",
    _id: "18",
    code: "18"
  },
  {
    name: "Stung Treng",
    _id: "19",
    code: "19"
  },
  {
    name: "Svay Rieng",
    _id: "20",
    code: "20"
  },
  {
    name: "Takeo",
    _id: "21",
    code: "21"
  },
  {
    name: "Oddar Meanchey",
    _id: "22",
    code: "22"
  },
  {
    name: "Pailin",
    _id: "24",
    code: "24"
  },
  {
    name: "Preah Vihear",
    _id: "13",
    code: "13"
  },
  {
    name: "Kep",
    _id: "23",
    code: "23"
  },
  {
    name: "Ratanakiri",
    _id: "16",
    code: "16"
  },
  {
    name: "Mondolkiri",
    _id: "11",
    code: "11"
  }
];
