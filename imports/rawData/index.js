import {sites} from "./sites";
import {provinces} from "./provinces";

export {sites, provinces};