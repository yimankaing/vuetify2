export const sex = [
  {id: '0', code: '00', name: 'Female'},
  {id: '1', code: '01', name: 'Male'},
];

export const occupations = [
  {id: "1", code: "01", name: "CSW"},
  {id: "2", code: "02", name: "Drivers"},
  {id: "3", code: "03", name: "Factory Worker"},
  {id: "4", code: "04", name: "Farmer"},
  {id: "5", code: "05", name: "Farmer and Self-business"},
  {id: "6", code: "06", name: "Fisherman"},
  {id: "7", code: "07", name: "Govenernment Staff"},
  {id: "8", code: "08", name: "Household Male"},
  {id: "9", code: "09", name: "Housewife"},
  {id: "10", code: "10", name: "IDSW(Karaoke,Beer,etc...)"},
  {id: "11", code: "11", name: "Construction Worker"},
  {id: "12", code: "12", name: "Military/Police"},
  {id: "13", code: "13", name: "Moto-Dop"},
  {id: "14", code: "14", name: "NGO Staff"},
  {id: "15", code: "15", name: "Private Company Staff"},
  {id: "16", code: "16", name: "Scavenger"},
  {id: "17", code: "17", name: "Self-business/Self-employed"},
  {id: "18", code: "18", name: "Singer"},
  {id: "19", code: "19", name: "Skill Worker"},
  {id: "20", code: "20", name: "Student"},
  {id: "21", code: "21", name: "Tailor"},
  {id: "22", code: "22", name: "Taxi driver"},
  {id: "23", code: "23", name: "Unskilled Worker"},
  {id: "24", code: "24", name: "Others"},
  {id: "25", code: "25", name: "Chef"},
  {id: "26", code: "26", name: "Car repair"},
];

export const keyPopulations = [
  {id: "1", code: "01", name: "MSM"},
  {id: "2", code: "02", name: "TG"},
  {id: "3", code: "03", name: "FEW"},
  {id: "4", code: "04", name: "PWID/PWUD"},
  {id: "5", code: "05", name: "Non KP"},
  {id: "9", code: "09", name: "N/A"},
];

export const patientStatus = [
  {id: "0", code: "00", name: "LTF"},
  {id: "1", code: "01", name: "Dead"},
  {id: "3", code: "03", name: "Transfer out"},
  {id: "9", code: "09", name: "Active"},
  {id: "99", code: "99", name: "HIV Negative"},
];

export const status = [
  {id: '1', code: '01', name: "pending"},
  {id: '2', code: '02', name: "reviewed"},
  {id: '3', code: '03', name: "completed"},
];

export const yesNo = [
  {id: '1', code: '01', name: 'Yes'},
  {id: '2', code: '02', name: 'No'},
];

export const yesNoEligibleForVl = [
  {id: '1', code: '01', name: 'Yes, pending result'},
  {id: '2', code: '02', name: 'Yes, not performed'},
  {id: '3', code: '03', name: 'No'},
];

export const yesNoTPT = [
  {id: '1', code: '01', name: 'Yes, completed'},
  {id: '2', code: '02', name: 'Yes, ongoing'},
  {id: '3', code: '03', name: 'No'},
];
