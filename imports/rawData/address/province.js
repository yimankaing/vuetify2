export const provinces = [
  {
    "id": "1",
    "name": "Banteay Meanchey",
    "code": "01"
  },
  {
    "id": "2",
    "name": "Battambang",
    "code": "02"
  },
  {
    "id": "3",
    "name": "Kampong Cham",
    "code": "03"
  },
  {
    "id": "4",
    "name": "Kampong Chhnang",
    "code": "04"
  },
  {
    "id": "5",
    "name": "Kampong Speu",
    "code": "05"
  },
  {
    "id": "6",
    "name": "Kampong Thom",
    "code": "06"
  },
  {
    "id": "7",
    "name": "Kampot",
    "code": "07"
  },
  {
    "id": "8",
    "name": "Kandal",
    "code": "08"
  },
  {
    "id": "9",
    "name": "Koh Kong",
    "code": "09"
  },
  {
    "id": "10",
    "name": "Kratie",
    "code": "10"
  },
  {
    "id": "11",
    "name": "Mondul Kiri",
    "code": "11"
  },
  {
    "id": "12",
    "name": "Phnom Penh",
    "code": "12"
  },
  {
    "id": "13",
    "name": "Preah Vihear",
    "code": "13"
  },
  {
    "id": "14",
    "name": "Prey Veng",
    "code": "14"
  },
  {
    "id": "15",
    "name": "Pursat",
    "code": "15"
  },
  {
    "id": "16",
    "name": "Ratanak Kiri",
    "code": "16"
  },
  {
    "id": "17",
    "name": "Siem Reap",
    "code": "17"
  },
  {
    "id": "18",
    "name": "Preah Sihanouk",
    "code": "18"
  },
  {
    "id": "19",
    "name": "Stung Treng",
    "code": "19"
  },
  {
    "id": "20",
    "name": "Svay Rieng",
    "code": "20"
  },
  {
    "id": "21",
    "name": "Takeo",
    "code": "21"
  },
  {
    "id": "22",
    "name": "Oddar Meanchey",
    "code": "22"
  },
  {
    "id": "23",
    "name": "Kep",
    "code": "23"
  },
  {
    "id": "24",
    "name": "Pailin",
    "code": "24"
  },
  {
    "id": "25",
    "name": "Tboung Khmum",
    "code": "25"
  },
  {
    "id": "99",
    "name": "Unknown",
    "code": "99"
  }
];
