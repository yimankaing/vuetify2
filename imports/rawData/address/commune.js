export const communes = [
  {
    "id": "10201",
    "name": "Banteay Neang",
    "districtId": "102",
    "code": "010201"
  },
  {
    "id": "10202",
    "name": "Bat Trang",
    "districtId": "102",
    "code": "010202"
  },
  {
    "id": "10203",
    "name": "Chamnaom",
    "districtId": "102",
    "code": "010203"
  },
  {
    "id": "10204",
    "name": "Kouk Ballangk",
    "districtId": "102",
    "code": "010204"
  },
  {
    "id": "10205",
    "name": "Koy Maeng",
    "districtId": "102",
    "code": "010205"
  },
  {
    "id": "10206",
    "name": "Ou Prasat",
    "districtId": "102",
    "code": "010206"
  },
  {
    "id": "10207",
    "name": "Phnum Touch",
    "districtId": "102",
    "code": "010207"
  },
  {
    "id": "10208",
    "name": "Rohat Tuek",
    "districtId": "102",
    "code": "010208"
  },
  {
    "id": "10209",
    "name": "Ruessei Kraok",
    "districtId": "102",
    "code": "010209"
  },
  {
    "id": "10210",
    "name": "Sambuor",
    "districtId": "102",
    "code": "010210"
  },
  {
    "id": "10211",
    "name": "Soea",
    "districtId": "102",
    "code": "010211"
  },
  {
    "id": "10212",
    "name": "Srah Reang",
    "districtId": "102",
    "code": "010212"
  },
  {
    "id": "10213",
    "name": "Ta Lam",
    "districtId": "102",
    "code": "010213"
  },
  {
    "id": "10301",
    "name": "Nam Tau",
    "districtId": "103",
    "code": "010301"
  },
  {
    "id": "10302",
    "name": "Poy Char",
    "districtId": "103",
    "code": "010302"
  },
  {
    "id": "10303",
    "name": "Ponley",
    "districtId": "103",
    "code": "010303"
  },
  {
    "id": "10304",
    "name": "Spean Sraeng",
    "districtId": "103",
    "code": "010304"
  },
  {
    "id": "10305",
    "name": "Srah Chik",
    "districtId": "103",
    "code": "010305"
  },
  {
    "id": "10306",
    "name": "Phnum Dei",
    "districtId": "103",
    "code": "010306"
  },
  {
    "id": "10401",
    "name": "Chnuor Mean Chey",
    "districtId": "104",
    "code": "010401"
  },
  {
    "id": "10402",
    "name": "Chob Vari",
    "districtId": "104",
    "code": "010402"
  },
  {
    "id": "10403",
    "name": "Phnum Lieb",
    "districtId": "104",
    "code": "010403"
  },
  {
    "id": "10404",
    "name": "Prasat",
    "districtId": "104",
    "code": "010404"
  },
  {
    "id": "10405",
    "name": "Preak Netr Preah",
    "districtId": "104",
    "code": "010405"
  },
  {
    "id": "10406",
    "name": "Rohal",
    "districtId": "104",
    "code": "010406"
  },
  {
    "id": "10407",
    "name": "Tean Kam",
    "districtId": "104",
    "code": "010407"
  },
  {
    "id": "10408",
    "name": "Tuek Chour",
    "districtId": "104",
    "code": "010408"
  },
  {
    "id": "10409",
    "name": "Bos Sbov",
    "districtId": "104",
    "code": "010409"
  },
  {
    "id": "10501",
    "name": "Changha",
    "districtId": "105",
    "code": "010501"
  },
  {
    "id": "10502",
    "name": "Koub",
    "districtId": "105",
    "code": "010502"
  },
  {
    "id": "10503",
    "name": "Kuttasat",
    "districtId": "105",
    "code": "010503"
  },
  {
    "id": "10505",
    "name": "Samraong",
    "districtId": "105",
    "code": "010505"
  },
  {
    "id": "10506",
    "name": "Souphi",
    "districtId": "105",
    "code": "010506"
  },
  {
    "id": "10507",
    "name": "Soengh",
    "districtId": "105",
    "code": "010507"
  },
  {
    "id": "10509",
    "name": "Ou Beichoan",
    "districtId": "105",
    "code": "010509"
  },
  {
    "id": "10602",
    "name": "Kampong Svay",
    "districtId": "106",
    "code": "010602"
  },
  {
    "id": "10603",
    "name": "Kaoh Pong Satv",
    "districtId": "106",
    "code": "010603"
  },
  {
    "id": "10604",
    "name": "Mkak",
    "districtId": "106",
    "code": "010604"
  },
  {
    "id": "10605",
    "name": "Ou Ambel",
    "districtId": "106",
    "code": "010605"
  },
  {
    "id": "10606",
    "name": "Phniet",
    "districtId": "106",
    "code": "010606"
  },
  {
    "id": "10607",
    "name": "Preah Ponlea",
    "districtId": "106",
    "code": "010607"
  },
  {
    "id": "10608",
    "name": "Tuek Thla",
    "districtId": "106",
    "code": "010608"
  },
  {
    "id": "10701",
    "name": "Banteay Chhmar",
    "districtId": "107",
    "code": "010701"
  },
  {
    "id": "10702",
    "name": "Kouk Romiet",
    "districtId": "107",
    "code": "010702"
  },
  {
    "id": "10703",
    "name": "Phum Thmei",
    "districtId": "107",
    "code": "010703"
  },
  {
    "id": "10704",
    "name": "Thma Puok",
    "districtId": "107",
    "code": "010704"
  },
  {
    "id": "10705",
    "name": "Kouk Kakthen",
    "districtId": "107",
    "code": "010705"
  },
  {
    "id": "10706",
    "name": "Kumru",
    "districtId": "107",
    "code": "010706"
  },
  {
    "id": "10801",
    "name": "Phkoam",
    "districtId": "108",
    "code": "010801"
  },
  {
    "id": "10802",
    "name": "Sarongk",
    "districtId": "108",
    "code": "010802"
  },
  {
    "id": "10803",
    "name": "Sla Kram",
    "districtId": "108",
    "code": "010803"
  },
  {
    "id": "10804",
    "name": "Svay Chek",
    "districtId": "108",
    "code": "010804"
  },
  {
    "id": "10805",
    "name": "Ta Baen",
    "districtId": "108",
    "code": "010805"
  },
  {
    "id": "10806",
    "name": "Ta Phou",
    "districtId": "108",
    "code": "010806"
  },
  {
    "id": "10807",
    "name": "Treas",
    "districtId": "108",
    "code": "010807"
  },
  {
    "id": "10808",
    "name": "Roluos",
    "districtId": "108",
    "code": "010808"
  },
  {
    "id": "10901",
    "name": "Boeng Beng",
    "districtId": "109",
    "code": "010901"
  },
  {
    "id": "10902",
    "name": "Malai",
    "districtId": "109",
    "code": "010902"
  },
  {
    "id": "10903",
    "name": "Ou Sampoar",
    "districtId": "109",
    "code": "010903"
  },
  {
    "id": "10904",
    "name": "Ou Sralau",
    "districtId": "109",
    "code": "010904"
  },
  {
    "id": "10905",
    "name": "Tuol Pongro",
    "districtId": "109",
    "code": "010905"
  },
  {
    "id": "10906",
    "name": "Ta Kong",
    "districtId": "109",
    "code": "010906"
  },
  {
    "id": "11001",
    "name": "Nimitt",
    "districtId": "110",
    "code": "011001"
  },
  {
    "id": "11002",
    "name": "Paoy Paet",
    "districtId": "110",
    "code": "011002"
  },
  {
    "id": "11003",
    "name": "Phsar Kandal",
    "districtId": "110",
    "code": "011003"
  },
  {
    "id": "20101",
    "name": "Kantueu Muoy",
    "districtId": "201",
    "code": "020101"
  },
  {
    "id": "20102",
    "name": "Kantueu Pir",
    "districtId": "201",
    "code": "020102"
  },
  {
    "id": "20103",
    "name": "Bay Damram",
    "districtId": "201",
    "code": "020103"
  },
  {
    "id": "20104",
    "name": "Chheu Teal",
    "districtId": "201",
    "code": "020104"
  },
  {
    "id": "20105",
    "name": "Chaeng Mean Chey",
    "districtId": "201",
    "code": "020105"
  },
  {
    "id": "20106",
    "name": "Phnum Sampov",
    "districtId": "201",
    "code": "020106"
  },
  {
    "id": "20107",
    "name": "Snoeng",
    "districtId": "201",
    "code": "020107"
  },
  {
    "id": "20108",
    "name": "Ta Kream",
    "districtId": "201",
    "code": "020108"
  },
  {
    "id": "20201",
    "name": "Ta Pung",
    "districtId": "202",
    "code": "020201"
  },
  {
    "id": "20202",
    "name": "Ta Meun",
    "districtId": "202",
    "code": "020202"
  },
  {
    "id": "20203",
    "name": "Ou Ta Ki",
    "districtId": "202",
    "code": "020203"
  },
  {
    "id": "20204",
    "name": "Chrey",
    "districtId": "202",
    "code": "020204"
  },
  {
    "id": "20205",
    "name": "Anlong Run",
    "districtId": "202",
    "code": "020205"
  },
  {
    "id": "20206",
    "name": "Chrouy Sdau",
    "districtId": "202",
    "code": "020206"
  },
  {
    "id": "20207",
    "name": "Boeng Pring",
    "districtId": "202",
    "code": "020207"
  },
  {
    "id": "20208",
    "name": "Kouk Khmum",
    "districtId": "202",
    "code": "020208"
  },
  {
    "id": "20209",
    "name": "Bansay Traeng",
    "districtId": "202",
    "code": "020209"
  },
  {
    "id": "20210",
    "name": "Rung Chrey",
    "districtId": "202",
    "code": "020210"
  },
  {
    "id": "20301",
    "name": "Tuol Ta Ek",
    "districtId": "203",
    "code": "020301"
  },
  {
    "id": "20302",
    "name": "Prek Preah Sdach",
    "districtId": "203",
    "code": "020302"
  },
  {
    "id": "20303",
    "name": "Rottanak",
    "districtId": "203",
    "code": "020303"
  },
  {
    "id": "20304",
    "name": "Chomkar Somraong",
    "districtId": "203",
    "code": "020304"
  },
  {
    "id": "20305",
    "name": "Sla Ket",
    "districtId": "203",
    "code": "020305"
  },
  {
    "id": "20306",
    "name": "Kdol Doun Teav",
    "districtId": "203",
    "code": "020306"
  },
  {
    "id": "20307",
    "name": "OMal",
    "districtId": "203",
    "code": "020307"
  },
  {
    "id": "20308",
    "name": "wat Kor",
    "districtId": "203",
    "code": "020308"
  },
  {
    "id": "20309",
    "name": "Ou Char",
    "districtId": "203",
    "code": "020309"
  },
  {
    "id": "20310",
    "name": "Svay Por",
    "districtId": "203",
    "code": "020310"
  },
  {
    "id": "20401",
    "name": "Bavel",
    "districtId": "204",
    "code": "020401"
  },
  {
    "id": "20402",
    "name": "Khnach Romeas",
    "districtId": "204",
    "code": "020402"
  },
  {
    "id": "20403",
    "name": "Lvea",
    "districtId": "204",
    "code": "020403"
  },
  {
    "id": "20404",
    "name": "Prey Khpos",
    "districtId": "204",
    "code": "020404"
  },
  {
    "id": "20405",
    "name": "Ampil Pram Daeum",
    "districtId": "204",
    "code": "020405"
  },
  {
    "id": "20406",
    "name": "Kdol Ta Haen",
    "districtId": "204",
    "code": "020406"
  },
  {
    "id": "20407",
    "name": "Khlaeng Meas",
    "districtId": "204",
    "code": "020407"
  },
  {
    "id": "20408",
    "name": "Boeung Pram",
    "districtId": "204",
    "code": "020408"
  },
  {
    "id": "20501",
    "name": "Preaek Norint",
    "districtId": "205",
    "code": "020501"
  },
  {
    "id": "20502",
    "name": "Samraong Knong",
    "districtId": "205",
    "code": "020502"
  },
  {
    "id": "20503",
    "name": "Preaek Khpob",
    "districtId": "205",
    "code": "020503"
  },
  {
    "id": "20504",
    "name": "Preaek Luong",
    "districtId": "205",
    "code": "020504"
  },
  {
    "id": "20505",
    "name": "Peam Aek",
    "districtId": "205",
    "code": "020505"
  },
  {
    "id": "20506",
    "name": "Prey Chas",
    "districtId": "205",
    "code": "020506"
  },
  {
    "id": "20507",
    "name": "Kaoh Chiveang",
    "districtId": "205",
    "code": "020507"
  },
  {
    "id": "20601",
    "name": "Moung",
    "districtId": "206",
    "code": "020601"
  },
  {
    "id": "20602",
    "name": "Kear",
    "districtId": "206",
    "code": "020602"
  },
  {
    "id": "20603",
    "name": "Prey Svay",
    "districtId": "206",
    "code": "020603"
  },
  {
    "id": "20604",
    "name": "Ruessei Krang",
    "districtId": "206",
    "code": "020604"
  },
  {
    "id": "20605",
    "name": "Chrey",
    "districtId": "206",
    "code": "020605"
  },
  {
    "id": "20606",
    "name": "Ta Loas",
    "districtId": "206",
    "code": "020606"
  },
  {
    "id": "20607",
    "name": "Kakaoh",
    "districtId": "206",
    "code": "020607"
  },
  {
    "id": "20608",
    "name": "Prey Touch",
    "districtId": "206",
    "code": "020608"
  },
  {
    "id": "20609",
    "name": "Robas Mongkol",
    "districtId": "206",
    "code": "020609"
  },
  {
    "id": "20701",
    "name": "Sdau",
    "districtId": "207",
    "code": "020701"
  },
  {
    "id": "20702",
    "name": "Andaeuk Haeb",
    "districtId": "207",
    "code": "020702"
  },
  {
    "id": "20703",
    "name": "Phlov Meas",
    "districtId": "207",
    "code": "020703"
  },
  {
    "id": "20704",
    "name": "Traeng",
    "districtId": "207",
    "code": "020704"
  },
  {
    "id": "20705",
    "name": "Reaksmei Songha",
    "districtId": "207",
    "code": "020705"
  },
  {
    "id": "20801",
    "name": "Anlong Vil",
    "districtId": "208",
    "code": "020801"
  },
  {
    "id": "20802",
    "name": "Norea",
    "districtId": "208",
    "code": "020802"
  },
  {
    "id": "20803",
    "name": "Ta Pon",
    "districtId": "208",
    "code": "020803"
  },
  {
    "id": "20804",
    "name": "Roka",
    "districtId": "208",
    "code": "020804"
  },
  {
    "id": "20805",
    "name": "Kampong Preah",
    "districtId": "208",
    "code": "020805"
  },
  {
    "id": "20806",
    "name": "Kampong Prieng",
    "districtId": "208",
    "code": "020806"
  },
  {
    "id": "20807",
    "name": "Reang Kesei",
    "districtId": "208",
    "code": "020807"
  },
  {
    "id": "20808",
    "name": "Ou Dambang Muoy",
    "districtId": "208",
    "code": "020808"
  },
  {
    "id": "20809",
    "name": "Ou Dambang Pir",
    "districtId": "208",
    "code": "020809"
  },
  {
    "id": "20810",
    "name": "Vaot Ta Muem",
    "districtId": "208",
    "code": "020810"
  },
  {
    "id": "20901",
    "name": "Ta Taok",
    "districtId": "209",
    "code": "020901"
  },
  {
    "id": "20902",
    "name": "Kampong Lpov",
    "districtId": "209",
    "code": "020902"
  },
  {
    "id": "20903",
    "name": "Ou Samril",
    "districtId": "209",
    "code": "020903"
  },
  {
    "id": "20904",
    "name": "Sung",
    "districtId": "209",
    "code": "020904"
  },
  {
    "id": "20905",
    "name": "Samlout",
    "districtId": "209",
    "code": "020905"
  },
  {
    "id": "20906",
    "name": "Mean Chey",
    "districtId": "209",
    "code": "020906"
  },
  {
    "id": "20907",
    "name": "Ta Sanh",
    "districtId": "209",
    "code": "020907"
  },
  {
    "id": "21001",
    "name": "Sampov Lun",
    "districtId": "210",
    "code": "021001"
  },
  {
    "id": "21002",
    "name": "Angkor Ban",
    "districtId": "210",
    "code": "021002"
  },
  {
    "id": "21003",
    "name": "Ta Sda",
    "districtId": "210",
    "code": "021003"
  },
  {
    "id": "21004",
    "name": "Santepheap",
    "districtId": "210",
    "code": "021004"
  },
  {
    "id": "21005",
    "name": "Serei Mean Chey",
    "districtId": "210",
    "code": "021005"
  },
  {
    "id": "21006",
    "name": "Chrey Seima",
    "districtId": "210",
    "code": "021006"
  },
  {
    "id": "21101",
    "name": "Phnum Proek",
    "districtId": "211",
    "code": "021101"
  },
  {
    "id": "21102",
    "name": "Pech Chenda",
    "districtId": "211",
    "code": "021102"
  },
  {
    "id": "21103",
    "name": "Bour",
    "districtId": "211",
    "code": "021103"
  },
  {
    "id": "21104",
    "name": "Barang Thleak",
    "districtId": "211",
    "code": "021104"
  },
  {
    "id": "21105",
    "name": "Ou Rumduol",
    "districtId": "211",
    "code": "021105"
  },
  {
    "id": "21201",
    "name": "Kamrieng",
    "districtId": "212",
    "code": "021201"
  },
  {
    "id": "21202",
    "name": "Boeng Reang",
    "districtId": "212",
    "code": "021202"
  },
  {
    "id": "21203",
    "name": "Ou Da",
    "districtId": "212",
    "code": "021203"
  },
  {
    "id": "21204",
    "name": "Trang",
    "districtId": "212",
    "code": "021204"
  },
  {
    "id": "21205",
    "name": "Ta Saen",
    "districtId": "212",
    "code": "021205"
  },
  {
    "id": "21206",
    "name": "Ta Krei",
    "districtId": "212",
    "code": "021206"
  },
  {
    "id": "21301",
    "name": "Thipakdei",
    "districtId": "213",
    "code": "021301"
  },
  {
    "id": "21302",
    "name": "Kaos Krala",
    "districtId": "213",
    "code": "021302"
  },
  {
    "id": "21303",
    "name": "Hab",
    "districtId": "213",
    "code": "021303"
  },
  {
    "id": "21304",
    "name": "Preah Phos",
    "districtId": "213",
    "code": "021304"
  },
  {
    "id": "21305",
    "name": "Doun Ba",
    "districtId": "213",
    "code": "021305"
  },
  {
    "id": "21306",
    "name": "Chhnal Moan",
    "districtId": "213",
    "code": "021306"
  },
  {
    "id": "21401",
    "name": "Preaek Chik",
    "districtId": "214",
    "code": "021401"
  },
  {
    "id": "21402",
    "name": "Prey Tralach",
    "districtId": "214",
    "code": "021402"
  },
  {
    "id": "21403",
    "name": "Mukh Reah",
    "districtId": "214",
    "code": "021403"
  },
  {
    "id": "21404",
    "name": "Sdok Pravoek",
    "districtId": "214",
    "code": "021404"
  },
  {
    "id": "21405",
    "name": "Basak",
    "districtId": "214",
    "code": "021405"
  },
  {
    "id": "30101",
    "name": "Batheay",
    "districtId": "301",
    "code": "030101"
  },
  {
    "id": "30102",
    "name": "Chbar Ampov",
    "districtId": "301",
    "code": "030102"
  },
  {
    "id": "30103",
    "name": "Chealea",
    "districtId": "301",
    "code": "030103"
  },
  {
    "id": "30104",
    "name": "Cheung Prey",
    "districtId": "301",
    "code": "030104"
  },
  {
    "id": "30105",
    "name": "Me Pring",
    "districtId": "301",
    "code": "030105"
  },
  {
    "id": "30106",
    "name": "Ph'av",
    "districtId": "301",
    "code": "030106"
  },
  {
    "id": "30107",
    "name": "Sambour",
    "districtId": "301",
    "code": "030107"
  },
  {
    "id": "30108",
    "name": "Sandaek",
    "districtId": "301",
    "code": "030108"
  },
  {
    "id": "30109",
    "name": "Tang Krang",
    "districtId": "301",
    "code": "030109"
  },
  {
    "id": "30110",
    "name": "Tang Krasang",
    "districtId": "301",
    "code": "030110"
  },
  {
    "id": "30111",
    "name": "Trab",
    "districtId": "301",
    "code": "030111"
  },
  {
    "id": "30112",
    "name": "Tumnob",
    "districtId": "301",
    "code": "030112"
  },
  {
    "id": "30201",
    "name": "Bos Khnor",
    "districtId": "302",
    "code": "030201"
  },
  {
    "id": "30202",
    "name": "Chamkar Andoung",
    "districtId": "302",
    "code": "030202"
  },
  {
    "id": "30203",
    "name": "Cheyyou",
    "districtId": "302",
    "code": "030203"
  },
  {
    "id": "30204",
    "name": "Lvea Leu",
    "districtId": "302",
    "code": "030204"
  },
  {
    "id": "30205",
    "name": "Spueu",
    "districtId": "302",
    "code": "030205"
  },
  {
    "id": "30206",
    "name": "Svay Teab",
    "districtId": "302",
    "code": "030206"
  },
  {
    "id": "30207",
    "name": "Ta Ong",
    "districtId": "302",
    "code": "030207"
  },
  {
    "id": "30208",
    "name": "Ta Prok",
    "districtId": "302",
    "code": "030208"
  },
  {
    "id": "30301",
    "name": "Khnor Dambang",
    "districtId": "303",
    "code": "030301"
  },
  {
    "id": "30302",
    "name": "Kouk Rovieng",
    "districtId": "303",
    "code": "030302"
  },
  {
    "id": "30303",
    "name": "Pdau Chum",
    "districtId": "303",
    "code": "030303"
  },
  {
    "id": "30304",
    "name": "Prey Char",
    "districtId": "303",
    "code": "030304"
  },
  {
    "id": "30305",
    "name": "Pring Chrum",
    "districtId": "303",
    "code": "030305"
  },
  {
    "id": "30306",
    "name": "Sampong Chey",
    "districtId": "303",
    "code": "030306"
  },
  {
    "id": "30307",
    "name": "Sdaeung Chey",
    "districtId": "303",
    "code": "030307"
  },
  {
    "id": "30308",
    "name": "Soutib",
    "districtId": "303",
    "code": "030308"
  },
  {
    "id": "30309",
    "name": "Sramar",
    "districtId": "303",
    "code": "030309"
  },
  {
    "id": "30310",
    "name": "Trapeang Kor",
    "districtId": "303",
    "code": "030310"
  },
  {
    "id": "30501",
    "name": "Boeng Kok",
    "districtId": "305",
    "code": "030501"
  },
  {
    "id": "30502",
    "name": "Kampong Cham",
    "districtId": "305",
    "code": "030502"
  },
  {
    "id": "30503",
    "name": "Sambuor Meas",
    "districtId": "305",
    "code": "030503"
  },
  {
    "id": "30504",
    "name": "Veal Vong",
    "districtId": "305",
    "code": "030504"
  },
  {
    "id": "30601",
    "name": "Ampil",
    "districtId": "306",
    "code": "030601"
  },
  {
    "id": "30602",
    "name": "Hanchey",
    "districtId": "306",
    "code": "030602"
  },
  {
    "id": "30603",
    "name": "Kien Chrey",
    "districtId": "306",
    "code": "030603"
  },
  {
    "id": "30604",
    "name": "Kokor",
    "districtId": "306",
    "code": "030604"
  },
  {
    "id": "30605",
    "name": "Kaoh Mitt",
    "districtId": "306",
    "code": "030605"
  },
  {
    "id": "30606",
    "name": "Kaoh Roka",
    "districtId": "306",
    "code": "030606"
  },
  {
    "id": "30607",
    "name": "Kaoh Samraong",
    "districtId": "306",
    "code": "030607"
  },
  {
    "id": "30608",
    "name": "Kaoh Tontuem",
    "districtId": "306",
    "code": "030608"
  },
  {
    "id": "30609",
    "name": "Krala",
    "districtId": "306",
    "code": "030609"
  },
  {
    "id": "30610",
    "name": "Ou Svay",
    "districtId": "306",
    "code": "030610"
  },
  {
    "id": "30611",
    "name": "Ro'ang",
    "districtId": "306",
    "code": "030611"
  },
  {
    "id": "30612",
    "name": "Rumchek",
    "districtId": "306",
    "code": "030612"
  },
  {
    "id": "30613",
    "name": "Srak",
    "districtId": "306",
    "code": "030613"
  },
  {
    "id": "30614",
    "name": "Trean",
    "districtId": "306",
    "code": "030614"
  },
  {
    "id": "30615",
    "name": "Vihear Thum",
    "districtId": "306",
    "code": "030615"
  },
  {
    "id": "30701",
    "name": "Angkor Ban",
    "districtId": "307",
    "code": "030701"
  },
  {
    "id": "30702",
    "name": "Kang Ta Noeng",
    "districtId": "307",
    "code": "030702"
  },
  {
    "id": "30703",
    "name": "Khchau",
    "districtId": "307",
    "code": "030703"
  },
  {
    "id": "30704",
    "name": "Peam Chi Kang",
    "districtId": "307",
    "code": "030704"
  },
  {
    "id": "30705",
    "name": "Preaek Koy",
    "districtId": "307",
    "code": "030705"
  },
  {
    "id": "30706",
    "name": "Preaek Krabau",
    "districtId": "307",
    "code": "030706"
  },
  {
    "id": "30707",
    "name": "Reay Pay",
    "districtId": "307",
    "code": "030707"
  },
  {
    "id": "30708",
    "name": "Roka Ar",
    "districtId": "307",
    "code": "030708"
  },
  {
    "id": "30709",
    "name": "Roka Koy",
    "districtId": "307",
    "code": "030709"
  },
  {
    "id": "30710",
    "name": "Sdau",
    "districtId": "307",
    "code": "030710"
  },
  {
    "id": "30711",
    "name": "Sour Kong",
    "districtId": "307",
    "code": "030711"
  },
  {
    "id": "30801",
    "name": "Kampong Reab",
    "districtId": "308",
    "code": "030801"
  },
  {
    "id": "30802",
    "name": "Kaoh Sotin",
    "districtId": "308",
    "code": "030802"
  },
  {
    "id": "30803",
    "name": "Lve",
    "districtId": "308",
    "code": "030803"
  },
  {
    "id": "30804",
    "name": "Moha Leaph",
    "districtId": "308",
    "code": "030804"
  },
  {
    "id": "30805",
    "name": "Moha Khnhoung",
    "districtId": "308",
    "code": "030805"
  },
  {
    "id": "30806",
    "name": "Peam Prathnuoh",
    "districtId": "308",
    "code": "030806"
  },
  {
    "id": "30807",
    "name": "Pongro",
    "districtId": "308",
    "code": "030807"
  },
  {
    "id": "30808",
    "name": "Preaek Ta Nong",
    "districtId": "308",
    "code": "030808"
  },
  {
    "id": "31301",
    "name": "Baray",
    "districtId": "313",
    "code": "031301"
  },
  {
    "id": "31302",
    "name": "Boeng Nay",
    "districtId": "313",
    "code": "031302"
  },
  {
    "id": "31303",
    "name": "Chrey Vien",
    "districtId": "313",
    "code": "031303"
  },
  {
    "id": "31304",
    "name": "Khvet Thum",
    "districtId": "313",
    "code": "031304"
  },
  {
    "id": "31305",
    "name": "Kor",
    "districtId": "313",
    "code": "031305"
  },
  {
    "id": "31306",
    "name": "Krouch",
    "districtId": "313",
    "code": "031306"
  },
  {
    "id": "31307",
    "name": "Lvea",
    "districtId": "313",
    "code": "031307"
  },
  {
    "id": "31308",
    "name": "Mien",
    "districtId": "313",
    "code": "031308"
  },
  {
    "id": "31309",
    "name": "Prey Chhor",
    "districtId": "313",
    "code": "031309"
  },
  {
    "id": "31310",
    "name": "Sour Saen",
    "districtId": "313",
    "code": "031310"
  },
  {
    "id": "31311",
    "name": "Samraong",
    "districtId": "313",
    "code": "031311"
  },
  {
    "id": "31312",
    "name": "Sragnae",
    "districtId": "313",
    "code": "031312"
  },
  {
    "id": "31313",
    "name": "Thma Pun",
    "districtId": "313",
    "code": "031313"
  },
  {
    "id": "31314",
    "name": "Tong Rong",
    "districtId": "313",
    "code": "031314"
  },
  {
    "id": "31315",
    "name": "Trapeang Preah",
    "districtId": "313",
    "code": "031315"
  },
  {
    "id": "31401",
    "name": "Baray",
    "districtId": "314",
    "code": "031401"
  },
  {
    "id": "31402",
    "name": "Chi Bal",
    "districtId": "314",
    "code": "031402"
  },
  {
    "id": "31403",
    "name": "Khnar Sa",
    "districtId": "314",
    "code": "031403"
  },
  {
    "id": "31404",
    "name": "Kaoh Andaet",
    "districtId": "314",
    "code": "031404"
  },
  {
    "id": "31405",
    "name": "Mean Chey",
    "districtId": "314",
    "code": "031405"
  },
  {
    "id": "31406",
    "name": "Phteah Kandal",
    "districtId": "314",
    "code": "031406"
  },
  {
    "id": "31407",
    "name": "Pram Yam",
    "districtId": "314",
    "code": "031407"
  },
  {
    "id": "31408",
    "name": "Preaek Dambouk",
    "districtId": "314",
    "code": "031408"
  },
  {
    "id": "31409",
    "name": "Preaek Pou",
    "districtId": "314",
    "code": "031409"
  },
  {
    "id": "31410",
    "name": "Preaek Rumdeng",
    "districtId": "314",
    "code": "031410"
  },
  {
    "id": "31411",
    "name": "Ruessei Srok",
    "districtId": "314",
    "code": "031411"
  },
  {
    "id": "31412",
    "name": "Svay Pou",
    "districtId": "314",
    "code": "031412"
  },
  {
    "id": "31413",
    "name": "Svay Khsach Phnum",
    "districtId": "314",
    "code": "031413"
  },
  {
    "id": "31414",
    "name": "Tong Tralach",
    "districtId": "314",
    "code": "031414"
  },
  {
    "id": "31501",
    "name": "Areaks Tnot",
    "districtId": "315",
    "code": "031501"
  },
  {
    "id": "31503",
    "name": "Dang Kdar",
    "districtId": "315",
    "code": "031503"
  },
  {
    "id": "31504",
    "name": "Khpob Ta Nguon",
    "districtId": "315",
    "code": "031504"
  },
  {
    "id": "31505",
    "name": "Me Sar Chrey",
    "districtId": "315",
    "code": "031505"
  },
  {
    "id": "31506",
    "name": "Ou Mlu",
    "districtId": "315",
    "code": "031506"
  },
  {
    "id": "31507",
    "name": "Peam Kaoh Snar",
    "districtId": "315",
    "code": "031507"
  },
  {
    "id": "31508",
    "name": "Preah Andoung",
    "districtId": "315",
    "code": "031508"
  },
  {
    "id": "31509",
    "name": "Preaek Bak",
    "districtId": "315",
    "code": "031509"
  },
  {
    "id": "31510",
    "name": "Preak Kak",
    "districtId": "315",
    "code": "031510"
  },
  {
    "id": "31512",
    "name": "Soupheas",
    "districtId": "315",
    "code": "031512"
  },
  {
    "id": "31513",
    "name": "Tuol Preah Khleang",
    "districtId": "315",
    "code": "031513"
  },
  {
    "id": "31514",
    "name": "Tuol Sambuor",
    "districtId": "315",
    "code": "031514"
  },
  {
    "id": "40101",
    "name": "Anhchanh Rung",
    "districtId": "401",
    "code": "040101"
  },
  {
    "id": "40102",
    "name": "Chhnok Tru",
    "districtId": "401",
    "code": "040102"
  },
  {
    "id": "40103",
    "name": "Chak",
    "districtId": "401",
    "code": "040103"
  },
  {
    "id": "40104",
    "name": "Khon Rang",
    "districtId": "401",
    "code": "040104"
  },
  {
    "id": "40105",
    "name": "Kampong Preah Kokir",
    "districtId": "401",
    "code": "040105"
  },
  {
    "id": "40106",
    "name": "Melum",
    "districtId": "401",
    "code": "040106"
  },
  {
    "id": "40107",
    "name": "Phsar",
    "districtId": "401",
    "code": "040107"
  },
  {
    "id": "40108",
    "name": "Pech Changvar",
    "districtId": "401",
    "code": "040108"
  },
  {
    "id": "40109",
    "name": "Popel",
    "districtId": "401",
    "code": "040109"
  },
  {
    "id": "40110",
    "name": "Ponley",
    "districtId": "401",
    "code": "040110"
  },
  {
    "id": "40111",
    "name": "Trapeang Chan",
    "districtId": "401",
    "code": "040111"
  },
  {
    "id": "40201",
    "name": "Chol Sar",
    "districtId": "402",
    "code": "040201"
  },
  {
    "id": "40202",
    "name": "Kaoh Thkov",
    "districtId": "402",
    "code": "040202"
  },
  {
    "id": "40203",
    "name": "Kampong Ous",
    "districtId": "402",
    "code": "040203"
  },
  {
    "id": "40204",
    "name": "Peam Chhkaok",
    "districtId": "402",
    "code": "040204"
  },
  {
    "id": "40205",
    "name": "Prey Kri",
    "districtId": "402",
    "code": "040205"
  },
  {
    "id": "40301",
    "name": "Phsar Chhnang",
    "districtId": "403",
    "code": "040301"
  },
  {
    "id": "40302",
    "name": "Kampong Chhnang",
    "districtId": "403",
    "code": "040302"
  },
  {
    "id": "40303",
    "name": "B'er",
    "districtId": "403",
    "code": "040303"
  },
  {
    "id": "40304",
    "name": "Khsam",
    "districtId": "403",
    "code": "040304"
  },
  {
    "id": "40401",
    "name": "Chranouk",
    "districtId": "404",
    "code": "040401"
  },
  {
    "id": "40402",
    "name": "Dar",
    "districtId": "404",
    "code": "040402"
  },
  {
    "id": "40403",
    "name": "Kampong Hau",
    "districtId": "404",
    "code": "040403"
  },
  {
    "id": "40404",
    "name": "Phlov Tuk",
    "districtId": "404",
    "code": "040404"
  },
  {
    "id": "40405",
    "name": "Pou",
    "districtId": "404",
    "code": "040405"
  },
  {
    "id": "40406",
    "name": "Pralay Meas",
    "districtId": "404",
    "code": "040406"
  },
  {
    "id": "40407",
    "name": "Samraong Saen",
    "districtId": "404",
    "code": "040407"
  },
  {
    "id": "40408",
    "name": "Svay Rumpear",
    "districtId": "404",
    "code": "040408"
  },
  {
    "id": "40409",
    "name": "Trangel",
    "districtId": "404",
    "code": "040409"
  },
  {
    "id": "40501",
    "name": "Ampil Tuek",
    "districtId": "405",
    "code": "040501"
  },
  {
    "id": "40502",
    "name": "Chhuk Sa",
    "districtId": "405",
    "code": "040502"
  },
  {
    "id": "40503",
    "name": "Chres",
    "districtId": "405",
    "code": "040503"
  },
  {
    "id": "40504",
    "name": "Kampong Tralach",
    "districtId": "405",
    "code": "040504"
  },
  {
    "id": "40505",
    "name": "Longveaek",
    "districtId": "405",
    "code": "040505"
  },
  {
    "id": "40506",
    "name": "Ou Ruessei",
    "districtId": "405",
    "code": "040506"
  },
  {
    "id": "40507",
    "name": "Peani",
    "districtId": "405",
    "code": "040507"
  },
  {
    "id": "40508",
    "name": "Saeb",
    "districtId": "405",
    "code": "040508"
  },
  {
    "id": "40509",
    "name": "Ta Ches",
    "districtId": "405",
    "code": "040509"
  },
  {
    "id": "40510",
    "name": "Thma Edth",
    "districtId": "405",
    "code": "040510"
  },
  {
    "id": "40601",
    "name": "Andoung Snay",
    "districtId": "406",
    "code": "040601"
  },
  {
    "id": "40602",
    "name": "Banteay Preal",
    "districtId": "406",
    "code": "040602"
  },
  {
    "id": "40603",
    "name": "Cheung Kreav",
    "districtId": "406",
    "code": "040603"
  },
  {
    "id": "40604",
    "name": "Chrey Bak",
    "districtId": "406",
    "code": "040604"
  },
  {
    "id": "40605",
    "name": "Kouk Banteay",
    "districtId": "406",
    "code": "040605"
  },
  {
    "id": "40606",
    "name": "Krang Leav",
    "districtId": "406",
    "code": "040606"
  },
  {
    "id": "40607",
    "name": "Pongro",
    "districtId": "406",
    "code": "040607"
  },
  {
    "id": "40608",
    "name": "Prasnoeb",
    "districtId": "406",
    "code": "040608"
  },
  {
    "id": "40609",
    "name": "Prey Mul",
    "districtId": "406",
    "code": "040609"
  },
  {
    "id": "40610",
    "name": "Rolea B'ier",
    "districtId": "406",
    "code": "040610"
  },
  {
    "id": "40611",
    "name": "Srae Thmei",
    "districtId": "406",
    "code": "040611"
  },
  {
    "id": "40612",
    "name": "Svay Chrum",
    "districtId": "406",
    "code": "040612"
  },
  {
    "id": "40613",
    "name": "Tuek Hout",
    "districtId": "406",
    "code": "040613"
  },
  {
    "id": "40701",
    "name": "Chhean Laeung",
    "districtId": "407",
    "code": "040701"
  },
  {
    "id": "40702",
    "name": "Khnar Chhmar",
    "districtId": "407",
    "code": "040702"
  },
  {
    "id": "40703",
    "name": "Krang Lvea",
    "districtId": "407",
    "code": "040703"
  },
  {
    "id": "40704",
    "name": "Peam",
    "districtId": "407",
    "code": "040704"
  },
  {
    "id": "40705",
    "name": "Sedthei",
    "districtId": "407",
    "code": "040705"
  },
  {
    "id": "40706",
    "name": "Svay",
    "districtId": "407",
    "code": "040706"
  },
  {
    "id": "40707",
    "name": "Svay Chuk",
    "districtId": "407",
    "code": "040707"
  },
  {
    "id": "40708",
    "name": "Tbaeng Khpos",
    "districtId": "407",
    "code": "040708"
  },
  {
    "id": "40709",
    "name": "Thlok Vien",
    "districtId": "407",
    "code": "040709"
  },
  {
    "id": "40801",
    "name": "Akphivoadth",
    "districtId": "408",
    "code": "040801"
  },
  {
    "id": "40802",
    "name": "Chieb",
    "districtId": "408",
    "code": "040802"
  },
  {
    "id": "40803",
    "name": "Chaong Maong",
    "districtId": "408",
    "code": "040803"
  },
  {
    "id": "40804",
    "name": "Kbal Tuek",
    "districtId": "408",
    "code": "040804"
  },
  {
    "id": "40805",
    "name": "Khlong Popok",
    "districtId": "408",
    "code": "040805"
  },
  {
    "id": "40806",
    "name": "Krang Skear",
    "districtId": "408",
    "code": "040806"
  },
  {
    "id": "40807",
    "name": "Tang Krasang",
    "districtId": "408",
    "code": "040807"
  },
  {
    "id": "40808",
    "name": "Tuol Khpos",
    "districtId": "408",
    "code": "040808"
  },
  {
    "id": "40809",
    "name": "Kdol Saen Chey",
    "districtId": "408",
    "code": "040809"
  },
  {
    "id": "50101",
    "name": "Basedth",
    "districtId": "501",
    "code": "050101"
  },
  {
    "id": "50102",
    "name": "Kat Phluk",
    "districtId": "501",
    "code": "050102"
  },
  {
    "id": "50103",
    "name": "Nitean",
    "districtId": "501",
    "code": "050103"
  },
  {
    "id": "50104",
    "name": "Pheakdei",
    "districtId": "501",
    "code": "050104"
  },
  {
    "id": "50105",
    "name": "Pheari Mean Chey",
    "districtId": "501",
    "code": "050105"
  },
  {
    "id": "50106",
    "name": "Phong",
    "districtId": "501",
    "code": "050106"
  },
  {
    "id": "50107",
    "name": "Pou Angkrang",
    "districtId": "501",
    "code": "050107"
  },
  {
    "id": "50108",
    "name": "Pou Chamraeun",
    "districtId": "501",
    "code": "050108"
  },
  {
    "id": "50109",
    "name": "Pou Mreal",
    "districtId": "501",
    "code": "050109"
  },
  {
    "id": "50110",
    "name": "Svay Chacheb",
    "districtId": "501",
    "code": "050110"
  },
  {
    "id": "50111",
    "name": "Tuol Ampil",
    "districtId": "501",
    "code": "050111"
  },
  {
    "id": "50112",
    "name": "Tuol Sala",
    "districtId": "501",
    "code": "050112"
  },
  {
    "id": "50113",
    "name": "Kak",
    "districtId": "501",
    "code": "050113"
  },
  {
    "id": "50114",
    "name": "Svay Rumpear",
    "districtId": "501",
    "code": "050114"
  },
  {
    "id": "50115",
    "name": "Preah Khae",
    "districtId": "501",
    "code": "050115"
  },
  {
    "id": "50201",
    "name": "Chbar Mon",
    "districtId": "502",
    "code": "050201"
  },
  {
    "id": "50202",
    "name": "Kandaol Dom",
    "districtId": "502",
    "code": "050202"
  },
  {
    "id": "50203",
    "name": "Rokar Thum",
    "districtId": "502",
    "code": "050203"
  },
  {
    "id": "50204",
    "name": "Sopoar Tep",
    "districtId": "502",
    "code": "050204"
  },
  {
    "id": "50205",
    "name": "Svay Kravan",
    "districtId": "502",
    "code": "050205"
  },
  {
    "id": "50301",
    "name": "Angk Popel",
    "districtId": "503",
    "code": "050301"
  },
  {
    "id": "50302",
    "name": "Chongruk",
    "districtId": "503",
    "code": "050302"
  },
  {
    "id": "50303",
    "name": "Moha Ruessei",
    "districtId": "503",
    "code": "050303"
  },
  {
    "id": "50304",
    "name": "Pechr Muni",
    "districtId": "503",
    "code": "050304"
  },
  {
    "id": "50305",
    "name": "Preah Nipean",
    "districtId": "503",
    "code": "050305"
  },
  {
    "id": "50306",
    "name": "Prey Nheat",
    "districtId": "503",
    "code": "050306"
  },
  {
    "id": "50307",
    "name": "Prey Vihear",
    "districtId": "503",
    "code": "050307"
  },
  {
    "id": "50308",
    "name": "Roka Kaoh",
    "districtId": "503",
    "code": "050308"
  },
  {
    "id": "50309",
    "name": "Sdok",
    "districtId": "503",
    "code": "050309"
  },
  {
    "id": "50310",
    "name": "Snam Krapeu",
    "districtId": "503",
    "code": "050310"
  },
  {
    "id": "50311",
    "name": "Srang",
    "districtId": "503",
    "code": "050311"
  },
  {
    "id": "50312",
    "name": "Tuek L'ak",
    "districtId": "503",
    "code": "050312"
  },
  {
    "id": "50313",
    "name": "Veal",
    "districtId": "503",
    "code": "050313"
  },
  {
    "id": "50401",
    "name": "Haong Samnam",
    "districtId": "504",
    "code": "050401"
  },
  {
    "id": "50402",
    "name": "Reaksmei Sameakki",
    "districtId": "504",
    "code": "050402"
  },
  {
    "id": "50403",
    "name": "Trapeang Chour",
    "districtId": "504",
    "code": "050403"
  },
  {
    "id": "50404",
    "name": "Sangkae Satob",
    "districtId": "504",
    "code": "050404"
  },
  {
    "id": "50405",
    "name": "Ta Sal",
    "districtId": "504",
    "code": "050405"
  },
  {
    "id": "50501",
    "name": "Chan Saen",
    "districtId": "505",
    "code": "050501"
  },
  {
    "id": "50502",
    "name": "Cheung Roas",
    "districtId": "505",
    "code": "050502"
  },
  {
    "id": "50503",
    "name": "Chumpu Proeks",
    "districtId": "505",
    "code": "050503"
  },
  {
    "id": "50504",
    "name": "Khsem Khsant",
    "districtId": "505",
    "code": "050504"
  },
  {
    "id": "50505",
    "name": "Krang Chek",
    "districtId": "505",
    "code": "050505"
  },
  {
    "id": "50506",
    "name": "Mean Chey",
    "districtId": "505",
    "code": "050506"
  },
  {
    "id": "50507",
    "name": "Preah Srae",
    "districtId": "505",
    "code": "050507"
  },
  {
    "id": "50508",
    "name": "Prey Krasang",
    "districtId": "505",
    "code": "050508"
  },
  {
    "id": "50509",
    "name": "Trach Tong",
    "districtId": "505",
    "code": "050509"
  },
  {
    "id": "50510",
    "name": "Veal Pong",
    "districtId": "505",
    "code": "050510"
  },
  {
    "id": "50511",
    "name": "Veang Chas",
    "districtId": "505",
    "code": "050511"
  },
  {
    "id": "50512",
    "name": "Yutth Sameakki",
    "districtId": "505",
    "code": "050512"
  },
  {
    "id": "50513",
    "name": "Damnak Reang",
    "districtId": "505",
    "code": "050513"
  },
  {
    "id": "50514",
    "name": "Peang Lvea",
    "districtId": "505",
    "code": "050514"
  },
  {
    "id": "50515",
    "name": "Phnom Touch",
    "districtId": "505",
    "code": "050515"
  },
  {
    "id": "50601",
    "name": "Chambak",
    "districtId": "506",
    "code": "050601"
  },
  {
    "id": "50602",
    "name": "Choam Sangkae",
    "districtId": "506",
    "code": "050602"
  },
  {
    "id": "50603",
    "name": "Dambouk Rung",
    "districtId": "506",
    "code": "050603"
  },
  {
    "id": "50604",
    "name": "Kiri Voan",
    "districtId": "506",
    "code": "050604"
  },
  {
    "id": "50605",
    "name": "Krang Dei Vay",
    "districtId": "506",
    "code": "050605"
  },
  {
    "id": "50606",
    "name": "Moha Sang",
    "districtId": "506",
    "code": "050606"
  },
  {
    "id": "50607",
    "name": "Ou",
    "districtId": "506",
    "code": "050607"
  },
  {
    "id": "50608",
    "name": "Prey Rumduol",
    "districtId": "506",
    "code": "050608"
  },
  {
    "id": "50609",
    "name": "Prey Kmeng",
    "districtId": "506",
    "code": "050609"
  },
  {
    "id": "50610",
    "name": "Tang Samraong",
    "districtId": "506",
    "code": "050610"
  },
  {
    "id": "50611",
    "name": "Tang Sya",
    "districtId": "506",
    "code": "050611"
  },
  {
    "id": "50613",
    "name": "Traeng Trayueng",
    "districtId": "506",
    "code": "050613"
  },
  {
    "id": "50701",
    "name": "Roleang Chak",
    "districtId": "507",
    "code": "050701"
  },
  {
    "id": "50702",
    "name": "Kahaeng",
    "districtId": "507",
    "code": "050702"
  },
  {
    "id": "50703",
    "name": "Khtum Krang",
    "districtId": "507",
    "code": "050703"
  },
  {
    "id": "50704",
    "name": "Krang Ampil",
    "districtId": "507",
    "code": "050704"
  },
  {
    "id": "50705",
    "name": "Pneay",
    "districtId": "507",
    "code": "050705"
  },
  {
    "id": "50706",
    "name": "Roleang Kreul",
    "districtId": "507",
    "code": "050706"
  },
  {
    "id": "50707",
    "name": "Samrong Tong",
    "districtId": "507",
    "code": "050707"
  },
  {
    "id": "50708",
    "name": "Sambour",
    "districtId": "507",
    "code": "050708"
  },
  {
    "id": "50709",
    "name": "Saen Dei",
    "districtId": "507",
    "code": "050709"
  },
  {
    "id": "50710",
    "name": "Skuh",
    "districtId": "507",
    "code": "050710"
  },
  {
    "id": "50711",
    "name": "Tang Krouch",
    "districtId": "507",
    "code": "050711"
  },
  {
    "id": "50712",
    "name": "Thummoda Ar",
    "districtId": "507",
    "code": "050712"
  },
  {
    "id": "50713",
    "name": "Trapeang Kong",
    "districtId": "507",
    "code": "050713"
  },
  {
    "id": "50714",
    "name": "Tumpoar Meas",
    "districtId": "507",
    "code": "050714"
  },
  {
    "id": "50715",
    "name": "Voa Sar",
    "districtId": "507",
    "code": "050715"
  },
  {
    "id": "50801",
    "name": "Amleang",
    "districtId": "508",
    "code": "050801"
  },
  {
    "id": "50802",
    "name": "Monourom",
    "districtId": "508",
    "code": "050802"
  },
  {
    "id": "50804",
    "name": "Prambei Mum",
    "districtId": "508",
    "code": "050804"
  },
  {
    "id": "50805",
    "name": "Rung Roeang",
    "districtId": "508",
    "code": "050805"
  },
  {
    "id": "50806",
    "name": "Toap Mean",
    "districtId": "508",
    "code": "050806"
  },
  {
    "id": "50807",
    "name": "Veal Pon",
    "districtId": "508",
    "code": "050807"
  },
  {
    "id": "50808",
    "name": "Yea Angk",
    "districtId": "508",
    "code": "050808"
  },
  {
    "id": "60101",
    "name": "Bak Sna",
    "districtId": "601",
    "code": "060101"
  },
  {
    "id": "60102",
    "name": "Ballangk",
    "districtId": "601",
    "code": "060102"
  },
  {
    "id": "60103",
    "name": "Baray",
    "districtId": "601",
    "code": "060103"
  },
  {
    "id": "60104",
    "name": "Boeng",
    "districtId": "601",
    "code": "060104"
  },
  {
    "id": "60105",
    "name": "Chaeung Daeung",
    "districtId": "601",
    "code": "060105"
  },
  {
    "id": "60107",
    "name": "Chhuk Khsach",
    "districtId": "601",
    "code": "060107"
  },
  {
    "id": "60108",
    "name": "Chong Doung",
    "districtId": "601",
    "code": "060108"
  },
  {
    "id": "60110",
    "name": "Kokir Thum",
    "districtId": "601",
    "code": "060110"
  },
  {
    "id": "60111",
    "name": "Krava",
    "districtId": "601",
    "code": "060111"
  },
  {
    "id": "60117",
    "name": "Tnaot Chum",
    "districtId": "601",
    "code": "060117"
  },
  {
    "id": "60201",
    "name": "Chey",
    "districtId": "602",
    "code": "060201"
  },
  {
    "id": "60202",
    "name": "Damrei Slab",
    "districtId": "602",
    "code": "060202"
  },
  {
    "id": "60203",
    "name": "Kampong Kou",
    "districtId": "602",
    "code": "060203"
  },
  {
    "id": "60204",
    "name": "Kampong Svay",
    "districtId": "602",
    "code": "060204"
  },
  {
    "id": "60205",
    "name": "Nipech",
    "districtId": "602",
    "code": "060205"
  },
  {
    "id": "60206",
    "name": "Phat Sanday",
    "districtId": "602",
    "code": "060206"
  },
  {
    "id": "60207",
    "name": "San Kor",
    "districtId": "602",
    "code": "060207"
  },
  {
    "id": "60208",
    "name": "Tbaeng",
    "districtId": "602",
    "code": "060208"
  },
  {
    "id": "60209",
    "name": "Trapeang Ruessei",
    "districtId": "602",
    "code": "060209"
  },
  {
    "id": "60210",
    "name": "Kdei Doung",
    "districtId": "602",
    "code": "060210"
  },
  {
    "id": "60211",
    "name": "Prey Kuy",
    "districtId": "602",
    "code": "060211"
  },
  {
    "id": "60301",
    "name": "Damrei Choan Khla",
    "districtId": "603",
    "code": "060301"
  },
  {
    "id": "60302",
    "name": "Kampong Thum",
    "districtId": "603",
    "code": "060302"
  },
  {
    "id": "60303",
    "name": "Kampong Roteh",
    "districtId": "603",
    "code": "060303"
  },
  {
    "id": "60304",
    "name": "Ou Kanthor",
    "districtId": "603",
    "code": "060304"
  },
  {
    "id": "60306",
    "name": "Kampong Krabau",
    "districtId": "603",
    "code": "060306"
  },
  {
    "id": "60308",
    "name": "Prey Ta Hu",
    "districtId": "603",
    "code": "060308"
  },
  {
    "id": "60309",
    "name": "Achar Leak",
    "districtId": "603",
    "code": "060309"
  },
  {
    "id": "60310",
    "name": "Srayov",
    "districtId": "603",
    "code": "060310"
  },
  {
    "id": "60401",
    "name": "Doung",
    "districtId": "604",
    "code": "060401"
  },
  {
    "id": "60402",
    "name": "Kraya",
    "districtId": "604",
    "code": "060402"
  },
  {
    "id": "60403",
    "name": "Phan Nheum",
    "districtId": "604",
    "code": "060403"
  },
  {
    "id": "60404",
    "name": "Sakream",
    "districtId": "604",
    "code": "060404"
  },
  {
    "id": "60405",
    "name": "Sala Visai",
    "districtId": "604",
    "code": "060405"
  },
  {
    "id": "60406",
    "name": "Sameakki",
    "districtId": "604",
    "code": "060406"
  },
  {
    "id": "60407",
    "name": "Tuol Kreul",
    "districtId": "604",
    "code": "060407"
  },
  {
    "id": "60501",
    "name": "Chhuk",
    "districtId": "605",
    "code": "060501"
  },
  {
    "id": "60502",
    "name": "Koul",
    "districtId": "605",
    "code": "060502"
  },
  {
    "id": "60503",
    "name": "Sambour",
    "districtId": "605",
    "code": "060503"
  },
  {
    "id": "60504",
    "name": "Sraeung",
    "districtId": "605",
    "code": "060504"
  },
  {
    "id": "60505",
    "name": "Tang Krasau",
    "districtId": "605",
    "code": "060505"
  },
  {
    "id": "60601",
    "name": "Chheu Teal",
    "districtId": "606",
    "code": "060601"
  },
  {
    "id": "60602",
    "name": "Dang Kambet",
    "districtId": "606",
    "code": "060602"
  },
  {
    "id": "60603",
    "name": "Klaeng",
    "districtId": "606",
    "code": "060603"
  },
  {
    "id": "60604",
    "name": "Mean Rith",
    "districtId": "606",
    "code": "060604"
  },
  {
    "id": "60605",
    "name": "Mean Chey",
    "districtId": "606",
    "code": "060605"
  },
  {
    "id": "60606",
    "name": "Ngan",
    "districtId": "606",
    "code": "060606"
  },
  {
    "id": "60607",
    "name": "Sandan",
    "districtId": "606",
    "code": "060607"
  },
  {
    "id": "60608",
    "name": "Sochet",
    "districtId": "606",
    "code": "060608"
  },
  {
    "id": "60609",
    "name": "Tum Ring",
    "districtId": "606",
    "code": "060609"
  },
  {
    "id": "60701",
    "name": "Boeng Lvea",
    "districtId": "607",
    "code": "060701"
  },
  {
    "id": "60702",
    "name": "Chroab",
    "districtId": "607",
    "code": "060702"
  },
  {
    "id": "60703",
    "name": "Kampong Thma",
    "districtId": "607",
    "code": "060703"
  },
  {
    "id": "60704",
    "name": "Kakaoh",
    "districtId": "607",
    "code": "060704"
  },
  {
    "id": "60705",
    "name": "Kraya",
    "districtId": "607",
    "code": "060705"
  },
  {
    "id": "60706",
    "name": "Pnov",
    "districtId": "607",
    "code": "060706"
  },
  {
    "id": "60707",
    "name": "Prasat",
    "districtId": "607",
    "code": "060707"
  },
  {
    "id": "60708",
    "name": "Tang Krasang",
    "districtId": "607",
    "code": "060708"
  },
  {
    "id": "60709",
    "name": "Ti Pou",
    "districtId": "607",
    "code": "060709"
  },
  {
    "id": "60710",
    "name": "Tboung Krapeu",
    "districtId": "607",
    "code": "060710"
  },
  {
    "id": "60801",
    "name": "Banteay Stoung",
    "districtId": "608",
    "code": "060801"
  },
  {
    "id": "60802",
    "name": "Chamna Kraom",
    "districtId": "608",
    "code": "060802"
  },
  {
    "id": "60803",
    "name": "Chamna Leu",
    "districtId": "608",
    "code": "060803"
  },
  {
    "id": "60804",
    "name": "Kampong Chen Cheung",
    "districtId": "608",
    "code": "060804"
  },
  {
    "id": "60805",
    "name": "Kampong Chen Tboung",
    "districtId": "608",
    "code": "060805"
  },
  {
    "id": "60806",
    "name": "Msa Krang",
    "districtId": "608",
    "code": "060806"
  },
  {
    "id": "60807",
    "name": "Peam Bang",
    "districtId": "608",
    "code": "060807"
  },
  {
    "id": "60808",
    "name": "Popok",
    "districtId": "608",
    "code": "060808"
  },
  {
    "id": "60809",
    "name": "Pralay",
    "districtId": "608",
    "code": "060809"
  },
  {
    "id": "60810",
    "name": "Preah Damrei",
    "districtId": "608",
    "code": "060810"
  },
  {
    "id": "60811",
    "name": "Rung Roeang",
    "districtId": "608",
    "code": "060811"
  },
  {
    "id": "60812",
    "name": "Samprouch",
    "districtId": "608",
    "code": "060812"
  },
  {
    "id": "60813",
    "name": "Trea",
    "districtId": "608",
    "code": "060813"
  },
  {
    "id": "60901",
    "name": "Pongro",
    "districtId": "609",
    "code": "060901"
  },
  {
    "id": "60902",
    "name": "Chraneang",
    "districtId": "609",
    "code": "060902"
  },
  {
    "id": "60903",
    "name": "Chrolong",
    "districtId": "609",
    "code": "060903"
  },
  {
    "id": "60904",
    "name": "Triel",
    "districtId": "609",
    "code": "060904"
  },
  {
    "id": "60905",
    "name": "Sou Young",
    "districtId": "609",
    "code": "060905"
  },
  {
    "id": "60906",
    "name": "Sralau",
    "districtId": "609",
    "code": "060906"
  },
  {
    "id": "60907",
    "name": "Svay Phleung",
    "districtId": "609",
    "code": "060907"
  },
  {
    "id": "60908",
    "name": "Andoung Pou",
    "districtId": "609",
    "code": "060908"
  },
  {
    "id": "70101",
    "name": "Angk Phnum Touch",
    "districtId": "701",
    "code": "070101"
  },
  {
    "id": "70102",
    "name": "Ankor Chey",
    "districtId": "701",
    "code": "070102"
  },
  {
    "id": "70103",
    "name": "Champei",
    "districtId": "701",
    "code": "070103"
  },
  {
    "id": "70104",
    "name": "Dambouk Khpos",
    "districtId": "701",
    "code": "070104"
  },
  {
    "id": "70105",
    "name": "Dan Koum",
    "districtId": "701",
    "code": "070105"
  },
  {
    "id": "70106",
    "name": "Daeum Doung",
    "districtId": "701",
    "code": "070106"
  },
  {
    "id": "70107",
    "name": "Mroum",
    "districtId": "701",
    "code": "070107"
  },
  {
    "id": "70108",
    "name": "Phnum Kong",
    "districtId": "701",
    "code": "070108"
  },
  {
    "id": "70109",
    "name": "Praphnum",
    "districtId": "701",
    "code": "070109"
  },
  {
    "id": "70110",
    "name": "Samlanh",
    "districtId": "701",
    "code": "070110"
  },
  {
    "id": "70111",
    "name": "Tani",
    "districtId": "701",
    "code": "070111"
  },
  {
    "id": "70201",
    "name": "Banteay Meas Khang Kaeut",
    "districtId": "702",
    "code": "070201"
  },
  {
    "id": "70202",
    "name": "Banteay Meas Khang lech",
    "districtId": "702",
    "code": "070202"
  },
  {
    "id": "70203",
    "name": "Prey Tonle",
    "districtId": "702",
    "code": "070203"
  },
  {
    "id": "70204",
    "name": "Samraong Kraom",
    "districtId": "702",
    "code": "070204"
  },
  {
    "id": "70205",
    "name": "Samraong Leu",
    "districtId": "702",
    "code": "070205"
  },
  {
    "id": "70206",
    "name": "Sdach Kong Khang Cheung",
    "districtId": "702",
    "code": "070206"
  },
  {
    "id": "70207",
    "name": "Sdach Kong Khang lech",
    "districtId": "702",
    "code": "070207"
  },
  {
    "id": "70208",
    "name": "Sdach Kong Khang Tboung",
    "districtId": "702",
    "code": "070208"
  },
  {
    "id": "70209",
    "name": "Tnoat Chong Srang",
    "districtId": "702",
    "code": "070209"
  },
  {
    "id": "70210",
    "name": "Trapeang Sala Khang Kaeut",
    "districtId": "702",
    "code": "070210"
  },
  {
    "id": "70211",
    "name": "Trapeang Sala Khang Lech",
    "districtId": "702",
    "code": "070211"
  },
  {
    "id": "70212",
    "name": "Tuk Meas Khang Kaeut",
    "districtId": "702",
    "code": "070212"
  },
  {
    "id": "70213",
    "name": "Tuk Meas Khang Lech",
    "districtId": "702",
    "code": "070213"
  },
  {
    "id": "70214",
    "name": "Voat Angk Khang Cheung",
    "districtId": "702",
    "code": "070214"
  },
  {
    "id": "70215",
    "name": "Voat Angk Khang Tboung",
    "districtId": "702",
    "code": "070215"
  },
  {
    "id": "70301",
    "name": "Baniev",
    "districtId": "703",
    "code": "070301"
  },
  {
    "id": "70302",
    "name": "Takaen",
    "districtId": "703",
    "code": "070302"
  },
  {
    "id": "70303",
    "name": "Boeng Nimol",
    "districtId": "703",
    "code": "070303"
  },
  {
    "id": "70304",
    "name": "Chhuk",
    "districtId": "703",
    "code": "070304"
  },
  {
    "id": "70305",
    "name": "Doun Yay",
    "districtId": "703",
    "code": "070305"
  },
  {
    "id": "70306",
    "name": "Krang Sbov",
    "districtId": "703",
    "code": "070306"
  },
  {
    "id": "70307",
    "name": "Krang Snay",
    "districtId": "703",
    "code": "070307"
  },
  {
    "id": "70308",
    "name": "Lbaeuk",
    "districtId": "703",
    "code": "070308"
  },
  {
    "id": "70309",
    "name": "Trapeang Phleang",
    "districtId": "703",
    "code": "070309"
  },
  {
    "id": "70310",
    "name": "Mean Chey",
    "districtId": "703",
    "code": "070310"
  },
  {
    "id": "70311",
    "name": "Neareay",
    "districtId": "703",
    "code": "070311"
  },
  {
    "id": "70312",
    "name": "Satv Pong",
    "districtId": "703",
    "code": "070312"
  },
  {
    "id": "70313",
    "name": "Trapeang Bei",
    "districtId": "703",
    "code": "070313"
  },
  {
    "id": "70314",
    "name": "Tramaeng",
    "districtId": "703",
    "code": "070314"
  },
  {
    "id": "70315",
    "name": "Dechou Akphivoadth",
    "districtId": "703",
    "code": "070315"
  },
  {
    "id": "70401",
    "name": "Chres",
    "districtId": "704",
    "code": "070401"
  },
  {
    "id": "70402",
    "name": "Chumpu Voan",
    "districtId": "704",
    "code": "070402"
  },
  {
    "id": "70403",
    "name": "Snay Anhchit",
    "districtId": "704",
    "code": "070403"
  },
  {
    "id": "70404",
    "name": "Srae Chaeng",
    "districtId": "704",
    "code": "070404"
  },
  {
    "id": "70405",
    "name": "Srae Knong",
    "districtId": "704",
    "code": "070405"
  },
  {
    "id": "70406",
    "name": "Srae Samraong",
    "districtId": "704",
    "code": "070406"
  },
  {
    "id": "70407",
    "name": "Trapeang Reang",
    "districtId": "704",
    "code": "070407"
  },
  {
    "id": "70501",
    "name": "Damnak Sokram",
    "districtId": "705",
    "code": "070501"
  },
  {
    "id": "70502",
    "name": "Dang Tong",
    "districtId": "705",
    "code": "070502"
  },
  {
    "id": "70503",
    "name": "Khcheay Khang Cheung",
    "districtId": "705",
    "code": "070503"
  },
  {
    "id": "70504",
    "name": "Khcheay Khang Tboung",
    "districtId": "705",
    "code": "070504"
  },
  {
    "id": "70505",
    "name": "Mean Ritth",
    "districtId": "705",
    "code": "070505"
  },
  {
    "id": "70506",
    "name": "Srae Chea Khang Cheung",
    "districtId": "705",
    "code": "070506"
  },
  {
    "id": "70507",
    "name": "Srae Chea Khang Tboung",
    "districtId": "705",
    "code": "070507"
  },
  {
    "id": "70508",
    "name": "Totung",
    "districtId": "705",
    "code": "070508"
  },
  {
    "id": "70509",
    "name": "Angk  Romeas",
    "districtId": "705",
    "code": "070509"
  },
  {
    "id": "70510",
    "name": "L'ang",
    "districtId": "705",
    "code": "070510"
  },
  {
    "id": "70601",
    "name": "Boeng Sala Khang Cheung",
    "districtId": "706",
    "code": "070601"
  },
  {
    "id": "70602",
    "name": "Boeng Sala Khang Tboung",
    "districtId": "706",
    "code": "070602"
  },
  {
    "id": "70603",
    "name": "Damnak Kantuot Khang Cheung",
    "districtId": "706",
    "code": "070603"
  },
  {
    "id": "70604",
    "name": "Damnak Kantuot Khang Tboung",
    "districtId": "706",
    "code": "070604"
  },
  {
    "id": "70605",
    "name": "Kampong Trach Khang Kaeut",
    "districtId": "706",
    "code": "070605"
  },
  {
    "id": "70606",
    "name": "Kampong Trach Khang Lech",
    "districtId": "706",
    "code": "070606"
  },
  {
    "id": "70607",
    "name": "Prasat Phnom Khyang",
    "districtId": "706",
    "code": "070607"
  },
  {
    "id": "70608",
    "name": "Phnom Prasat",
    "districtId": "706",
    "code": "070608"
  },
  {
    "id": "70609",
    "name": "Ang Sophy",
    "districtId": "706",
    "code": "070609"
  },
  {
    "id": "70612",
    "name": "Preaek Kroes",
    "districtId": "706",
    "code": "070612"
  },
  {
    "id": "70613",
    "name": "Ruessei Srok Khang Kaeut",
    "districtId": "706",
    "code": "070613"
  },
  {
    "id": "70614",
    "name": "Ruessei Srok Khang Lech",
    "districtId": "706",
    "code": "070614"
  },
  {
    "id": "70615",
    "name": "Svay Tong Khang Cheung",
    "districtId": "706",
    "code": "070615"
  },
  {
    "id": "70616",
    "name": "Svay Tong Khang Tboung",
    "districtId": "706",
    "code": "070616"
  },
  {
    "id": "70701",
    "name": "Boeng Tuk",
    "districtId": "707",
    "code": "070701"
  },
  {
    "id": "70702",
    "name": "Chum Kriel",
    "districtId": "707",
    "code": "070702"
  },
  {
    "id": "70703",
    "name": "Kampong Kraeng",
    "districtId": "707",
    "code": "070703"
  },
  {
    "id": "70704",
    "name": "Kampong Samraong",
    "districtId": "707",
    "code": "070704"
  },
  {
    "id": "70705",
    "name": "Kandaol",
    "districtId": "707",
    "code": "070705"
  },
  {
    "id": "70707",
    "name": "Kaoh Touch",
    "districtId": "707",
    "code": "070707"
  },
  {
    "id": "70708",
    "name": "Koun Satv",
    "districtId": "707",
    "code": "070708"
  },
  {
    "id": "70709",
    "name": "Makprang",
    "districtId": "707",
    "code": "070709"
  },
  {
    "id": "70711",
    "name": "Preaek Tnoat",
    "districtId": "707",
    "code": "070711"
  },
  {
    "id": "70712",
    "name": "Prey Khmum",
    "districtId": "707",
    "code": "070712"
  },
  {
    "id": "70713",
    "name": "Prey Thnang",
    "districtId": "707",
    "code": "070713"
  },
  {
    "id": "70715",
    "name": "Stueng Kaev",
    "districtId": "707",
    "code": "070715"
  },
  {
    "id": "70716",
    "name": "Thmei",
    "districtId": "707",
    "code": "070716"
  },
  {
    "id": "70717",
    "name": "Trapeang Pring",
    "districtId": "707",
    "code": "070717"
  },
  {
    "id": "70718",
    "name": "Trapeang Sangkae",
    "districtId": "707",
    "code": "070718"
  },
  {
    "id": "70719",
    "name": "Trapeang Thum",
    "districtId": "707",
    "code": "070719"
  },
  {
    "id": "70801",
    "name": "Kampong Kandal",
    "districtId": "708",
    "code": "070801"
  },
  {
    "id": "70802",
    "name": "Krang Ampil",
    "districtId": "708",
    "code": "070802"
  },
  {
    "id": "70803",
    "name": "Kampong Bay",
    "districtId": "708",
    "code": "070803"
  },
  {
    "id": "70804",
    "name": "Andoung Khmer",
    "districtId": "708",
    "code": "070804"
  },
  {
    "id": "70805",
    "name": "Traeuy Kaoh",
    "districtId": "708",
    "code": "070805"
  },
  {
    "id": "80101",
    "name": "Ampov Prey",
    "districtId": "801",
    "code": "080101"
  },
  {
    "id": "80102",
    "name": "Anlong Romiet",
    "districtId": "801",
    "code": "080102"
  },
  {
    "id": "80103",
    "name": "Barku",
    "districtId": "801",
    "code": "080103"
  },
  {
    "id": "80104",
    "name": "Boeng Khyang",
    "districtId": "801",
    "code": "080104"
  },
  {
    "id": "80105",
    "name": "Cheung Kaeub",
    "districtId": "801",
    "code": "080105"
  },
  {
    "id": "80106",
    "name": "Daeum Rues",
    "districtId": "801",
    "code": "080106"
  },
  {
    "id": "80107",
    "name": "Kandaok",
    "districtId": "801",
    "code": "080107"
  },
  {
    "id": "80108",
    "name": "Thmei",
    "districtId": "801",
    "code": "080108"
  },
  {
    "id": "80109",
    "name": "Kouk Trab",
    "districtId": "801",
    "code": "080109"
  },
  {
    "id": "80113",
    "name": "Preah Putth",
    "districtId": "801",
    "code": "080113"
  },
  {
    "id": "80115",
    "name": "Preaek Roka",
    "districtId": "801",
    "code": "080115"
  },
  {
    "id": "80116",
    "name": "Preaek Slaeng",
    "districtId": "801",
    "code": "080116"
  },
  {
    "id": "80117",
    "name": "Roka",
    "districtId": "801",
    "code": "080117"
  },
  {
    "id": "80118",
    "name": "Roleang Kaen",
    "districtId": "801",
    "code": "080118"
  },
  {
    "id": "80122",
    "name": "Siem Reab",
    "districtId": "801",
    "code": "080122"
  },
  {
    "id": "80125",
    "name": "Tbaeng",
    "districtId": "801",
    "code": "080125"
  },
  {
    "id": "80127",
    "name": "Trapeang Veaeng",
    "districtId": "801",
    "code": "080127"
  },
  {
    "id": "80128",
    "name": "Trea",
    "districtId": "801",
    "code": "080128"
  },
  {
    "id": "80201",
    "name": "Banteay Daek",
    "districtId": "802",
    "code": "080201"
  },
  {
    "id": "80202",
    "name": "Chheu Teal",
    "districtId": "802",
    "code": "080202"
  },
  {
    "id": "80203",
    "name": "Dei Edth",
    "districtId": "802",
    "code": "080203"
  },
  {
    "id": "80204",
    "name": "Kampong Svay",
    "districtId": "802",
    "code": "080204"
  },
  {
    "id": "80206",
    "name": "Kokir",
    "districtId": "802",
    "code": "080206"
  },
  {
    "id": "80207",
    "name": "Kokir Thum",
    "districtId": "802",
    "code": "080207"
  },
  {
    "id": "80208",
    "name": "Phum Thum",
    "districtId": "802",
    "code": "080208"
  },
  {
    "id": "80211",
    "name": "Samraong Thum",
    "districtId": "802",
    "code": "080211"
  },
  {
    "id": "80301",
    "name": "Bak Dav",
    "districtId": "803",
    "code": "080301"
  },
  {
    "id": "80302",
    "name": "Chey Thum",
    "districtId": "803",
    "code": "080302"
  },
  {
    "id": "80303",
    "name": "Kampong Chamlang",
    "districtId": "803",
    "code": "080303"
  },
  {
    "id": "80304",
    "name": "Kaoh Chouram",
    "districtId": "803",
    "code": "080304"
  },
  {
    "id": "80305",
    "name": "Kaoh Oknha Tei",
    "districtId": "803",
    "code": "080305"
  },
  {
    "id": "80306",
    "name": "Preah Prasab",
    "districtId": "803",
    "code": "080306"
  },
  {
    "id": "80307",
    "name": "Preaek Ampil",
    "districtId": "803",
    "code": "080307"
  },
  {
    "id": "80308",
    "name": "Preaek Luong",
    "districtId": "803",
    "code": "080308"
  },
  {
    "id": "80309",
    "name": "Preaek Ta kov",
    "districtId": "803",
    "code": "080309"
  },
  {
    "id": "80310",
    "name": "Preaek Ta Meak",
    "districtId": "803",
    "code": "080310"
  },
  {
    "id": "80311",
    "name": "Puk Ruessei",
    "districtId": "803",
    "code": "080311"
  },
  {
    "id": "80312",
    "name": "Roka Chonlueng",
    "districtId": "803",
    "code": "080312"
  },
  {
    "id": "80313",
    "name": "Sanlung",
    "districtId": "803",
    "code": "080313"
  },
  {
    "id": "80314",
    "name": "Sithor",
    "districtId": "803",
    "code": "080314"
  },
  {
    "id": "80315",
    "name": "Svay Chrum",
    "districtId": "803",
    "code": "080315"
  },
  {
    "id": "80316",
    "name": "Svay Romiet",
    "districtId": "803",
    "code": "080316"
  },
  {
    "id": "80317",
    "name": "Ta Aek",
    "districtId": "803",
    "code": "080317"
  },
  {
    "id": "80318",
    "name": "Vihear Suork",
    "districtId": "803",
    "code": "080318"
  },
  {
    "id": "80401",
    "name": "Chheu Kmau",
    "districtId": "804",
    "code": "080401"
  },
  {
    "id": "80402",
    "name": "Chrouy Ta Kaev",
    "districtId": "804",
    "code": "080402"
  },
  {
    "id": "80403",
    "name": "Kampong Kong",
    "districtId": "804",
    "code": "080403"
  },
  {
    "id": "80404",
    "name": "Kaoh Thum Ka",
    "districtId": "804",
    "code": "080404"
  },
  {
    "id": "80405",
    "name": "Kaoh Thum Kha",
    "districtId": "804",
    "code": "080405"
  },
  {
    "id": "80407",
    "name": "Leuk Daek",
    "districtId": "804",
    "code": "080407"
  },
  {
    "id": "80408",
    "name": "Pouthi Ban",
    "districtId": "804",
    "code": "080408"
  },
  {
    "id": "80409",
    "name": "Prea​ek Chrey",
    "districtId": "804",
    "code": "080409"
  },
  {
    "id": "80410",
    "name": "Preaek Sdei",
    "districtId": "804",
    "code": "080410"
  },
  {
    "id": "80411",
    "name": "Preaek Thmei",
    "districtId": "804",
    "code": "080411"
  },
  {
    "id": "80412",
    "name": "Sampeou Poun",
    "districtId": "804",
    "code": "080412"
  },
  {
    "id": "80501",
    "name": "Kampong Phnum",
    "districtId": "805",
    "code": "080501"
  },
  {
    "id": "80502",
    "name": "K'am Samnar",
    "districtId": "805",
    "code": "080502"
  },
  {
    "id": "80503",
    "name": "Khpob Ateav",
    "districtId": "805",
    "code": "080503"
  },
  {
    "id": "80504",
    "name": "Peam Reang",
    "districtId": "805",
    "code": "080504"
  },
  {
    "id": "80505",
    "name": "Preaek Dach",
    "districtId": "805",
    "code": "080505"
  },
  {
    "id": "80506",
    "name": "Preaek Tonloab",
    "districtId": "805",
    "code": "080506"
  },
  {
    "id": "80507",
    "name": "Sandar",
    "districtId": "805",
    "code": "080507"
  },
  {
    "id": "80601",
    "name": "Akreiy Ksatr",
    "districtId": "806",
    "code": "080601"
  },
  {
    "id": "80602",
    "name": "Barong",
    "districtId": "806",
    "code": "080602"
  },
  {
    "id": "80603",
    "name": "Boeng Krum",
    "districtId": "806",
    "code": "080603"
  },
  {
    "id": "80604",
    "name": "Kaoh Kaev",
    "districtId": "806",
    "code": "080604"
  },
  {
    "id": "80605",
    "name": "Kaoh Reah",
    "districtId": "806",
    "code": "080605"
  },
  {
    "id": "80606",
    "name": "Lvea Sar",
    "districtId": "806",
    "code": "080606"
  },
  {
    "id": "80607",
    "name": "Peam Oknha Ong",
    "districtId": "806",
    "code": "080607"
  },
  {
    "id": "80608",
    "name": "Phum Thum",
    "districtId": "806",
    "code": "080608"
  },
  {
    "id": "80609",
    "name": "Preaek Kmeng",
    "districtId": "806",
    "code": "080609"
  },
  {
    "id": "80610",
    "name": "Preaek Rey",
    "districtId": "806",
    "code": "080610"
  },
  {
    "id": "80611",
    "name": "Preaek Ruessei",
    "districtId": "806",
    "code": "080611"
  },
  {
    "id": "80612",
    "name": "Sambuor",
    "districtId": "806",
    "code": "080612"
  },
  {
    "id": "80613",
    "name": "Sarikakaev",
    "districtId": "806",
    "code": "080613"
  },
  {
    "id": "80614",
    "name": "Thma Kor",
    "districtId": "806",
    "code": "080614"
  },
  {
    "id": "80615",
    "name": "Tuek Khleang",
    "districtId": "806",
    "code": "080615"
  },
  {
    "id": "80703",
    "name": "Preaek Anhchanh",
    "districtId": "807",
    "code": "080703"
  },
  {
    "id": "80704",
    "name": "Preaek Dambang",
    "districtId": "807",
    "code": "080704"
  },
  {
    "id": "80707",
    "name": "Roka Kong Ti Muoy",
    "districtId": "807",
    "code": "080707"
  },
  {
    "id": "80708",
    "name": "Roka Kong Ti Pir",
    "districtId": "807",
    "code": "080708"
  },
  {
    "id": "80709",
    "name": "Ruessei Chrouy",
    "districtId": "807",
    "code": "080709"
  },
  {
    "id": "80710",
    "name": "Sambuor Meas",
    "districtId": "807",
    "code": "080710"
  },
  {
    "id": "80711",
    "name": "Svay Ampear",
    "districtId": "807",
    "code": "080711"
  },
  {
    "id": "80801",
    "name": "Baek Chan",
    "districtId": "808",
    "code": "080801"
  },
  {
    "id": "80803",
    "name": "Chhak Chheu Neang",
    "districtId": "808",
    "code": "080803"
  },
  {
    "id": "80804",
    "name": "Damnak Ampil",
    "districtId": "808",
    "code": "080804"
  },
  {
    "id": "80807",
    "name": "Krang Mkak",
    "districtId": "808",
    "code": "080807"
  },
  {
    "id": "80808",
    "name": "Lumhach",
    "districtId": "808",
    "code": "080808"
  },
  {
    "id": "80809",
    "name": "Mkak",
    "districtId": "808",
    "code": "080809"
  },
  {
    "id": "80811",
    "name": "Peuk",
    "districtId": "808",
    "code": "080811"
  },
  {
    "id": "80813",
    "name": "Prey Puoch",
    "districtId": "808",
    "code": "080813"
  },
  {
    "id": "80814",
    "name": "Samraong Leu",
    "districtId": "808",
    "code": "080814"
  },
  {
    "id": "80816",
    "name": "Tuol Prech",
    "districtId": "808",
    "code": "080816"
  },
  {
    "id": "80901",
    "name": "Chhveang",
    "districtId": "809",
    "code": "080901"
  },
  {
    "id": "80902",
    "name": "Chrey Loas",
    "districtId": "809",
    "code": "080902"
  },
  {
    "id": "80903",
    "name": "Kampong Luong",
    "districtId": "809",
    "code": "080903"
  },
  {
    "id": "80904",
    "name": "Kampong Os",
    "districtId": "809",
    "code": "080904"
  },
  {
    "id": "80905",
    "name": "Kaoh Chen",
    "districtId": "809",
    "code": "080905"
  },
  {
    "id": "80906",
    "name": "Phnum Bat",
    "districtId": "809",
    "code": "080906"
  },
  {
    "id": "80907",
    "name": "Ponhea Lueu",
    "districtId": "809",
    "code": "080907"
  },
  {
    "id": "80910",
    "name": "Preaek Ta Teaen",
    "districtId": "809",
    "code": "080910"
  },
  {
    "id": "80911",
    "name": "Phsar Daek",
    "districtId": "809",
    "code": "080911"
  },
  {
    "id": "80913",
    "name": "Tumnob Thum",
    "districtId": "809",
    "code": "080913"
  },
  {
    "id": "80914",
    "name": "Vihear Luong",
    "districtId": "809",
    "code": "080914"
  },
  {
    "id": "81001",
    "name": "Khpob",
    "districtId": "810",
    "code": "081001"
  },
  {
    "id": "81003",
    "name": "Kaoh Khael",
    "districtId": "810",
    "code": "081003"
  },
  {
    "id": "81004",
    "name": "Kaoh Khsach Tonlea",
    "districtId": "810",
    "code": "081004"
  },
  {
    "id": "81005",
    "name": "Krang Yov",
    "districtId": "810",
    "code": "081005"
  },
  {
    "id": "81006",
    "name": "Prasat",
    "districtId": "810",
    "code": "081006"
  },
  {
    "id": "81007",
    "name": "Preaek Ambel",
    "districtId": "810",
    "code": "081007"
  },
  {
    "id": "81008",
    "name": "Preaek Koy",
    "districtId": "810",
    "code": "081008"
  },
  {
    "id": "81010",
    "name": "S'ang Phnum",
    "districtId": "810",
    "code": "081010"
  },
  {
    "id": "81012",
    "name": "Svay Prateal",
    "districtId": "810",
    "code": "081012"
  },
  {
    "id": "81014",
    "name": "Ta Lon",
    "districtId": "810",
    "code": "081014"
  },
  {
    "id": "81015",
    "name": "Traeuy Sla",
    "districtId": "810",
    "code": "081015"
  },
  {
    "id": "81016",
    "name": "Tuek Vil",
    "districtId": "810",
    "code": "081016"
  },
  {
    "id": "81101",
    "name": "Ta Kdol",
    "districtId": "811",
    "code": "081101"
  },
  {
    "id": "81102",
    "name": "Prek Ruessey",
    "districtId": "811",
    "code": "081102"
  },
  {
    "id": "81103",
    "name": "Doeum Mien",
    "districtId": "811",
    "code": "081103"
  },
  {
    "id": "81104",
    "name": "Ta Khmao",
    "districtId": "811",
    "code": "081104"
  },
  {
    "id": "81105",
    "name": "Prek Ho",
    "districtId": "811",
    "code": "081105"
  },
  {
    "id": "81106",
    "name": "Kampong Samnanh",
    "districtId": "811",
    "code": "081106"
  },
  {
    "id": "81107",
    "name": "Svay Rolum",
    "districtId": "811",
    "code": "081107"
  },
  {
    "id": "81108",
    "name": "Kaoh Anlong Chen",
    "districtId": "811",
    "code": "081108"
  },
  {
    "id": "81109",
    "name": "Setbou",
    "districtId": "811",
    "code": "081109"
  },
  {
    "id": "81110",
    "name": "Roka Khpos",
    "districtId": "811",
    "code": "081110"
  },
  {
    "id": "90101",
    "name": "Andoung Tuek",
    "districtId": "901",
    "code": "090101"
  },
  {
    "id": "90102",
    "name": "Kandaol",
    "districtId": "901",
    "code": "090102"
  },
  {
    "id": "90103",
    "name": "Ta Noun",
    "districtId": "901",
    "code": "090103"
  },
  {
    "id": "90104",
    "name": "Thma Sa",
    "districtId": "901",
    "code": "090104"
  },
  {
    "id": "90201",
    "name": "Kaoh Sdach",
    "districtId": "902",
    "code": "090201"
  },
  {
    "id": "90202",
    "name": "Phnhi Meas",
    "districtId": "902",
    "code": "090202"
  },
  {
    "id": "90203",
    "name": "Preaek Khsach",
    "districtId": "902",
    "code": "090203"
  },
  {
    "id": "90301",
    "name": "Chrouy Pras",
    "districtId": "903",
    "code": "090301"
  },
  {
    "id": "90302",
    "name": "Kaoh Kapi",
    "districtId": "903",
    "code": "090302"
  },
  {
    "id": "90303",
    "name": "Ta Tai Kraom",
    "districtId": "903",
    "code": "090303"
  },
  {
    "id": "90304",
    "name": "Trapeang Rung",
    "districtId": "903",
    "code": "090304"
  },
  {
    "id": "90401",
    "name": "Smach Mean Chey",
    "districtId": "904",
    "code": "090401"
  },
  {
    "id": "90402",
    "name": "Dang Tong",
    "districtId": "904",
    "code": "090402"
  },
  {
    "id": "90403",
    "name": "Stueng Veaeng",
    "districtId": "904",
    "code": "090403"
  },
  {
    "id": "90501",
    "name": "Pak Khlang",
    "districtId": "905",
    "code": "090501"
  },
  {
    "id": "90502",
    "name": "Peam Krasaob",
    "districtId": "905",
    "code": "090502"
  },
  {
    "id": "90503",
    "name": "Tuol Kokir",
    "districtId": "905",
    "code": "090503"
  },
  {
    "id": "90601",
    "name": "Boeng Preav",
    "districtId": "906",
    "code": "090601"
  },
  {
    "id": "90602",
    "name": "Chi Kha Kraom",
    "districtId": "906",
    "code": "090602"
  },
  {
    "id": "90603",
    "name": "Chi kha Leu",
    "districtId": "906",
    "code": "090603"
  },
  {
    "id": "90604",
    "name": "Chrouy Svay",
    "districtId": "906",
    "code": "090604"
  },
  {
    "id": "90605",
    "name": "Dang Peaeng",
    "districtId": "906",
    "code": "090605"
  },
  {
    "id": "90606",
    "name": "Srae Ambel",
    "districtId": "906",
    "code": "090606"
  },
  {
    "id": "90701",
    "name": "Ta Tey Leu",
    "districtId": "907",
    "code": "090701"
  },
  {
    "id": "90702",
    "name": "Pralay",
    "districtId": "907",
    "code": "090702"
  },
  {
    "id": "90703",
    "name": "Chumnoab",
    "districtId": "907",
    "code": "090703"
  },
  {
    "id": "90704",
    "name": "Ruessei Chrum",
    "districtId": "907",
    "code": "090704"
  },
  {
    "id": "90705",
    "name": "Chi Phat",
    "districtId": "907",
    "code": "090705"
  },
  {
    "id": "90706",
    "name": "Thma Doun Pov",
    "districtId": "907",
    "code": "090706"
  },
  {
    "id": "100101",
    "name": "Chhloung",
    "districtId": "1001",
    "code": "100101"
  },
  {
    "id": "100102",
    "name": "Damrei Phong",
    "districtId": "1001",
    "code": "100102"
  },
  {
    "id": "100103",
    "name": "Han Chey",
    "districtId": "1001",
    "code": "100103"
  },
  {
    "id": "100104",
    "name": "Kampong Damrei",
    "districtId": "1001",
    "code": "100104"
  },
  {
    "id": "100105",
    "name": "Kanhchor",
    "districtId": "1001",
    "code": "100105"
  },
  {
    "id": "100106",
    "name": "Khsach Andeth",
    "districtId": "1001",
    "code": "100106"
  },
  {
    "id": "100107",
    "name": "Pongro",
    "districtId": "1001",
    "code": "100107"
  },
  {
    "id": "100108",
    "name": "Preaek Saman",
    "districtId": "1001",
    "code": "100108"
  },
  {
    "id": "100207",
    "name": "Kaoh Trong",
    "districtId": "1002",
    "code": "100207"
  },
  {
    "id": "100208",
    "name": "Krakor",
    "districtId": "1002",
    "code": "100208"
  },
  {
    "id": "100209",
    "name": "Kracheh",
    "districtId": "1002",
    "code": "100209"
  },
  {
    "id": "100210",
    "name": "Ou Ruessei",
    "districtId": "1002",
    "code": "100210"
  },
  {
    "id": "100211",
    "name": "Roka Kandal",
    "districtId": "1002",
    "code": "100211"
  },
  {
    "id": "100301",
    "name": "Chambâk",
    "districtId": "1003",
    "code": "100301"
  },
  {
    "id": "100302",
    "name": "Chrouy Banteay",
    "districtId": "1003",
    "code": "100302"
  },
  {
    "id": "100303",
    "name": "Kampong Kor",
    "districtId": "1003",
    "code": "100303"
  },
  {
    "id": "100304",
    "name": "Koh Ta Suy",
    "districtId": "1003",
    "code": "100304"
  },
  {
    "id": "100305",
    "name": "Preaek Prasab",
    "districtId": "1003",
    "code": "100305"
  },
  {
    "id": "100306",
    "name": "Russey Keo",
    "districtId": "1003",
    "code": "100306"
  },
  {
    "id": "100307",
    "name": "Saob",
    "districtId": "1003",
    "code": "100307"
  },
  {
    "id": "100308",
    "name": "Ta Mao",
    "districtId": "1003",
    "code": "100308"
  },
  {
    "id": "100401",
    "name": "Boeng Char",
    "districtId": "1004",
    "code": "100401"
  },
  {
    "id": "100402",
    "name": "Kampong Cham",
    "districtId": "1004",
    "code": "100402"
  },
  {
    "id": "100403",
    "name": "Kbal Damrei",
    "districtId": "1004",
    "code": "100403"
  },
  {
    "id": "100404",
    "name": "Kaoh Khnhaer",
    "districtId": "1004",
    "code": "100404"
  },
  {
    "id": "100405",
    "name": "Ou Krieng",
    "districtId": "1004",
    "code": "100405"
  },
  {
    "id": "100406",
    "name": "Roluos Mean Chey",
    "districtId": "1004",
    "code": "100406"
  },
  {
    "id": "100407",
    "name": "Sambour",
    "districtId": "1004",
    "code": "100407"
  },
  {
    "id": "100408",
    "name": "Sandan",
    "districtId": "1004",
    "code": "100408"
  },
  {
    "id": "100409",
    "name": "Srae Chis",
    "districtId": "1004",
    "code": "100409"
  },
  {
    "id": "100410",
    "name": "Voadthonak",
    "districtId": "1004",
    "code": "100410"
  },
  {
    "id": "100501",
    "name": "Khsuem",
    "districtId": "1005",
    "code": "100501"
  },
  {
    "id": "100502",
    "name": "Pir Thnu",
    "districtId": "1005",
    "code": "100502"
  },
  {
    "id": "100503",
    "name": "Snuol",
    "districtId": "1005",
    "code": "100503"
  },
  {
    "id": "100504",
    "name": "Srae Char",
    "districtId": "1005",
    "code": "100504"
  },
  {
    "id": "100505",
    "name": "Svay Chreah",
    "districtId": "1005",
    "code": "100505"
  },
  {
    "id": "100506",
    "name": "Kronhoung Saen Chey",
    "districtId": "1005",
    "code": "100506"
  },
  {
    "id": "100601",
    "name": "Bos Leav",
    "districtId": "1006",
    "code": "100601"
  },
  {
    "id": "100602",
    "name": "Changkrang",
    "districtId": "1006",
    "code": "100602"
  },
  {
    "id": "100603",
    "name": "Dar",
    "districtId": "1006",
    "code": "100603"
  },
  {
    "id": "100604",
    "name": "Kantuot",
    "districtId": "1006",
    "code": "100604"
  },
  {
    "id": "100605",
    "name": "Kou Loab",
    "districtId": "1006",
    "code": "100605"
  },
  {
    "id": "100606",
    "name": "Kaoh Chraeng",
    "districtId": "1006",
    "code": "100606"
  },
  {
    "id": "100607",
    "name": "Sambok",
    "districtId": "1006",
    "code": "100607"
  },
  {
    "id": "100608",
    "name": "Thma Andaeuk",
    "districtId": "1006",
    "code": "100608"
  },
  {
    "id": "100609",
    "name": "Thma Kreae",
    "districtId": "1006",
    "code": "100609"
  },
  {
    "id": "100610",
    "name": "Thmei",
    "districtId": "1006",
    "code": "100610"
  },
  {
    "id": "110101",
    "name": "Chong Phlah",
    "districtId": "1101",
    "code": "110101"
  },
  {
    "id": "110102",
    "name": "Memang",
    "districtId": "1101",
    "code": "110102"
  },
  {
    "id": "110103",
    "name": "Srae Chhuk",
    "districtId": "1101",
    "code": "110103"
  },
  {
    "id": "110104",
    "name": "Srae Khtum",
    "districtId": "1101",
    "code": "110104"
  },
  {
    "id": "110105",
    "name": "Srae Preah",
    "districtId": "1101",
    "code": "110105"
  },
  {
    "id": "110201",
    "name": "Nang Khi Lik",
    "districtId": "1102",
    "code": "110201"
  },
  {
    "id": "110202",
    "name": "A Buon Leu",
    "districtId": "1102",
    "code": "110202"
  },
  {
    "id": "110203",
    "name": "Roya",
    "districtId": "1102",
    "code": "110203"
  },
  {
    "id": "110204",
    "name": "Sokh Sant",
    "districtId": "1102",
    "code": "110204"
  },
  {
    "id": "110205",
    "name": "Srae Huy",
    "districtId": "1102",
    "code": "110205"
  },
  {
    "id": "110206",
    "name": "Srae Sangkum",
    "districtId": "1102",
    "code": "110206"
  },
  {
    "id": "110301",
    "name": "Dak Dam",
    "districtId": "1103",
    "code": "110301"
  },
  {
    "id": "110302",
    "name": "Saen Monourom",
    "districtId": "1103",
    "code": "110302"
  },
  {
    "id": "110401",
    "name": "Krang Teh",
    "districtId": "1104",
    "code": "110401"
  },
  {
    "id": "110402",
    "name": "Pu Chrey",
    "districtId": "1104",
    "code": "110402"
  },
  {
    "id": "110403",
    "name": "Srae Ampum",
    "districtId": "1104",
    "code": "110403"
  },
  {
    "id": "110404",
    "name": "Bu Sra",
    "districtId": "1104",
    "code": "110404"
  },
  {
    "id": "110501",
    "name": "Monourom",
    "districtId": "1105",
    "code": "110501"
  },
  {
    "id": "110502",
    "name": "Sokh Dom",
    "districtId": "1105",
    "code": "110502"
  },
  {
    "id": "110503",
    "name": "Spean Mean Chey",
    "districtId": "1105",
    "code": "110503"
  },
  {
    "id": "110504",
    "name": "Romonea",
    "districtId": "1105",
    "code": "110504"
  },
  {
    "id": "120101",
    "name": "Tonle Basak",
    "districtId": "1201",
    "code": "120101"
  },
  {
    "id": "120109",
    "name": "Tuol Tumpung Ti Pir",
    "districtId": "1201",
    "code": "120109"
  },
  {
    "id": "120110",
    "name": "Tuol Tumpung Ti Muoy",
    "districtId": "1201",
    "code": "120110"
  },
  {
    "id": "120111",
    "name": "Boeng Trabaek",
    "districtId": "1201",
    "code": "120111"
  },
  {
    "id": "120112",
    "name": "Phsar Daeum Thkov",
    "districtId": "1201",
    "code": "120112"
  },
  {
    "id": "120201",
    "name": "Phsar Thmei Ti Muoy",
    "districtId": "1202",
    "code": "120201"
  },
  {
    "id": "120202",
    "name": "Phsar Thmei Ti Pir",
    "districtId": "1202",
    "code": "120202"
  },
  {
    "id": "120203",
    "name": "Phsar Thmei Ti Bei",
    "districtId": "1202",
    "code": "120203"
  },
  {
    "id": "120204",
    "name": "Boeng Reang",
    "districtId": "1202",
    "code": "120204"
  },
  {
    "id": "120205",
    "name": "Phsar Kandal Ti Muoy",
    "districtId": "1202",
    "code": "120205"
  },
  {
    "id": "120206",
    "name": "Phsar Kandal Ti Pir",
    "districtId": "1202",
    "code": "120206"
  },
  {
    "id": "120207",
    "name": "Chakto Mukh",
    "districtId": "1202",
    "code": "120207"
  },
  {
    "id": "120208",
    "name": "Chey Chummeah",
    "districtId": "1202",
    "code": "120208"
  },
  {
    "id": "120209",
    "name": "Phsar Chas",
    "districtId": "1202",
    "code": "120209"
  },
  {
    "id": "120210",
    "name": "Srah Chak",
    "districtId": "1202",
    "code": "120210"
  },
  {
    "id": "120211",
    "name": "Voat Phnum",
    "districtId": "1202",
    "code": "120211"
  },
  {
    "id": "120301",
    "name": "Ou Ruessei Ti Muoy",
    "districtId": "1203",
    "code": "120301"
  },
  {
    "id": "120302",
    "name": "Ou Ruessei Ti Pir",
    "districtId": "1203",
    "code": "120302"
  },
  {
    "id": "120303",
    "name": "Ou Ruessei Ti Bei",
    "districtId": "1203",
    "code": "120303"
  },
  {
    "id": "120304",
    "name": "Ou Ruessei Ti Buon",
    "districtId": "1203",
    "code": "120304"
  },
  {
    "id": "120305",
    "name": "Monourom",
    "districtId": "1203",
    "code": "120305"
  },
  {
    "id": "120306",
    "name": "Mittapheap",
    "districtId": "1203",
    "code": "120306"
  },
  {
    "id": "120307",
    "name": "Veal Vong",
    "districtId": "1203",
    "code": "120307"
  },
  {
    "id": "120308",
    "name": "Boeng Proluet",
    "districtId": "1203",
    "code": "120308"
  },
  {
    "id": "120401",
    "name": "Phsar Depou Ti Muoy",
    "districtId": "1204",
    "code": "120401"
  },
  {
    "id": "120402",
    "name": "Phsar Depou Ti Pir",
    "districtId": "1204",
    "code": "120402"
  },
  {
    "id": "120403",
    "name": "Phsar Depou Ti Bei",
    "districtId": "1204",
    "code": "120403"
  },
  {
    "id": "120404",
    "name": "Tuek L'ak Ti Muoy",
    "districtId": "1204",
    "code": "120404"
  },
  {
    "id": "120405",
    "name": "Tuek L'ak Ti Pir",
    "districtId": "1204",
    "code": "120405"
  },
  {
    "id": "120406",
    "name": "Tuek L'ak Ti Bei",
    "districtId": "1204",
    "code": "120406"
  },
  {
    "id": "120407",
    "name": "Boeng Kak Ti Muoy",
    "districtId": "1204",
    "code": "120407"
  },
  {
    "id": "120408",
    "name": "Boeng Kak Ti Pir",
    "districtId": "1204",
    "code": "120408"
  },
  {
    "id": "120409",
    "name": "Phsar Daeum Kor",
    "districtId": "1204",
    "code": "120409"
  },
  {
    "id": "120410",
    "name": "Boeng Salang",
    "districtId": "1204",
    "code": "120410"
  },
  {
    "id": "120501",
    "name": "Dangkao",
    "districtId": "1205",
    "code": "120501"
  },
  {
    "id": "120507",
    "name": "Pong Tuek",
    "districtId": "1205",
    "code": "120507"
  },
  {
    "id": "120508",
    "name": "Prey Veaeng",
    "districtId": "1205",
    "code": "120508"
  },
  {
    "id": "120510",
    "name": "Prey Sa",
    "districtId": "1205",
    "code": "120510"
  },
  {
    "id": "120512",
    "name": "Krang Pongro",
    "districtId": "1205",
    "code": "120512"
  },
  {
    "id": "120514",
    "name": "Sak Sampov",
    "districtId": "1205",
    "code": "120514"
  },
  {
    "id": "120515",
    "name": "Cheung Aek",
    "districtId": "1205",
    "code": "120515"
  },
  {
    "id": "120516",
    "name": "Kong Noy",
    "districtId": "1205",
    "code": "120516"
  },
  {
    "id": "120517",
    "name": "Preaek Kampues",
    "districtId": "1205",
    "code": "120517"
  },
  {
    "id": "120518",
    "name": "Roluos",
    "districtId": "1205",
    "code": "120518"
  },
  {
    "id": "120519",
    "name": "Spean Thma",
    "districtId": "1205",
    "code": "120519"
  },
  {
    "id": "120520",
    "name": "Tien",
    "districtId": "1205",
    "code": "120520"
  },
  {
    "id": "120606",
    "name": "Chak Angrae Leu",
    "districtId": "1206",
    "code": "120606"
  },
  {
    "id": "120607",
    "name": "Chak Angrae Kraom",
    "districtId": "1206",
    "code": "120607"
  },
  {
    "id": "120608",
    "name": "Stueng Mean chey 1",
    "districtId": "1206",
    "code": "120608"
  },
  {
    "id": "120609",
    "name": "Stueng Mean chey 2",
    "districtId": "1206",
    "code": "120609"
  },
  {
    "id": "120610",
    "name": "Stueng Mean chey 3",
    "districtId": "1206",
    "code": "120610"
  },
  {
    "id": "120611",
    "name": "Boeng Tumpun 1",
    "districtId": "1206",
    "code": "120611"
  },
  {
    "id": "120612",
    "name": "Boeng Tumpun 2",
    "districtId": "1206",
    "code": "120612"
  },
  {
    "id": "120703",
    "name": "Svay Pak",
    "districtId": "1207",
    "code": "120703"
  },
  {
    "id": "120704",
    "name": "Kilomaetr Lekh Prammuoy",
    "districtId": "1207",
    "code": "120704"
  },
  {
    "id": "120706",
    "name": "Ruessei Kaev",
    "districtId": "1207",
    "code": "120706"
  },
  {
    "id": "120711",
    "name": "Chrang Chamreh Ti Muoy",
    "districtId": "1207",
    "code": "120711"
  },
  {
    "id": "120712",
    "name": "Chrang Chamreh Ti Pir",
    "districtId": "1207",
    "code": "120712"
  },
  {
    "id": "120713",
    "name": "Tuol Sangkae 1",
    "districtId": "1207",
    "code": "120713"
  },
  {
    "id": "120714",
    "name": "Tuol Sangkae 2",
    "districtId": "1207",
    "code": "120714"
  },
  {
    "id": "120801",
    "name": "Phnom Penh Thmei",
    "districtId": "1208",
    "code": "120801"
  },
  {
    "id": "120802",
    "name": "Tuek Thla",
    "districtId": "1208",
    "code": "120802"
  },
  {
    "id": "120803",
    "name": "Khmuonh",
    "districtId": "1208",
    "code": "120803"
  },
  {
    "id": "120807",
    "name": "Krang Thnong",
    "districtId": "1208",
    "code": "120807"
  },
  {
    "id": "120808",
    "name": "Ou Baek K'am",
    "districtId": "1208",
    "code": "120808"
  },
  {
    "id": "120809",
    "name": "Kouk Khleang",
    "districtId": "1208",
    "code": "120809"
  },
  {
    "id": "120901",
    "name": "Trapeang Krasang",
    "districtId": "1209",
    "code": "120901"
  },
  {
    "id": "120906",
    "name": "Samraong Kraom",
    "districtId": "1209",
    "code": "120906"
  },
  {
    "id": "120914",
    "name": "Chaom Chau 1",
    "districtId": "1209",
    "code": "120914"
  },
  {
    "id": "120915",
    "name": "Chaom Chau 2",
    "districtId": "1209",
    "code": "120915"
  },
  {
    "id": "120916",
    "name": "Chaom Chau 3",
    "districtId": "1209",
    "code": "120916"
  },
  {
    "id": "120917",
    "name": "Kakab 1",
    "districtId": "1209",
    "code": "120917"
  },
  {
    "id": "120918",
    "name": "Kakab 2",
    "districtId": "1209",
    "code": "120918"
  },
  {
    "id": "121001",
    "name": "Chrouy Changvar",
    "districtId": "1210",
    "code": "121001"
  },
  {
    "id": "121002",
    "name": "Preaek Lieb",
    "districtId": "1210",
    "code": "121002"
  },
  {
    "id": "121003",
    "name": "Preaek Ta Sek",
    "districtId": "1210",
    "code": "121003"
  },
  {
    "id": "121004",
    "name": "Kaoh Dach",
    "districtId": "1210",
    "code": "121004"
  },
  {
    "id": "121005",
    "name": "Bak Kaeng",
    "districtId": "1210",
    "code": "121005"
  },
  {
    "id": "121101",
    "name": "Preaek Phnov",
    "districtId": "1211",
    "code": "121101"
  },
  {
    "id": "121102",
    "name": "Ponhea Pon",
    "districtId": "1211",
    "code": "121102"
  },
  {
    "id": "121103",
    "name": "Samraong",
    "districtId": "1211",
    "code": "121103"
  },
  {
    "id": "121104",
    "name": "Kouk Roka",
    "districtId": "1211",
    "code": "121104"
  },
  {
    "id": "121105",
    "name": "Ponsang",
    "districtId": "1211",
    "code": "121105"
  },
  {
    "id": "121201",
    "name": "Chhbar Ampov Ti Muoy",
    "districtId": "1212",
    "code": "121201"
  },
  {
    "id": "121202",
    "name": "Chbar Ampov Ti Pir",
    "districtId": "1212",
    "code": "121202"
  },
  {
    "id": "121203",
    "name": "Nirouth",
    "districtId": "1212",
    "code": "121203"
  },
  {
    "id": "121204",
    "name": "Preaek Pra",
    "districtId": "1212",
    "code": "121204"
  },
  {
    "id": "121205",
    "name": "Veal Sbov",
    "districtId": "1212",
    "code": "121205"
  },
  {
    "id": "121206",
    "name": "Preaek Aeng",
    "districtId": "1212",
    "code": "121206"
  },
  {
    "id": "121207",
    "name": "Kbal Kaoh",
    "districtId": "1212",
    "code": "121207"
  },
  {
    "id": "121208",
    "name": "Preaek Thmei",
    "districtId": "1212",
    "code": "121208"
  },
  {
    "id": "121301",
    "name": "Boeng Keng Kang Ti Muoy",
    "districtId": "1213",
    "code": "121301"
  },
  {
    "id": "121302",
    "name": "Boeng Keng Kang Ti Pir",
    "districtId": "1213",
    "code": "121302"
  },
  {
    "id": "121303",
    "name": "Boeng Keng Kang Ti Bei",
    "districtId": "1213",
    "code": "121303"
  },
  {
    "id": "121304",
    "name": "Olympic",
    "districtId": "1213",
    "code": "121304"
  },
  {
    "id": "121305",
    "name": "Tumnob Tuek",
    "districtId": "1213",
    "code": "121305"
  },
  {
    "id": "121306",
    "name": "Tuol Svay Prey Ti Muoy",
    "districtId": "1213",
    "code": "121306"
  },
  {
    "id": "121307",
    "name": "Tuol Svay Prey Ti Pir",
    "districtId": "1213",
    "code": "121307"
  },
  {
    "id": "121401",
    "name": "Kamboul",
    "districtId": "1214",
    "code": "121401"
  },
  {
    "id": "121402",
    "name": "Kantaok",
    "districtId": "1214",
    "code": "121402"
  },
  {
    "id": "121403",
    "name": "Ovlaok",
    "districtId": "1214",
    "code": "121403"
  },
  {
    "id": "121404",
    "name": "Snaor",
    "districtId": "1214",
    "code": "121404"
  },
  {
    "id": "121405",
    "name": "Phleung Chheh Roteh",
    "districtId": "1214",
    "code": "121405"
  },
  {
    "id": "121406",
    "name": "Boeng Thum",
    "districtId": "1214",
    "code": "121406"
  },
  {
    "id": "121407",
    "name": "Prateah Lang",
    "districtId": "1214",
    "code": "121407"
  },
  {
    "id": "130101",
    "name": "S'ang",
    "districtId": "1301",
    "code": "130101"
  },
  {
    "id": "130102",
    "name": "Tasu",
    "districtId": "1301",
    "code": "130102"
  },
  {
    "id": "130103",
    "name": "Khyang",
    "districtId": "1301",
    "code": "130103"
  },
  {
    "id": "130104",
    "name": "Chrach",
    "districtId": "1301",
    "code": "130104"
  },
  {
    "id": "130105",
    "name": "Thmea",
    "districtId": "1301",
    "code": "130105"
  },
  {
    "id": "130106",
    "name": "Putrea",
    "districtId": "1301",
    "code": "130106"
  },
  {
    "id": "130201",
    "name": "Chhaeb Muoy",
    "districtId": "1302",
    "code": "130201"
  },
  {
    "id": "130202",
    "name": "Chhaeb Pir",
    "districtId": "1302",
    "code": "130202"
  },
  {
    "id": "130203",
    "name": "Sangkae Muoy",
    "districtId": "1302",
    "code": "130203"
  },
  {
    "id": "130204",
    "name": "Sangkae Pir",
    "districtId": "1302",
    "code": "130204"
  },
  {
    "id": "130205",
    "name": "Mlu Prey Muoy",
    "districtId": "1302",
    "code": "130205"
  },
  {
    "id": "130206",
    "name": "Mlu Prey Pir",
    "districtId": "1302",
    "code": "130206"
  },
  {
    "id": "130207",
    "name": "Kampong Sralau Muoy",
    "districtId": "1302",
    "code": "130207"
  },
  {
    "id": "130208",
    "name": "Kampong Sralau Pir",
    "districtId": "1302",
    "code": "130208"
  },
  {
    "id": "130301",
    "name": "Choam Ksant",
    "districtId": "1303",
    "code": "130301"
  },
  {
    "id": "130302",
    "name": "Tuek Kraham",
    "districtId": "1303",
    "code": "130302"
  },
  {
    "id": "130303",
    "name": "Pring Thum",
    "districtId": "1303",
    "code": "130303"
  },
  {
    "id": "130304",
    "name": "Rumdaoh Srae",
    "districtId": "1303",
    "code": "130304"
  },
  {
    "id": "130305",
    "name": "Yeang",
    "districtId": "1303",
    "code": "130305"
  },
  {
    "id": "130306",
    "name": "Kantuot",
    "districtId": "1303",
    "code": "130306"
  },
  {
    "id": "130307",
    "name": "Sror Aem",
    "districtId": "1303",
    "code": "130307"
  },
  {
    "id": "130308",
    "name": "Morokot",
    "districtId": "1303",
    "code": "130308"
  },
  {
    "id": "130401",
    "name": "Kuleaen Tboung",
    "districtId": "1304",
    "code": "130401"
  },
  {
    "id": "130402",
    "name": "Kuleaen Cheung",
    "districtId": "1304",
    "code": "130402"
  },
  {
    "id": "130403",
    "name": "Thmei",
    "districtId": "1304",
    "code": "130403"
  },
  {
    "id": "130404",
    "name": "Phnum Penh",
    "districtId": "1304",
    "code": "130404"
  },
  {
    "id": "130405",
    "name": "Phnum Tbaeng Pir",
    "districtId": "1304",
    "code": "130405"
  },
  {
    "id": "130406",
    "name": "Srayang",
    "districtId": "1304",
    "code": "130406"
  },
  {
    "id": "130501",
    "name": "Robieb",
    "districtId": "1305",
    "code": "130501"
  },
  {
    "id": "130502",
    "name": "Reaksmei",
    "districtId": "1305",
    "code": "130502"
  },
  {
    "id": "130503",
    "name": "Rohas",
    "districtId": "1305",
    "code": "130503"
  },
  {
    "id": "130504",
    "name": "Rung Roeang",
    "districtId": "1305",
    "code": "130504"
  },
  {
    "id": "130505",
    "name": "Rik Reay",
    "districtId": "1305",
    "code": "130505"
  },
  {
    "id": "130506",
    "name": "Ruos Roan",
    "districtId": "1305",
    "code": "130506"
  },
  {
    "id": "130507",
    "name": "Rotanak",
    "districtId": "1305",
    "code": "130507"
  },
  {
    "id": "130508",
    "name": "Rieb Roy",
    "districtId": "1305",
    "code": "130508"
  },
  {
    "id": "130509",
    "name": "Reaksa",
    "districtId": "1305",
    "code": "130509"
  },
  {
    "id": "130510",
    "name": "Rumdaoh",
    "districtId": "1305",
    "code": "130510"
  },
  {
    "id": "130511",
    "name": "Romtum",
    "districtId": "1305",
    "code": "130511"
  },
  {
    "id": "130512",
    "name": "Romoneiy",
    "districtId": "1305",
    "code": "130512"
  },
  {
    "id": "130601",
    "name": "Chamraeun",
    "districtId": "1306",
    "code": "130601"
  },
  {
    "id": "130602",
    "name": "Ro'ang",
    "districtId": "1306",
    "code": "130602"
  },
  {
    "id": "130603",
    "name": "Phnum Tbaeng Muoy",
    "districtId": "1306",
    "code": "130603"
  },
  {
    "id": "130604",
    "name": "Sdau",
    "districtId": "1306",
    "code": "130604"
  },
  {
    "id": "130605",
    "name": "Ronak Ser",
    "districtId": "1306",
    "code": "130605"
  },
  {
    "id": "130703",
    "name": "Chhean Mukh",
    "districtId": "1307",
    "code": "130703"
  },
  {
    "id": "130704",
    "name": "Pou",
    "districtId": "1307",
    "code": "130704"
  },
  {
    "id": "130705",
    "name": "Prame",
    "districtId": "1307",
    "code": "130705"
  },
  {
    "id": "130706",
    "name": "Preah Khleang",
    "districtId": "1307",
    "code": "130706"
  },
  {
    "id": "130801",
    "name": "Kampong Pranak",
    "districtId": "1308",
    "code": "130801"
  },
  {
    "id": "130802",
    "name": "Pal Hal",
    "districtId": "1308",
    "code": "130802"
  },
  {
    "id": "140101",
    "name": "Boeng Preah",
    "districtId": "1401",
    "code": "140101"
  },
  {
    "id": "140102",
    "name": "Cheung Phnum",
    "districtId": "1401",
    "code": "140102"
  },
  {
    "id": "140103",
    "name": "Chheu Kach",
    "districtId": "1401",
    "code": "140103"
  },
  {
    "id": "140104",
    "name": "Reaks Chey",
    "districtId": "1401",
    "code": "140104"
  },
  {
    "id": "140105",
    "name": "Roung Damrei",
    "districtId": "1401",
    "code": "140105"
  },
  {
    "id": "140106",
    "name": "Sdau Kaong",
    "districtId": "1401",
    "code": "140106"
  },
  {
    "id": "140107",
    "name": "Spueu Ka",
    "districtId": "1401",
    "code": "140107"
  },
  {
    "id": "140108",
    "name": "Spueu Kha",
    "districtId": "1401",
    "code": "140108"
  },
  {
    "id": "140109",
    "name": "Theay",
    "districtId": "1401",
    "code": "140109"
  },
  {
    "id": "140201",
    "name": "Cheach",
    "districtId": "1402",
    "code": "140201"
  },
  {
    "id": "140202",
    "name": "Doun Koeng",
    "districtId": "1402",
    "code": "140202"
  },
  {
    "id": "140203",
    "name": "Kranhung",
    "districtId": "1402",
    "code": "140203"
  },
  {
    "id": "140204",
    "name": "Krabau",
    "districtId": "1402",
    "code": "140204"
  },
  {
    "id": "140205",
    "name": "Seang Khveang",
    "districtId": "1402",
    "code": "140205"
  },
  {
    "id": "140206",
    "name": "Smaong Khang Cheung",
    "districtId": "1402",
    "code": "140206"
  },
  {
    "id": "140207",
    "name": "Smaong Khang Tboung",
    "districtId": "1402",
    "code": "140207"
  },
  {
    "id": "140208",
    "name": "Trabaek",
    "districtId": "1402",
    "code": "140208"
  },
  {
    "id": "140301",
    "name": "Ansaong",
    "districtId": "1403",
    "code": "140301"
  },
  {
    "id": "140302",
    "name": "Cham",
    "districtId": "1403",
    "code": "140302"
  },
  {
    "id": "140303",
    "name": "Cheang Daek",
    "districtId": "1403",
    "code": "140303"
  },
  {
    "id": "140304",
    "name": "Chrey",
    "districtId": "1403",
    "code": "140304"
  },
  {
    "id": "140305",
    "name": "Kansoam Ak",
    "districtId": "1403",
    "code": "140305"
  },
  {
    "id": "140306",
    "name": "Kou Khchak",
    "districtId": "1403",
    "code": "140306"
  },
  {
    "id": "140307",
    "name": "Kampong Trabaek",
    "districtId": "1403",
    "code": "140307"
  },
  {
    "id": "140308",
    "name": "Peam Montear",
    "districtId": "1403",
    "code": "140308"
  },
  {
    "id": "140309",
    "name": "Prasat",
    "districtId": "1403",
    "code": "140309"
  },
  {
    "id": "140310",
    "name": "Pratheat",
    "districtId": "1403",
    "code": "140310"
  },
  {
    "id": "140311",
    "name": "Prey Chhor",
    "districtId": "1403",
    "code": "140311"
  },
  {
    "id": "140312",
    "name": "Prey Poun",
    "districtId": "1403",
    "code": "140312"
  },
  {
    "id": "140313",
    "name": "Thkov",
    "districtId": "1403",
    "code": "140313"
  },
  {
    "id": "140401",
    "name": "Chong Ampil",
    "districtId": "1404",
    "code": "140401"
  },
  {
    "id": "140402",
    "name": "Kanhchriech",
    "districtId": "1404",
    "code": "140402"
  },
  {
    "id": "140403",
    "name": "Kdoeang Reay",
    "districtId": "1404",
    "code": "140403"
  },
  {
    "id": "140404",
    "name": "Kouk Kong Kaeut",
    "districtId": "1404",
    "code": "140404"
  },
  {
    "id": "140405",
    "name": "Kouk Kong Lech",
    "districtId": "1404",
    "code": "140405"
  },
  {
    "id": "140406",
    "name": "Preal",
    "districtId": "1404",
    "code": "140406"
  },
  {
    "id": "140407",
    "name": "Thma Pun",
    "districtId": "1404",
    "code": "140407"
  },
  {
    "id": "140408",
    "name": "Tnaot",
    "districtId": "1404",
    "code": "140408"
  },
  {
    "id": "140501",
    "name": "Angkor Sar",
    "districtId": "1405",
    "code": "140501"
  },
  {
    "id": "140502",
    "name": "Chres",
    "districtId": "1405",
    "code": "140502"
  },
  {
    "id": "140503",
    "name": "Chi Phoch",
    "districtId": "1405",
    "code": "140503"
  },
  {
    "id": "140504",
    "name": "Prey Khnes",
    "districtId": "1405",
    "code": "140504"
  },
  {
    "id": "140505",
    "name": "Prey Rumdeng",
    "districtId": "1405",
    "code": "140505"
  },
  {
    "id": "140506",
    "name": "Prey Totueng",
    "districtId": "1405",
    "code": "140506"
  },
  {
    "id": "140507",
    "name": "Svay Chrum",
    "districtId": "1405",
    "code": "140507"
  },
  {
    "id": "140508",
    "name": "Trapeang Srae",
    "districtId": "1405",
    "code": "140508"
  },
  {
    "id": "140601",
    "name": "Angkor Angk",
    "districtId": "1406",
    "code": "140601"
  },
  {
    "id": "140602",
    "name": "Kampong Prasat",
    "districtId": "1406",
    "code": "140602"
  },
  {
    "id": "140603",
    "name": "Kaoh Chek",
    "districtId": "1406",
    "code": "140603"
  },
  {
    "id": "140604",
    "name": "Kaoh Roka",
    "districtId": "1406",
    "code": "140604"
  },
  {
    "id": "140605",
    "name": "Kaoh Sampov",
    "districtId": "1406",
    "code": "140605"
  },
  {
    "id": "140606",
    "name": "Krang Ta Yang",
    "districtId": "1406",
    "code": "140606"
  },
  {
    "id": "140607",
    "name": "Preaek Krabau",
    "districtId": "1406",
    "code": "140607"
  },
  {
    "id": "140608",
    "name": "Preaek Sambuor",
    "districtId": "1406",
    "code": "140608"
  },
  {
    "id": "140609",
    "name": "Ruessei Srok",
    "districtId": "1406",
    "code": "140609"
  },
  {
    "id": "140610",
    "name": "Svay Phluoh",
    "districtId": "1406",
    "code": "140610"
  },
  {
    "id": "140701",
    "name": "Ba Baong",
    "districtId": "1407",
    "code": "140701"
  },
  {
    "id": "140702",
    "name": "Banlich Prasat",
    "districtId": "1407",
    "code": "140702"
  },
  {
    "id": "140703",
    "name": "Neak Loeang",
    "districtId": "1407",
    "code": "140703"
  },
  {
    "id": "140704",
    "name": "Peam Mean Chey",
    "districtId": "1407",
    "code": "140704"
  },
  {
    "id": "140705",
    "name": "Peam Ro",
    "districtId": "1407",
    "code": "140705"
  },
  {
    "id": "140706",
    "name": "Preaek Khsay Ka",
    "districtId": "1407",
    "code": "140706"
  },
  {
    "id": "140707",
    "name": "Preaek Khsay Kha",
    "districtId": "1407",
    "code": "140707"
  },
  {
    "id": "140708",
    "name": "Prey Kandieng",
    "districtId": "1407",
    "code": "140708"
  },
  {
    "id": "140801",
    "name": "Kampong Popil",
    "districtId": "1408",
    "code": "140801"
  },
  {
    "id": "140802",
    "name": "Kanhcham",
    "districtId": "1408",
    "code": "140802"
  },
  {
    "id": "140803",
    "name": "Kampong Prang",
    "districtId": "1408",
    "code": "140803"
  },
  {
    "id": "140805",
    "name": "Mesar Prachan",
    "districtId": "1408",
    "code": "140805"
  },
  {
    "id": "140807",
    "name": "Prey Pnov",
    "districtId": "1408",
    "code": "140807"
  },
  {
    "id": "140808",
    "name": "Prey Sniet",
    "districtId": "1408",
    "code": "140808"
  },
  {
    "id": "140809",
    "name": "Prey Sralet",
    "districtId": "1408",
    "code": "140809"
  },
  {
    "id": "140810",
    "name": "Reab",
    "districtId": "1408",
    "code": "140810"
  },
  {
    "id": "140811",
    "name": "Roka",
    "districtId": "1408",
    "code": "140811"
  },
  {
    "id": "140901",
    "name": "Angkor Reach",
    "districtId": "1409",
    "code": "140901"
  },
  {
    "id": "140902",
    "name": "Banteay Chakrei",
    "districtId": "1409",
    "code": "140902"
  },
  {
    "id": "140903",
    "name": "Boeng Daol",
    "districtId": "1409",
    "code": "140903"
  },
  {
    "id": "140904",
    "name": "Chey Kampok",
    "districtId": "1409",
    "code": "140904"
  },
  {
    "id": "140905",
    "name": "Kampong Soeng",
    "districtId": "1409",
    "code": "140905"
  },
  {
    "id": "140906",
    "name": "Krang Svay",
    "districtId": "1409",
    "code": "140906"
  },
  {
    "id": "140907",
    "name": "Lvea",
    "districtId": "1409",
    "code": "140907"
  },
  {
    "id": "140908",
    "name": "Preah Sdach",
    "districtId": "1409",
    "code": "140908"
  },
  {
    "id": "140909",
    "name": "Reathor",
    "districtId": "1409",
    "code": "140909"
  },
  {
    "id": "140910",
    "name": "Rumchek",
    "districtId": "1409",
    "code": "140910"
  },
  {
    "id": "140911",
    "name": "Sena Reach Otdam",
    "districtId": "1409",
    "code": "140911"
  },
  {
    "id": "141001",
    "name": "Baray",
    "districtId": "1410",
    "code": "141001"
  },
  {
    "id": "141002",
    "name": "Cheung Tuek",
    "districtId": "1410",
    "code": "141002"
  },
  {
    "id": "141003",
    "name": "Kampong Leav",
    "districtId": "1410",
    "code": "141003"
  },
  {
    "id": "141004",
    "name": "Ta Kao",
    "districtId": "1410",
    "code": "141004"
  },
  {
    "id": "141101",
    "name": "Pou Rieng",
    "districtId": "1411",
    "code": "141101"
  },
  {
    "id": "141102",
    "name": "Preaek Anteah",
    "districtId": "1411",
    "code": "141102"
  },
  {
    "id": "141103",
    "name": "Preaek Chrey",
    "districtId": "1411",
    "code": "141103"
  },
  {
    "id": "141104",
    "name": "Prey Kanlaong",
    "districtId": "1411",
    "code": "141104"
  },
  {
    "id": "141106",
    "name": "Kampong Ruessei",
    "districtId": "1411",
    "code": "141106"
  },
  {
    "id": "141107",
    "name": "Preaek Ta Sar",
    "districtId": "1411",
    "code": "141107"
  },
  {
    "id": "141201",
    "name": "Ampil Krau",
    "districtId": "1412",
    "code": "141201"
  },
  {
    "id": "141202",
    "name": "Chrey Khmum",
    "districtId": "1412",
    "code": "141202"
  },
  {
    "id": "141203",
    "name": "Lve",
    "districtId": "1412",
    "code": "141203"
  },
  {
    "id": "141204",
    "name": "Pnov Ti Muoy",
    "districtId": "1412",
    "code": "141204"
  },
  {
    "id": "141205",
    "name": "Pnov Ti Pir",
    "districtId": "1412",
    "code": "141205"
  },
  {
    "id": "141206",
    "name": "Pou Ti",
    "districtId": "1412",
    "code": "141206"
  },
  {
    "id": "141207",
    "name": "Preaek Changkran",
    "districtId": "1412",
    "code": "141207"
  },
  {
    "id": "141208",
    "name": "Prey Daeum Thnoeng",
    "districtId": "1412",
    "code": "141208"
  },
  {
    "id": "141209",
    "name": "Prey Tueng",
    "districtId": "1412",
    "code": "141209"
  },
  {
    "id": "141210",
    "name": "Rumlech",
    "districtId": "1412",
    "code": "141210"
  },
  {
    "id": "141211",
    "name": "Ruessei Sanh",
    "districtId": "1412",
    "code": "141211"
  },
  {
    "id": "141301",
    "name": "Angkor Tret",
    "districtId": "1413",
    "code": "141301"
  },
  {
    "id": "141302",
    "name": "Chea Khlang",
    "districtId": "1413",
    "code": "141302"
  },
  {
    "id": "141303",
    "name": "Chrey",
    "districtId": "1413",
    "code": "141303"
  },
  {
    "id": "141304",
    "name": "Damrei Puon",
    "districtId": "1413",
    "code": "141304"
  },
  {
    "id": "141305",
    "name": "Me Bon",
    "districtId": "1413",
    "code": "141305"
  },
  {
    "id": "141306",
    "name": "Pean Roung",
    "districtId": "1413",
    "code": "141306"
  },
  {
    "id": "141307",
    "name": "Popueus",
    "districtId": "1413",
    "code": "141307"
  },
  {
    "id": "141308",
    "name": "Prey Khla",
    "districtId": "1413",
    "code": "141308"
  },
  {
    "id": "141309",
    "name": "Samraong",
    "districtId": "1413",
    "code": "141309"
  },
  {
    "id": "141310",
    "name": "Svay Antor",
    "districtId": "1413",
    "code": "141310"
  },
  {
    "id": "141311",
    "name": "Tuek Thla",
    "districtId": "1413",
    "code": "141311"
  },
  {
    "id": "150101",
    "name": "Boeng Bat Kandaol",
    "districtId": "1501",
    "code": "150101"
  },
  {
    "id": "150102",
    "name": "Boeng Khnar",
    "districtId": "1501",
    "code": "150102"
  },
  {
    "id": "150103",
    "name": "Khnar Totueng",
    "districtId": "1501",
    "code": "150103"
  },
  {
    "id": "150104",
    "name": "Me Tuek",
    "districtId": "1501",
    "code": "150104"
  },
  {
    "id": "150105",
    "name": "Ou Ta Paong",
    "districtId": "1501",
    "code": "150105"
  },
  {
    "id": "150106",
    "name": "Rumlech",
    "districtId": "1501",
    "code": "150106"
  },
  {
    "id": "150107",
    "name": "Snam Preah",
    "districtId": "1501",
    "code": "150107"
  },
  {
    "id": "150108",
    "name": "Svay Doun Kaev",
    "districtId": "1501",
    "code": "150108"
  },
  {
    "id": "150110",
    "name": "Trapeang chorng",
    "districtId": "1501",
    "code": "150110"
  },
  {
    "id": "150201",
    "name": "Anlong Vil",
    "districtId": "1502",
    "code": "150201"
  },
  {
    "id": "150203",
    "name": "Kandieng",
    "districtId": "1502",
    "code": "150203"
  },
  {
    "id": "150204",
    "name": "Kanhchor",
    "districtId": "1502",
    "code": "150204"
  },
  {
    "id": "150205",
    "name": "Reang Til",
    "districtId": "1502",
    "code": "150205"
  },
  {
    "id": "150206",
    "name": "Srae Sdok",
    "districtId": "1502",
    "code": "150206"
  },
  {
    "id": "150207",
    "name": "Svay Luong",
    "districtId": "1502",
    "code": "150207"
  },
  {
    "id": "150208",
    "name": "Sya",
    "districtId": "1502",
    "code": "150208"
  },
  {
    "id": "150209",
    "name": "Veal",
    "districtId": "1502",
    "code": "150209"
  },
  {
    "id": "150210",
    "name": "Kaoh Chum",
    "districtId": "1502",
    "code": "150210"
  },
  {
    "id": "150301",
    "name": "Anlong Tnaot",
    "districtId": "1503",
    "code": "150301"
  },
  {
    "id": "150302",
    "name": "Ansa Chambak",
    "districtId": "1503",
    "code": "150302"
  },
  {
    "id": "150303",
    "name": "Boeng Kantuot",
    "districtId": "1503",
    "code": "150303"
  },
  {
    "id": "150304",
    "name": "Chheu Tom",
    "districtId": "1503",
    "code": "150304"
  },
  {
    "id": "150305",
    "name": "Kampong Luong",
    "districtId": "1503",
    "code": "150305"
  },
  {
    "id": "150306",
    "name": "Kampong Pou",
    "districtId": "1503",
    "code": "150306"
  },
  {
    "id": "150307",
    "name": "Kbal Trach",
    "districtId": "1503",
    "code": "150307"
  },
  {
    "id": "150308",
    "name": "Ou Sandan",
    "districtId": "1503",
    "code": "150308"
  },
  {
    "id": "150309",
    "name": "Sna Ansa",
    "districtId": "1503",
    "code": "150309"
  },
  {
    "id": "150310",
    "name": "Svay Sa",
    "districtId": "1503",
    "code": "150310"
  },
  {
    "id": "150311",
    "name": "Tnaot Chum",
    "districtId": "1503",
    "code": "150311"
  },
  {
    "id": "150401",
    "name": "Bak Chenhchien",
    "districtId": "1504",
    "code": "150401"
  },
  {
    "id": "150402",
    "name": "Leach",
    "districtId": "1504",
    "code": "150402"
  },
  {
    "id": "150404",
    "name": "Prongil",
    "districtId": "1504",
    "code": "150404"
  },
  {
    "id": "150405",
    "name": "Rokat",
    "districtId": "1504",
    "code": "150405"
  },
  {
    "id": "150406",
    "name": "Santreae",
    "districtId": "1504",
    "code": "150406"
  },
  {
    "id": "150407",
    "name": "Samraong",
    "districtId": "1504",
    "code": "150407"
  },
  {
    "id": "150501",
    "name": "Chamraeun Phal",
    "districtId": "1505",
    "code": "150501"
  },
  {
    "id": "150503",
    "name": "Lolok Sa",
    "districtId": "1505",
    "code": "150503"
  },
  {
    "id": "150504",
    "name": "Phteah Prey",
    "districtId": "1505",
    "code": "150504"
  },
  {
    "id": "150505",
    "name": "Prey Nhi",
    "districtId": "1505",
    "code": "150505"
  },
  {
    "id": "150506",
    "name": "Roleab",
    "districtId": "1505",
    "code": "150506"
  },
  {
    "id": "150507",
    "name": "Svay At",
    "districtId": "1505",
    "code": "150507"
  },
  {
    "id": "150508",
    "name": "Banteay Dei",
    "districtId": "1505",
    "code": "150508"
  },
  {
    "id": "150601",
    "name": "Ou Saom",
    "districtId": "1506",
    "code": "150601"
  },
  {
    "id": "150602",
    "name": "Krapeu Pir",
    "districtId": "1506",
    "code": "150602"
  },
  {
    "id": "150603",
    "name": "Anlong Reab",
    "districtId": "1506",
    "code": "150603"
  },
  {
    "id": "150604",
    "name": "Pramaoy",
    "districtId": "1506",
    "code": "150604"
  },
  {
    "id": "150605",
    "name": "Thma Da",
    "districtId": "1506",
    "code": "150605"
  },
  {
    "id": "150701",
    "name": "Ta Lou",
    "districtId": "1507",
    "code": "150701"
  },
  {
    "id": "150702",
    "name": "Phteah Rung",
    "districtId": "1507",
    "code": "150702"
  },
  {
    "id": "160101",
    "name": "Malik",
    "districtId": "1601",
    "code": "160101"
  },
  {
    "id": "160103",
    "name": "Nhang",
    "districtId": "1601",
    "code": "160103"
  },
  {
    "id": "160104",
    "name": "Ta Lav",
    "districtId": "1601",
    "code": "160104"
  },
  {
    "id": "160201",
    "name": "Kachanh",
    "districtId": "1602",
    "code": "160201"
  },
  {
    "id": "160202",
    "name": "Labansiek",
    "districtId": "1602",
    "code": "160202"
  },
  {
    "id": "160203",
    "name": "Yeak Laom",
    "districtId": "1602",
    "code": "160203"
  },
  {
    "id": "160204",
    "name": "Boeng Kansaeng",
    "districtId": "1602",
    "code": "160204"
  },
  {
    "id": "160301",
    "name": "Kak",
    "districtId": "1603",
    "code": "160301"
  },
  {
    "id": "160302",
    "name": "Keh Chong",
    "districtId": "1603",
    "code": "160302"
  },
  {
    "id": "160303",
    "name": "La Minh",
    "districtId": "1603",
    "code": "160303"
  },
  {
    "id": "160304",
    "name": "Lung Khung",
    "districtId": "1603",
    "code": "160304"
  },
  {
    "id": "160305",
    "name": "Saeung",
    "districtId": "1603",
    "code": "160305"
  },
  {
    "id": "160306",
    "name": "Ting Chak",
    "districtId": "1603",
    "code": "160306"
  },
  {
    "id": "160401",
    "name": "Serei Mongkol",
    "districtId": "1604",
    "code": "160401"
  },
  {
    "id": "160402",
    "name": "Srae Angkrorng",
    "districtId": "1604",
    "code": "160402"
  },
  {
    "id": "160403",
    "name": "Ta Ang",
    "districtId": "1604",
    "code": "160403"
  },
  {
    "id": "160404",
    "name": "Teun",
    "districtId": "1604",
    "code": "160404"
  },
  {
    "id": "160405",
    "name": "Trapeang Chres",
    "districtId": "1604",
    "code": "160405"
  },
  {
    "id": "160406",
    "name": "Trapeang Kraham",
    "districtId": "1604",
    "code": "160406"
  },
  {
    "id": "160501",
    "name": "Chey Otdam",
    "districtId": "1605",
    "code": "160501"
  },
  {
    "id": "160502",
    "name": "Ka Laeng",
    "districtId": "1605",
    "code": "160502"
  },
  {
    "id": "160503",
    "name": "Lbang Muoy",
    "districtId": "1605",
    "code": "160503"
  },
  {
    "id": "160504",
    "name": "Lbang Pir",
    "districtId": "1605",
    "code": "160504"
  },
  {
    "id": "160505",
    "name": "Ba Tang",
    "districtId": "1605",
    "code": "160505"
  },
  {
    "id": "160506",
    "name": "Seda",
    "districtId": "1605",
    "code": "160506"
  },
  {
    "id": "160601",
    "name": "Cha Ung",
    "districtId": "1606",
    "code": "160601"
  },
  {
    "id": "160602",
    "name": "Pouy",
    "districtId": "1606",
    "code": "160602"
  },
  {
    "id": "160603",
    "name": "Aekakpheap",
    "districtId": "1606",
    "code": "160603"
  },
  {
    "id": "160604",
    "name": "Kalai",
    "districtId": "1606",
    "code": "160604"
  },
  {
    "id": "160605",
    "name": "Ou Chum",
    "districtId": "1606",
    "code": "160605"
  },
  {
    "id": "160606",
    "name": "Sameakki",
    "districtId": "1606",
    "code": "160606"
  },
  {
    "id": "160607",
    "name": "L'ak",
    "districtId": "1606",
    "code": "160607"
  },
  {
    "id": "160701",
    "name": "Bar Kham",
    "districtId": "1607",
    "code": "160701"
  },
  {
    "id": "160702",
    "name": "Lum Choar",
    "districtId": "1607",
    "code": "160702"
  },
  {
    "id": "160703",
    "name": "Pak Nhai",
    "districtId": "1607",
    "code": "160703"
  },
  {
    "id": "160704",
    "name": "Pa Te",
    "districtId": "1607",
    "code": "160704"
  },
  {
    "id": "160705",
    "name": "Sesan",
    "districtId": "1607",
    "code": "160705"
  },
  {
    "id": "160706",
    "name": "Saom Thum",
    "districtId": "1607",
    "code": "160706"
  },
  {
    "id": "160707",
    "name": "Ya Tung",
    "districtId": "1607",
    "code": "160707"
  },
  {
    "id": "160801",
    "name": "Ta Veaeng Leu",
    "districtId": "1608",
    "code": "160801"
  },
  {
    "id": "160802",
    "name": "Ta Veaeng Kraom",
    "districtId": "1608",
    "code": "160802"
  },
  {
    "id": "160901",
    "name": "Pong",
    "districtId": "1609",
    "code": "160901"
  },
  {
    "id": "160903",
    "name": "Hat Pak",
    "districtId": "1609",
    "code": "160903"
  },
  {
    "id": "160904",
    "name": "Ka Choun",
    "districtId": "1609",
    "code": "160904"
  },
  {
    "id": "160905",
    "name": "Kaoh Pang",
    "districtId": "1609",
    "code": "160905"
  },
  {
    "id": "160906",
    "name": "Kaoh Peak",
    "districtId": "1609",
    "code": "160906"
  },
  {
    "id": "160907",
    "name": "Kok Lak",
    "districtId": "1609",
    "code": "160907"
  },
  {
    "id": "160908",
    "name": "Pa Kalan",
    "districtId": "1609",
    "code": "160908"
  },
  {
    "id": "160909",
    "name": "Phnum Kok",
    "districtId": "1609",
    "code": "160909"
  },
  {
    "id": "160910",
    "name": "Veun Sai",
    "districtId": "1609",
    "code": "160910"
  },
  {
    "id": "170101",
    "name": "Char Chhuk",
    "districtId": "1701",
    "code": "170101"
  },
  {
    "id": "170102",
    "name": "Doun Peng",
    "districtId": "1701",
    "code": "170102"
  },
  {
    "id": "170103",
    "name": "Kouk Doung",
    "districtId": "1701",
    "code": "170103"
  },
  {
    "id": "170104",
    "name": "Koul",
    "districtId": "1701",
    "code": "170104"
  },
  {
    "id": "170105",
    "name": "Nokor Pheas",
    "districtId": "1701",
    "code": "170105"
  },
  {
    "id": "170106",
    "name": "Srae Khvav",
    "districtId": "1701",
    "code": "170106"
  },
  {
    "id": "170107",
    "name": "Ta Saom",
    "districtId": "1701",
    "code": "170107"
  },
  {
    "id": "170201",
    "name": "Chob Ta Trav",
    "districtId": "1702",
    "code": "170201"
  },
  {
    "id": "170202",
    "name": "Leang Dai",
    "districtId": "1702",
    "code": "170202"
  },
  {
    "id": "170203",
    "name": "Peak Snaeng",
    "districtId": "1702",
    "code": "170203"
  },
  {
    "id": "170204",
    "name": "Svay Chek",
    "districtId": "1702",
    "code": "170204"
  },
  {
    "id": "170301",
    "name": "Khnar Sanday",
    "districtId": "1703",
    "code": "170301"
  },
  {
    "id": "170302",
    "name": "Khun Ream",
    "districtId": "1703",
    "code": "170302"
  },
  {
    "id": "170303",
    "name": "Preah Dak",
    "districtId": "1703",
    "code": "170303"
  },
  {
    "id": "170304",
    "name": "Rumchek",
    "districtId": "1703",
    "code": "170304"
  },
  {
    "id": "170305",
    "name": "Run Ta Aek",
    "districtId": "1703",
    "code": "170305"
  },
  {
    "id": "170306",
    "name": "Tbaeng",
    "districtId": "1703",
    "code": "170306"
  },
  {
    "id": "170401",
    "name": "Anlong Samnar",
    "districtId": "1704",
    "code": "170401"
  },
  {
    "id": "170402",
    "name": "Chi Kraeng",
    "districtId": "1704",
    "code": "170402"
  },
  {
    "id": "170403",
    "name": "Kampong Kdei",
    "districtId": "1704",
    "code": "170403"
  },
  {
    "id": "170404",
    "name": "Khvav",
    "districtId": "1704",
    "code": "170404"
  },
  {
    "id": "170405",
    "name": "Kouk Thlok Kraom",
    "districtId": "1704",
    "code": "170405"
  },
  {
    "id": "170406",
    "name": "Kouk Thlok Leu",
    "districtId": "1704",
    "code": "170406"
  },
  {
    "id": "170407",
    "name": "Lveaeng Ruessei",
    "districtId": "1704",
    "code": "170407"
  },
  {
    "id": "170408",
    "name": "Pongro Kraom",
    "districtId": "1704",
    "code": "170408"
  },
  {
    "id": "170409",
    "name": "Pongro Leu",
    "districtId": "1704",
    "code": "170409"
  },
  {
    "id": "170410",
    "name": "Ruessei Lok",
    "districtId": "1704",
    "code": "170410"
  },
  {
    "id": "170411",
    "name": "Sangvaeuy",
    "districtId": "1704",
    "code": "170411"
  },
  {
    "id": "170412",
    "name": "Spean Tnaot",
    "districtId": "1704",
    "code": "170412"
  },
  {
    "id": "170601",
    "name": "Chanleas Dai",
    "districtId": "1706",
    "code": "170601"
  },
  {
    "id": "170602",
    "name": "Kampong Thkov",
    "districtId": "1706",
    "code": "170602"
  },
  {
    "id": "170603",
    "name": "Kralanh",
    "districtId": "1706",
    "code": "170603"
  },
  {
    "id": "170604",
    "name": "Krouch Kor",
    "districtId": "1706",
    "code": "170604"
  },
  {
    "id": "170605",
    "name": "Roung Kou",
    "districtId": "1706",
    "code": "170605"
  },
  {
    "id": "170606",
    "name": "Sambuor",
    "districtId": "1706",
    "code": "170606"
  },
  {
    "id": "170607",
    "name": "Saen Sokh",
    "districtId": "1706",
    "code": "170607"
  },
  {
    "id": "170608",
    "name": "Snuol",
    "districtId": "1706",
    "code": "170608"
  },
  {
    "id": "170609",
    "name": "Sranal",
    "districtId": "1706",
    "code": "170609"
  },
  {
    "id": "170610",
    "name": "Ta An",
    "districtId": "1706",
    "code": "170610"
  },
  {
    "id": "170701",
    "name": "Sasar Sdam",
    "districtId": "1707",
    "code": "170701"
  },
  {
    "id": "170702",
    "name": "Doun Kaev",
    "districtId": "1707",
    "code": "170702"
  },
  {
    "id": "170703",
    "name": "Kdei Run",
    "districtId": "1707",
    "code": "170703"
  },
  {
    "id": "170704",
    "name": "Kaev Poar",
    "districtId": "1707",
    "code": "170704"
  },
  {
    "id": "170705",
    "name": "Khnat",
    "districtId": "1707",
    "code": "170705"
  },
  {
    "id": "170707",
    "name": "Lvea",
    "districtId": "1707",
    "code": "170707"
  },
  {
    "id": "170708",
    "name": "Mukh Paen",
    "districtId": "1707",
    "code": "170708"
  },
  {
    "id": "170709",
    "name": "Pou Treay",
    "districtId": "1707",
    "code": "170709"
  },
  {
    "id": "170710",
    "name": "Puok",
    "districtId": "1707",
    "code": "170710"
  },
  {
    "id": "170711",
    "name": "Prey Chruk",
    "districtId": "1707",
    "code": "170711"
  },
  {
    "id": "170712",
    "name": "Reul",
    "districtId": "1707",
    "code": "170712"
  },
  {
    "id": "170713",
    "name": "Samraong Yea",
    "districtId": "1707",
    "code": "170713"
  },
  {
    "id": "170715",
    "name": "Trei Nhoar",
    "districtId": "1707",
    "code": "170715"
  },
  {
    "id": "170716",
    "name": "Yeang",
    "districtId": "1707",
    "code": "170716"
  },
  {
    "id": "170902",
    "name": "Bakong",
    "districtId": "1709",
    "code": "170902"
  },
  {
    "id": "170903",
    "name": "Ballangk",
    "districtId": "1709",
    "code": "170903"
  },
  {
    "id": "170904",
    "name": "Kampong Phluk",
    "districtId": "1709",
    "code": "170904"
  },
  {
    "id": "170905",
    "name": "Kantreang",
    "districtId": "1709",
    "code": "170905"
  },
  {
    "id": "170906",
    "name": "Kandaek",
    "districtId": "1709",
    "code": "170906"
  },
  {
    "id": "170907",
    "name": "Mean Chey",
    "districtId": "1709",
    "code": "170907"
  },
  {
    "id": "170908",
    "name": "Roluos",
    "districtId": "1709",
    "code": "170908"
  },
  {
    "id": "170909",
    "name": "Trapeang Thum",
    "districtId": "1709",
    "code": "170909"
  },
  {
    "id": "170910",
    "name": "Ampil",
    "districtId": "1709",
    "code": "170910"
  },
  {
    "id": "171001",
    "name": "Sla Kram",
    "districtId": "1710",
    "code": "171001"
  },
  {
    "id": "171002",
    "name": "Svay Dankum",
    "districtId": "1710",
    "code": "171002"
  },
  {
    "id": "171003",
    "name": "Kok Chak",
    "districtId": "1710",
    "code": "171003"
  },
  {
    "id": "171004",
    "name": "Sala Kamreuk",
    "districtId": "1710",
    "code": "171004"
  },
  {
    "id": "171005",
    "name": "Nokor Thum",
    "districtId": "1710",
    "code": "171005"
  },
  {
    "id": "171006",
    "name": "Chreav",
    "districtId": "1710",
    "code": "171006"
  },
  {
    "id": "171007",
    "name": "Chong Khnies",
    "districtId": "1710",
    "code": "171007"
  },
  {
    "id": "171008",
    "name": "Sngkat Sambuor",
    "districtId": "1710",
    "code": "171008"
  },
  {
    "id": "171009",
    "name": "Siem Reab",
    "districtId": "1710",
    "code": "171009"
  },
  {
    "id": "171010",
    "name": "Srangae",
    "districtId": "1710",
    "code": "171010"
  },
  {
    "id": "171012",
    "name": "Krabei Riel",
    "districtId": "1710",
    "code": "171012"
  },
  {
    "id": "171013",
    "name": "Tuek Vil",
    "districtId": "1710",
    "code": "171013"
  },
  {
    "id": "171101",
    "name": "Chan Sa",
    "districtId": "1711",
    "code": "171101"
  },
  {
    "id": "171102",
    "name": "Dam Daek",
    "districtId": "1711",
    "code": "171102"
  },
  {
    "id": "171103",
    "name": "Dan Run",
    "districtId": "1711",
    "code": "171103"
  },
  {
    "id": "171104",
    "name": "Kampong Khleang",
    "districtId": "1711",
    "code": "171104"
  },
  {
    "id": "171105",
    "name": "Kien Sangkae",
    "districtId": "1711",
    "code": "171105"
  },
  {
    "id": "171106",
    "name": "Khchas",
    "districtId": "1711",
    "code": "171106"
  },
  {
    "id": "171107",
    "name": "Khnar Pou",
    "districtId": "1711",
    "code": "171107"
  },
  {
    "id": "171108",
    "name": "Popel",
    "districtId": "1711",
    "code": "171108"
  },
  {
    "id": "171109",
    "name": "Samraong",
    "districtId": "1711",
    "code": "171109"
  },
  {
    "id": "171110",
    "name": "Ta Yaek",
    "districtId": "1711",
    "code": "171110"
  },
  {
    "id": "171201",
    "name": "Chrouy Neang Nguon",
    "districtId": "1712",
    "code": "171201"
  },
  {
    "id": "171202",
    "name": "Klang Hay",
    "districtId": "1712",
    "code": "171202"
  },
  {
    "id": "171203",
    "name": "Tram Sasar",
    "districtId": "1712",
    "code": "171203"
  },
  {
    "id": "171204",
    "name": "Moung",
    "districtId": "1712",
    "code": "171204"
  },
  {
    "id": "171205",
    "name": "Prei",
    "districtId": "1712",
    "code": "171205"
  },
  {
    "id": "171206",
    "name": "Slaeng Spean",
    "districtId": "1712",
    "code": "171206"
  },
  {
    "id": "171301",
    "name": "Boeng Mealea",
    "districtId": "1713",
    "code": "171301"
  },
  {
    "id": "171302",
    "name": "Kantuot",
    "districtId": "1713",
    "code": "171302"
  },
  {
    "id": "171303",
    "name": "Khnang Phnum",
    "districtId": "1713",
    "code": "171303"
  },
  {
    "id": "171304",
    "name": "Svay Leu",
    "districtId": "1713",
    "code": "171304"
  },
  {
    "id": "171305",
    "name": "Ta Siem",
    "districtId": "1713",
    "code": "171305"
  },
  {
    "id": "171401",
    "name": "Prasat",
    "districtId": "1714",
    "code": "171401"
  },
  {
    "id": "171402",
    "name": "Lvea Krang",
    "districtId": "1714",
    "code": "171402"
  },
  {
    "id": "171403",
    "name": "Srae Nouy",
    "districtId": "1714",
    "code": "171403"
  },
  {
    "id": "171404",
    "name": "Svay Sa",
    "districtId": "1714",
    "code": "171404"
  },
  {
    "id": "171405",
    "name": "Varin",
    "districtId": "1714",
    "code": "171405"
  },
  {
    "id": "180101",
    "name": "lek Muoy",
    "districtId": "1801",
    "code": "180101"
  },
  {
    "id": "180102",
    "name": "Pir",
    "districtId": "1801",
    "code": "180102"
  },
  {
    "id": "180103",
    "name": "Bei",
    "districtId": "1801",
    "code": "180103"
  },
  {
    "id": "180104",
    "name": "Buon",
    "districtId": "1801",
    "code": "180104"
  },
  {
    "id": "180201",
    "name": "Andoung Thma",
    "districtId": "1802",
    "code": "180201"
  },
  {
    "id": "180202",
    "name": "Boeng Ta Prum",
    "districtId": "1802",
    "code": "180202"
  },
  {
    "id": "180203",
    "name": "Bet Trang",
    "districtId": "1802",
    "code": "180203"
  },
  {
    "id": "180204",
    "name": "Cheung Kou",
    "districtId": "1802",
    "code": "180204"
  },
  {
    "id": "180205",
    "name": "Ou Chrov",
    "districtId": "1802",
    "code": "180205"
  },
  {
    "id": "180206",
    "name": "Ou Oknha Heng",
    "districtId": "1802",
    "code": "180206"
  },
  {
    "id": "180207",
    "name": "Prey Nob",
    "districtId": "1802",
    "code": "180207"
  },
  {
    "id": "180208",
    "name": "Ream",
    "districtId": "1802",
    "code": "180208"
  },
  {
    "id": "180209",
    "name": "Sameakki",
    "districtId": "1802",
    "code": "180209"
  },
  {
    "id": "180210",
    "name": "Samrong",
    "districtId": "1802",
    "code": "180210"
  },
  {
    "id": "180211",
    "name": "Tuek L'ak",
    "districtId": "1802",
    "code": "180211"
  },
  {
    "id": "180212",
    "name": "Tuek Thla",
    "districtId": "1802",
    "code": "180212"
  },
  {
    "id": "180213",
    "name": "Tuol Totueng",
    "districtId": "1802",
    "code": "180213"
  },
  {
    "id": "180214",
    "name": "Veal Renh",
    "districtId": "1802",
    "code": "180214"
  },
  {
    "id": "180215",
    "name": "Ta Ney",
    "districtId": "1802",
    "code": "180215"
  },
  {
    "id": "180301",
    "name": "Kampenh",
    "districtId": "1803",
    "code": "180301"
  },
  {
    "id": "180302",
    "name": "Ou Treh",
    "districtId": "1803",
    "code": "180302"
  },
  {
    "id": "180303",
    "name": "Tumnob Rolok",
    "districtId": "1803",
    "code": "180303"
  },
  {
    "id": "180304",
    "name": "Kaev Phos",
    "districtId": "1803",
    "code": "180304"
  },
  {
    "id": "180401",
    "name": "Chamkar Luong",
    "districtId": "1804",
    "code": "180401"
  },
  {
    "id": "180402",
    "name": "Kampong Seila",
    "districtId": "1804",
    "code": "180402"
  },
  {
    "id": "180403",
    "name": "Ou Bak Roteh",
    "districtId": "1804",
    "code": "180403"
  },
  {
    "id": "180404",
    "name": "Stueng Chhay",
    "districtId": "1804",
    "code": "180404"
  },
  {
    "id": "180501",
    "name": "Kaoh Rung",
    "districtId": "1805",
    "code": "180501"
  },
  {
    "id": "180502",
    "name": "Koah Rung Sonlem",
    "districtId": "1805",
    "code": "180502"
  },
  {
    "id": "190101",
    "name": "Kamphun",
    "districtId": "1901",
    "code": "190101"
  },
  {
    "id": "190102",
    "name": "Kbal Romeas",
    "districtId": "1901",
    "code": "190102"
  },
  {
    "id": "190103",
    "name": "Phluk",
    "districtId": "1901",
    "code": "190103"
  },
  {
    "id": "190104",
    "name": "Samkhuoy",
    "districtId": "1901",
    "code": "190104"
  },
  {
    "id": "190105",
    "name": "Sdau",
    "districtId": "1901",
    "code": "190105"
  },
  {
    "id": "190106",
    "name": "Srae Kor",
    "districtId": "1901",
    "code": "190106"
  },
  {
    "id": "190107",
    "name": "Ta Lat",
    "districtId": "1901",
    "code": "190107"
  },
  {
    "id": "190201",
    "name": "Kaoh Preah",
    "districtId": "1902",
    "code": "190201"
  },
  {
    "id": "190202",
    "name": "Kaoh Sampeay",
    "districtId": "1902",
    "code": "190202"
  },
  {
    "id": "190203",
    "name": "Kaoh Sralay",
    "districtId": "1902",
    "code": "190203"
  },
  {
    "id": "190204",
    "name": "Ou Mreah",
    "districtId": "1902",
    "code": "190204"
  },
  {
    "id": "190205",
    "name": "Ou Ruessei Kandal",
    "districtId": "1902",
    "code": "190205"
  },
  {
    "id": "190206",
    "name": "Siem Bouk",
    "districtId": "1902",
    "code": "190206"
  },
  {
    "id": "190207",
    "name": "Srae Krasang",
    "districtId": "1902",
    "code": "190207"
  },
  {
    "id": "190301",
    "name": "Preaek Meas",
    "districtId": "1903",
    "code": "190301"
  },
  {
    "id": "190302",
    "name": "Sekong",
    "districtId": "1903",
    "code": "190302"
  },
  {
    "id": "190303",
    "name": "Santepheap",
    "districtId": "1903",
    "code": "190303"
  },
  {
    "id": "190304",
    "name": "Srae Sambour",
    "districtId": "1903",
    "code": "190304"
  },
  {
    "id": "190305",
    "name": "Tma Kaev",
    "districtId": "1903",
    "code": "190305"
  },
  {
    "id": "190401",
    "name": "Stueng Traeng",
    "districtId": "1904",
    "code": "190401"
  },
  {
    "id": "190402",
    "name": "Srah Ruessei",
    "districtId": "1904",
    "code": "190402"
  },
  {
    "id": "190403",
    "name": "Preah Bat",
    "districtId": "1904",
    "code": "190403"
  },
  {
    "id": "190404",
    "name": "Sameakki",
    "districtId": "1904",
    "code": "190404"
  },
  {
    "id": "190501",
    "name": "Anlong Phe",
    "districtId": "1905",
    "code": "190501"
  },
  {
    "id": "190502",
    "name": "Chamkar Leu",
    "districtId": "1905",
    "code": "190502"
  },
  {
    "id": "190503",
    "name": "Kang Cham",
    "districtId": "1905",
    "code": "190503"
  },
  {
    "id": "190505",
    "name": "Anlong Chrey",
    "districtId": "1905",
    "code": "190505"
  },
  {
    "id": "190506",
    "name": "Ou Rai",
    "districtId": "1905",
    "code": "190506"
  },
  {
    "id": "190509",
    "name": "Sam Ang",
    "districtId": "1905",
    "code": "190509"
  },
  {
    "id": "190510",
    "name": "Srae Ruessei",
    "districtId": "1905",
    "code": "190510"
  },
  {
    "id": "190511",
    "name": "Thala Barivat",
    "districtId": "1905",
    "code": "190511"
  },
  {
    "id": "190601",
    "name": "Ou Svay",
    "districtId": "1906",
    "code": "190601"
  },
  {
    "id": "190602",
    "name": "Kaoh Snaeng",
    "districtId": "1906",
    "code": "190602"
  },
  {
    "id": "190603",
    "name": "Preah Rumkel",
    "districtId": "1906",
    "code": "190603"
  },
  {
    "id": "200103",
    "name": "Chantrea",
    "districtId": "2001",
    "code": "200103"
  },
  {
    "id": "200104",
    "name": "Chres",
    "districtId": "2001",
    "code": "200104"
  },
  {
    "id": "200105",
    "name": "Me Sar Thngak",
    "districtId": "2001",
    "code": "200105"
  },
  {
    "id": "200108",
    "name": "Prey Kokir",
    "districtId": "2001",
    "code": "200108"
  },
  {
    "id": "200109",
    "name": "Samraong",
    "districtId": "2001",
    "code": "200109"
  },
  {
    "id": "200110",
    "name": "Tuol Sdei",
    "districtId": "2001",
    "code": "200110"
  },
  {
    "id": "200201",
    "name": "Banteay Krang",
    "districtId": "2002",
    "code": "200201"
  },
  {
    "id": "200202",
    "name": "Nhor",
    "districtId": "2002",
    "code": "200202"
  },
  {
    "id": "200203",
    "name": "Khsaetr",
    "districtId": "2002",
    "code": "200203"
  },
  {
    "id": "200204",
    "name": "Preah Ponlea",
    "districtId": "2002",
    "code": "200204"
  },
  {
    "id": "200205",
    "name": "Prey Thum",
    "districtId": "2002",
    "code": "200205"
  },
  {
    "id": "200206",
    "name": "Reach Montir",
    "districtId": "2002",
    "code": "200206"
  },
  {
    "id": "200207",
    "name": "Samlei",
    "districtId": "2002",
    "code": "200207"
  },
  {
    "id": "200208",
    "name": "Samyaong",
    "districtId": "2002",
    "code": "200208"
  },
  {
    "id": "200209",
    "name": "Svay Ta Yean",
    "districtId": "2002",
    "code": "200209"
  },
  {
    "id": "200211",
    "name": "Thmei",
    "districtId": "2002",
    "code": "200211"
  },
  {
    "id": "200212",
    "name": "Tnaot",
    "districtId": "2002",
    "code": "200212"
  },
  {
    "id": "200301",
    "name": "Bos Mon",
    "districtId": "2003",
    "code": "200301"
  },
  {
    "id": "200302",
    "name": "Thmea",
    "districtId": "2003",
    "code": "200302"
  },
  {
    "id": "200303",
    "name": "Kampong Chak",
    "districtId": "2003",
    "code": "200303"
  },
  {
    "id": "200304",
    "name": "Chrung Popel",
    "districtId": "2003",
    "code": "200304"
  },
  {
    "id": "200305",
    "name": "Kampong Ampil",
    "districtId": "2003",
    "code": "200305"
  },
  {
    "id": "200306",
    "name": "Meun Chey",
    "districtId": "2003",
    "code": "200306"
  },
  {
    "id": "200307",
    "name": "Pong Tuek",
    "districtId": "2003",
    "code": "200307"
  },
  {
    "id": "200308",
    "name": "Sangkae",
    "districtId": "2003",
    "code": "200308"
  },
  {
    "id": "200309",
    "name": "Svay Chek",
    "districtId": "2003",
    "code": "200309"
  },
  {
    "id": "200310",
    "name": "Thna Thnong",
    "districtId": "2003",
    "code": "200310"
  },
  {
    "id": "200401",
    "name": "Ampil",
    "districtId": "2004",
    "code": "200401"
  },
  {
    "id": "200402",
    "name": "Andoung Pou",
    "districtId": "2004",
    "code": "200402"
  },
  {
    "id": "200403",
    "name": "Andoung Trabaek",
    "districtId": "2004",
    "code": "200403"
  },
  {
    "id": "200404",
    "name": "Angk Prasrae",
    "districtId": "2004",
    "code": "200404"
  },
  {
    "id": "200405",
    "name": "Chantrei",
    "districtId": "2004",
    "code": "200405"
  },
  {
    "id": "200406",
    "name": "Chrey Thum",
    "districtId": "2004",
    "code": "200406"
  },
  {
    "id": "200407",
    "name": "Doung",
    "districtId": "2004",
    "code": "200407"
  },
  {
    "id": "200408",
    "name": "Kampong Trach",
    "districtId": "2004",
    "code": "200408"
  },
  {
    "id": "200409",
    "name": "Kokir",
    "districtId": "2004",
    "code": "200409"
  },
  {
    "id": "200410",
    "name": "Krasang",
    "districtId": "2004",
    "code": "200410"
  },
  {
    "id": "200411",
    "name": "Mukh Da",
    "districtId": "2004",
    "code": "200411"
  },
  {
    "id": "200412",
    "name": "Mream",
    "districtId": "2004",
    "code": "200412"
  },
  {
    "id": "200413",
    "name": "Sambuor",
    "districtId": "2004",
    "code": "200413"
  },
  {
    "id": "200414",
    "name": "Sambatt Mean Chey",
    "districtId": "2004",
    "code": "200414"
  },
  {
    "id": "200415",
    "name": "Trapeang Sdau",
    "districtId": "2004",
    "code": "200415"
  },
  {
    "id": "200416",
    "name": "Tras",
    "districtId": "2004",
    "code": "200416"
  },
  {
    "id": "200501",
    "name": "Angk Ta Sou",
    "districtId": "2005",
    "code": "200501"
  },
  {
    "id": "200502",
    "name": "Basak",
    "districtId": "2005",
    "code": "200502"
  },
  {
    "id": "200503",
    "name": "Chambak",
    "districtId": "2005",
    "code": "200503"
  },
  {
    "id": "200504",
    "name": "Kampong Chamlang",
    "districtId": "2005",
    "code": "200504"
  },
  {
    "id": "200505",
    "name": "Ta Suos",
    "districtId": "2005",
    "code": "200505"
  },
  {
    "id": "200507",
    "name": "Chheu Teal",
    "districtId": "2005",
    "code": "200507"
  },
  {
    "id": "200508",
    "name": "Doun Sa",
    "districtId": "2005",
    "code": "200508"
  },
  {
    "id": "200509",
    "name": "Kouk Pring",
    "districtId": "2005",
    "code": "200509"
  },
  {
    "id": "200510",
    "name": "Kraol Kou",
    "districtId": "2005",
    "code": "200510"
  },
  {
    "id": "200511",
    "name": "Kruos",
    "districtId": "2005",
    "code": "200511"
  },
  {
    "id": "200512",
    "name": "Pouthi Reach",
    "districtId": "2005",
    "code": "200512"
  },
  {
    "id": "200513",
    "name": "Svay Angk",
    "districtId": "2005",
    "code": "200513"
  },
  {
    "id": "200514",
    "name": "Svay Chrum",
    "districtId": "2005",
    "code": "200514"
  },
  {
    "id": "200515",
    "name": "Svay Thum",
    "districtId": "2005",
    "code": "200515"
  },
  {
    "id": "200516",
    "name": "Svay Yea",
    "districtId": "2005",
    "code": "200516"
  },
  {
    "id": "200517",
    "name": "Thlok",
    "districtId": "2005",
    "code": "200517"
  },
  {
    "id": "200601",
    "name": "Svay Rieng",
    "districtId": "2006",
    "code": "200601"
  },
  {
    "id": "200602",
    "name": "Prey Chhlak",
    "districtId": "2006",
    "code": "200602"
  },
  {
    "id": "200603",
    "name": "Koy Trabaek",
    "districtId": "2006",
    "code": "200603"
  },
  {
    "id": "200604",
    "name": "Pou Ta Hao",
    "districtId": "2006",
    "code": "200604"
  },
  {
    "id": "200605",
    "name": "Chek",
    "districtId": "2006",
    "code": "200605"
  },
  {
    "id": "200606",
    "name": "Svay Toea",
    "districtId": "2006",
    "code": "200606"
  },
  {
    "id": "200607",
    "name": "Sangkhoar",
    "districtId": "2006",
    "code": "200607"
  },
  {
    "id": "200702",
    "name": "Koki Saom",
    "districtId": "2007",
    "code": "200702"
  },
  {
    "id": "200703",
    "name": "Kandieng Reay",
    "districtId": "2007",
    "code": "200703"
  },
  {
    "id": "200704",
    "name": "Monourom",
    "districtId": "2007",
    "code": "200704"
  },
  {
    "id": "200705",
    "name": "Popeaet",
    "districtId": "2007",
    "code": "200705"
  },
  {
    "id": "200706",
    "name": "Prey Ta Ei",
    "districtId": "2007",
    "code": "200706"
  },
  {
    "id": "200707",
    "name": "Prasoutr",
    "districtId": "2007",
    "code": "200707"
  },
  {
    "id": "200708",
    "name": "Romeang Thkaol",
    "districtId": "2007",
    "code": "200708"
  },
  {
    "id": "200709",
    "name": "Sambuor",
    "districtId": "2007",
    "code": "200709"
  },
  {
    "id": "200711",
    "name": "Svay Rumpear",
    "districtId": "2007",
    "code": "200711"
  },
  {
    "id": "200801",
    "name": "Bati",
    "districtId": "2008",
    "code": "200801"
  },
  {
    "id": "200802",
    "name": "Bavet",
    "districtId": "2008",
    "code": "200802"
  },
  {
    "id": "200803",
    "name": "Chrak Mtes",
    "districtId": "2008",
    "code": "200803"
  },
  {
    "id": "200804",
    "name": "Prasat",
    "districtId": "2008",
    "code": "200804"
  },
  {
    "id": "200805",
    "name": "Prey Angkunh",
    "districtId": "2008",
    "code": "200805"
  },
  {
    "id": "210101",
    "name": "Angkor Borei",
    "districtId": "2101",
    "code": "210101"
  },
  {
    "id": "210102",
    "name": "Ba Srae",
    "districtId": "2101",
    "code": "210102"
  },
  {
    "id": "210103",
    "name": "Kouk Thlok",
    "districtId": "2101",
    "code": "210103"
  },
  {
    "id": "210104",
    "name": "Ponley",
    "districtId": "2101",
    "code": "210104"
  },
  {
    "id": "210105",
    "name": "Preaek Phtoul",
    "districtId": "2101",
    "code": "210105"
  },
  {
    "id": "210106",
    "name": "Prey Phkoam",
    "districtId": "2101",
    "code": "210106"
  },
  {
    "id": "210201",
    "name": "Chambak",
    "districtId": "2102",
    "code": "210201"
  },
  {
    "id": "210202",
    "name": "Champei",
    "districtId": "2102",
    "code": "210202"
  },
  {
    "id": "210203",
    "name": "Doung",
    "districtId": "2102",
    "code": "210203"
  },
  {
    "id": "210204",
    "name": "Kandoeng",
    "districtId": "2102",
    "code": "210204"
  },
  {
    "id": "210205",
    "name": "Komar Reachea",
    "districtId": "2102",
    "code": "210205"
  },
  {
    "id": "210206",
    "name": "Krang Leav",
    "districtId": "2102",
    "code": "210206"
  },
  {
    "id": "210207",
    "name": "Krang Thnong",
    "districtId": "2102",
    "code": "210207"
  },
  {
    "id": "210208",
    "name": "Lumpong",
    "districtId": "2102",
    "code": "210208"
  },
  {
    "id": "210209",
    "name": "Pea Ream",
    "districtId": "2102",
    "code": "210209"
  },
  {
    "id": "210210",
    "name": "Pot Sar",
    "districtId": "2102",
    "code": "210210"
  },
  {
    "id": "210211",
    "name": "Sour Phi",
    "districtId": "2102",
    "code": "210211"
  },
  {
    "id": "210212",
    "name": "Tang Doung",
    "districtId": "2102",
    "code": "210212"
  },
  {
    "id": "210213",
    "name": "Tnaot",
    "districtId": "2102",
    "code": "210213"
  },
  {
    "id": "210214",
    "name": "Trapeang Krasang",
    "districtId": "2102",
    "code": "210214"
  },
  {
    "id": "210215",
    "name": "Trapeang Sab",
    "districtId": "2102",
    "code": "210215"
  },
  {
    "id": "210301",
    "name": "Borei Cholsar",
    "districtId": "2103",
    "code": "210301"
  },
  {
    "id": "210302",
    "name": "Chey Chouk",
    "districtId": "2103",
    "code": "210302"
  },
  {
    "id": "210303",
    "name": "Doung Khpos",
    "districtId": "2103",
    "code": "210303"
  },
  {
    "id": "210304",
    "name": "Kampong Krasang",
    "districtId": "2103",
    "code": "210304"
  },
  {
    "id": "210305",
    "name": "Kouk Pou",
    "districtId": "2103",
    "code": "210305"
  },
  {
    "id": "210401",
    "name": "Angk Prasat",
    "districtId": "2104",
    "code": "210401"
  },
  {
    "id": "210402",
    "name": "Preah Bat Choan Chum",
    "districtId": "2104",
    "code": "210402"
  },
  {
    "id": "210403",
    "name": "Kamnab",
    "districtId": "2104",
    "code": "210403"
  },
  {
    "id": "210404",
    "name": "Kampeaeng",
    "districtId": "2104",
    "code": "210404"
  },
  {
    "id": "210405",
    "name": "Kiri Chong Kaoh",
    "districtId": "2104",
    "code": "210405"
  },
  {
    "id": "210406",
    "name": "Kouk Prech",
    "districtId": "2104",
    "code": "210406"
  },
  {
    "id": "210407",
    "name": "Phnum Den",
    "districtId": "2104",
    "code": "210407"
  },
  {
    "id": "210408",
    "name": "Prey Ampok",
    "districtId": "2104",
    "code": "210408"
  },
  {
    "id": "210409",
    "name": "Prey Rumdeng",
    "districtId": "2104",
    "code": "210409"
  },
  {
    "id": "210410",
    "name": "Ream Andaeuk",
    "districtId": "2104",
    "code": "210410"
  },
  {
    "id": "210411",
    "name": "Saom",
    "districtId": "2104",
    "code": "210411"
  },
  {
    "id": "210412",
    "name": "Ta Ou",
    "districtId": "2104",
    "code": "210412"
  },
  {
    "id": "210501",
    "name": "Krapum Chhuk",
    "districtId": "2105",
    "code": "210501"
  },
  {
    "id": "210502",
    "name": "Pech Sar",
    "districtId": "2105",
    "code": "210502"
  },
  {
    "id": "210503",
    "name": "Prey Khla",
    "districtId": "2105",
    "code": "210503"
  },
  {
    "id": "210504",
    "name": "Prey Yuthka",
    "districtId": "2105",
    "code": "210504"
  },
  {
    "id": "210505",
    "name": "Romenh",
    "districtId": "2105",
    "code": "210505"
  },
  {
    "id": "210506",
    "name": "Thlea Prachum",
    "districtId": "2105",
    "code": "210506"
  },
  {
    "id": "210601",
    "name": "Angkanh",
    "districtId": "2106",
    "code": "210601"
  },
  {
    "id": "210602",
    "name": "Ban Kam",
    "districtId": "2106",
    "code": "210602"
  },
  {
    "id": "210603",
    "name": "Champa",
    "districtId": "2106",
    "code": "210603"
  },
  {
    "id": "210604",
    "name": "Char",
    "districtId": "2106",
    "code": "210604"
  },
  {
    "id": "210605",
    "name": "Kampeaeng",
    "districtId": "2106",
    "code": "210605"
  },
  {
    "id": "210606",
    "name": "Kampong Reab",
    "districtId": "2106",
    "code": "210606"
  },
  {
    "id": "210607",
    "name": "Kdanh",
    "districtId": "2106",
    "code": "210607"
  },
  {
    "id": "210608",
    "name": "Pou Rumchak",
    "districtId": "2106",
    "code": "210608"
  },
  {
    "id": "210609",
    "name": "Prey Kabbas",
    "districtId": "2106",
    "code": "210609"
  },
  {
    "id": "210610",
    "name": "Prey Lvea",
    "districtId": "2106",
    "code": "210610"
  },
  {
    "id": "210611",
    "name": "Prey Phdau",
    "districtId": "2106",
    "code": "210611"
  },
  {
    "id": "210612",
    "name": "Snao",
    "districtId": "2106",
    "code": "210612"
  },
  {
    "id": "210613",
    "name": "Tang Yab",
    "districtId": "2106",
    "code": "210613"
  },
  {
    "id": "210701",
    "name": "Boeng Tranh Khang Cheung",
    "districtId": "2107",
    "code": "210701"
  },
  {
    "id": "210702",
    "name": "Boeng Tranh Khang Tboung",
    "districtId": "2107",
    "code": "210702"
  },
  {
    "id": "210703",
    "name": "Cheung Kuon",
    "districtId": "2107",
    "code": "210703"
  },
  {
    "id": "210704",
    "name": "Chumreah Pen",
    "districtId": "2107",
    "code": "210704"
  },
  {
    "id": "210705",
    "name": "Khvav",
    "districtId": "2107",
    "code": "210705"
  },
  {
    "id": "210706",
    "name": "Lumchang",
    "districtId": "2107",
    "code": "210706"
  },
  {
    "id": "210707",
    "name": "Rovieng",
    "districtId": "2107",
    "code": "210707"
  },
  {
    "id": "210708",
    "name": "Samraong",
    "districtId": "2107",
    "code": "210708"
  },
  {
    "id": "210709",
    "name": "Soengh",
    "districtId": "2107",
    "code": "210709"
  },
  {
    "id": "210710",
    "name": "Sla",
    "districtId": "2107",
    "code": "210710"
  },
  {
    "id": "210711",
    "name": "Trea",
    "districtId": "2107",
    "code": "210711"
  },
  {
    "id": "210801",
    "name": "Baray",
    "districtId": "2108",
    "code": "210801"
  },
  {
    "id": "210802",
    "name": "Roka Knong",
    "districtId": "2108",
    "code": "210802"
  },
  {
    "id": "210803",
    "name": "Roka Krau",
    "districtId": "2108",
    "code": "210803"
  },
  {
    "id": "210901",
    "name": "Angk Ta Saom",
    "districtId": "2109",
    "code": "210901"
  },
  {
    "id": "210902",
    "name": "Cheang Tong",
    "districtId": "2109",
    "code": "210902"
  },
  {
    "id": "210903",
    "name": "Kus",
    "districtId": "2109",
    "code": "210903"
  },
  {
    "id": "210904",
    "name": "Leay Bour",
    "districtId": "2109",
    "code": "210904"
  },
  {
    "id": "210905",
    "name": "Nhaeng Nhang",
    "districtId": "2109",
    "code": "210905"
  },
  {
    "id": "210906",
    "name": "Ou Saray",
    "districtId": "2109",
    "code": "210906"
  },
  {
    "id": "210907",
    "name": "Trapeang Kranhoung",
    "districtId": "2109",
    "code": "210907"
  },
  {
    "id": "210908",
    "name": "Otdam Soriya",
    "districtId": "2109",
    "code": "210908"
  },
  {
    "id": "210909",
    "name": "Popel",
    "districtId": "2109",
    "code": "210909"
  },
  {
    "id": "210910",
    "name": "Samraong",
    "districtId": "2109",
    "code": "210910"
  },
  {
    "id": "210911",
    "name": "Srae Ronoung",
    "districtId": "2109",
    "code": "210911"
  },
  {
    "id": "210912",
    "name": "Ta Phem",
    "districtId": "2109",
    "code": "210912"
  },
  {
    "id": "210913",
    "name": "Tram Kak",
    "districtId": "2109",
    "code": "210913"
  },
  {
    "id": "210914",
    "name": "Trapeang Thum Khang Cheung",
    "districtId": "2109",
    "code": "210914"
  },
  {
    "id": "210915",
    "name": "Trapeang Thum Khang Tboung",
    "districtId": "2109",
    "code": "210915"
  },
  {
    "id": "211001",
    "name": "Angkanh",
    "districtId": "2110",
    "code": "211001"
  },
  {
    "id": "211002",
    "name": "Angk Khnor",
    "districtId": "2110",
    "code": "211002"
  },
  {
    "id": "211003",
    "name": "Chi Khma",
    "districtId": "2110",
    "code": "211003"
  },
  {
    "id": "211004",
    "name": "Khvav",
    "districtId": "2110",
    "code": "211004"
  },
  {
    "id": "211005",
    "name": "Prambei Mum",
    "districtId": "2110",
    "code": "211005"
  },
  {
    "id": "211006",
    "name": "Angk Kaev",
    "districtId": "2110",
    "code": "211006"
  },
  {
    "id": "211007",
    "name": "Prey Sloek",
    "districtId": "2110",
    "code": "211007"
  },
  {
    "id": "211008",
    "name": "Roneam",
    "districtId": "2110",
    "code": "211008"
  },
  {
    "id": "211009",
    "name": "Sambuor",
    "districtId": "2110",
    "code": "211009"
  },
  {
    "id": "211010",
    "name": "Sanlung",
    "districtId": "2110",
    "code": "211010"
  },
  {
    "id": "211011",
    "name": "Smaong",
    "districtId": "2110",
    "code": "211011"
  },
  {
    "id": "211012",
    "name": "Srangae",
    "districtId": "2110",
    "code": "211012"
  },
  {
    "id": "211013",
    "name": "Thlok",
    "districtId": "2110",
    "code": "211013"
  },
  {
    "id": "211014",
    "name": "Tralach",
    "districtId": "2110",
    "code": "211014"
  },
  {
    "id": "220101",
    "name": "Anlong Veaeng",
    "districtId": "2201",
    "code": "220101"
  },
  {
    "id": "220103",
    "name": "Trapeang Tav",
    "districtId": "2201",
    "code": "220103"
  },
  {
    "id": "220104",
    "name": "Trapeang Prei",
    "districtId": "2201",
    "code": "220104"
  },
  {
    "id": "220105",
    "name": "Thlat",
    "districtId": "2201",
    "code": "220105"
  },
  {
    "id": "220106",
    "name": "Lumtong",
    "districtId": "2201",
    "code": "220106"
  },
  {
    "id": "220201",
    "name": "Ampil",
    "districtId": "2202",
    "code": "220201"
  },
  {
    "id": "220202",
    "name": "Beng",
    "districtId": "2202",
    "code": "220202"
  },
  {
    "id": "220203",
    "name": "Kouk Khpos",
    "districtId": "2202",
    "code": "220203"
  },
  {
    "id": "220204",
    "name": "Kouk Mon",
    "districtId": "2202",
    "code": "220204"
  },
  {
    "id": "220301",
    "name": "Cheung Tien",
    "districtId": "2203",
    "code": "220301"
  },
  {
    "id": "220302",
    "name": "Chong Kal",
    "districtId": "2203",
    "code": "220302"
  },
  {
    "id": "220303",
    "name": "Krasang",
    "districtId": "2203",
    "code": "220303"
  },
  {
    "id": "220304",
    "name": "Pongro",
    "districtId": "2203",
    "code": "220304"
  },
  {
    "id": "220401",
    "name": "Bansay Reak",
    "districtId": "2204",
    "code": "220401"
  },
  {
    "id": "220402",
    "name": "Bos Sbov",
    "districtId": "2204",
    "code": "220402"
  },
  {
    "id": "220403",
    "name": "Koun Kriel",
    "districtId": "2204",
    "code": "220403"
  },
  {
    "id": "220404",
    "name": "Samraong",
    "districtId": "2204",
    "code": "220404"
  },
  {
    "id": "220405",
    "name": "Ou Smach",
    "districtId": "2204",
    "code": "220405"
  },
  {
    "id": "220501",
    "name": "Bak Anloung",
    "districtId": "2205",
    "code": "220501"
  },
  {
    "id": "220502",
    "name": "Ph'av",
    "districtId": "2205",
    "code": "220502"
  },
  {
    "id": "220503",
    "name": "Ou Svay",
    "districtId": "2205",
    "code": "220503"
  },
  {
    "id": "220504",
    "name": "Preah Pralay",
    "districtId": "2205",
    "code": "220504"
  },
  {
    "id": "220505",
    "name": "Tumnob Dach",
    "districtId": "2205",
    "code": "220505"
  },
  {
    "id": "220506",
    "name": "Trapeang Prasat",
    "districtId": "2205",
    "code": "220506"
  },
  {
    "id": "230101",
    "name": "Angkaol",
    "districtId": "2301",
    "code": "230101"
  },
  {
    "id": "230103",
    "name": "Pong Tuek",
    "districtId": "2301",
    "code": "230103"
  },
  {
    "id": "230201",
    "name": "Kaeb",
    "districtId": "2302",
    "code": "230201"
  },
  {
    "id": "230202",
    "name": "Prey Thum",
    "districtId": "2302",
    "code": "230202"
  },
  {
    "id": "230203",
    "name": "Ou Krasar",
    "districtId": "2302",
    "code": "230203"
  },
  {
    "id": "240101",
    "name": "Pailin",
    "districtId": "2401",
    "code": "240101"
  },
  {
    "id": "240102",
    "name": "Ou Ta Vau",
    "districtId": "2401",
    "code": "240102"
  },
  {
    "id": "240103",
    "name": "Tuol Lvea",
    "districtId": "2401",
    "code": "240103"
  },
  {
    "id": "240104",
    "name": "Bar Yakha",
    "districtId": "2401",
    "code": "240104"
  },
  {
    "id": "240201",
    "name": "Sala Krau",
    "districtId": "2402",
    "code": "240201"
  },
  {
    "id": "240202",
    "name": "Stueng Trang",
    "districtId": "2402",
    "code": "240202"
  },
  {
    "id": "240203",
    "name": "Stueng Kach",
    "districtId": "2402",
    "code": "240203"
  },
  {
    "id": "240204",
    "name": "Ou Andoung",
    "districtId": "2402",
    "code": "240204"
  },
  {
    "id": "250101",
    "name": "Chong Cheach",
    "districtId": "2501",
    "code": "250101"
  },
  {
    "id": "250102",
    "name": "Dambae",
    "districtId": "2501",
    "code": "250102"
  },
  {
    "id": "250103",
    "name": "Kouk Srok",
    "districtId": "2501",
    "code": "250103"
  },
  {
    "id": "250104",
    "name": "Neang Teut",
    "districtId": "2501",
    "code": "250104"
  },
  {
    "id": "250105",
    "name": "Seda",
    "districtId": "2501",
    "code": "250105"
  },
  {
    "id": "250106",
    "name": "Trapeang Pring",
    "districtId": "2501",
    "code": "250106"
  },
  {
    "id": "250107",
    "name": "Tuek Chrov",
    "districtId": "2501",
    "code": "250107"
  },
  {
    "id": "250201",
    "name": "Chhuk",
    "districtId": "2502",
    "code": "250201"
  },
  {
    "id": "250202",
    "name": "Chumnik",
    "districtId": "2502",
    "code": "250202"
  },
  {
    "id": "250203",
    "name": "Kampong Treas",
    "districtId": "2502",
    "code": "250203"
  },
  {
    "id": "250204",
    "name": "Kaoh Pir",
    "districtId": "2502",
    "code": "250204"
  },
  {
    "id": "250205",
    "name": "Krouch Chhmar",
    "districtId": "2502",
    "code": "250205"
  },
  {
    "id": "250206",
    "name": "Peus Muoy",
    "districtId": "2502",
    "code": "250206"
  },
  {
    "id": "250207",
    "name": "Peus Pir",
    "districtId": "2502",
    "code": "250207"
  },
  {
    "id": "250208",
    "name": "Preaek A chi",
    "districtId": "2502",
    "code": "250208"
  },
  {
    "id": "250209",
    "name": "Roka Khnor",
    "districtId": "2502",
    "code": "250209"
  },
  {
    "id": "250210",
    "name": "Svay Khleang",
    "districtId": "2502",
    "code": "250210"
  },
  {
    "id": "250211",
    "name": "Trea",
    "districtId": "2502",
    "code": "250211"
  },
  {
    "id": "250212",
    "name": "Tuol Snuol",
    "districtId": "2502",
    "code": "250212"
  },
  {
    "id": "250301",
    "name": "Chan Mul",
    "districtId": "2503",
    "code": "250301"
  },
  {
    "id": "250302",
    "name": "Choam",
    "districtId": "2503",
    "code": "250302"
  },
  {
    "id": "250303",
    "name": "Choam Kravien",
    "districtId": "2503",
    "code": "250303"
  },
  {
    "id": "250304",
    "name": "Choam Ta Mau",
    "districtId": "2503",
    "code": "250304"
  },
  {
    "id": "250305",
    "name": "Dar",
    "districtId": "2503",
    "code": "250305"
  },
  {
    "id": "250306",
    "name": "Kampoan",
    "districtId": "2503",
    "code": "250306"
  },
  {
    "id": "250307",
    "name": "Kokir",
    "districtId": "2503",
    "code": "250307"
  },
  {
    "id": "250308",
    "name": "Memong",
    "districtId": "2503",
    "code": "250308"
  },
  {
    "id": "250309",
    "name": "Memot",
    "districtId": "2503",
    "code": "250309"
  },
  {
    "id": "250310",
    "name": "Rumchek",
    "districtId": "2503",
    "code": "250310"
  },
  {
    "id": "250311",
    "name": "Rung",
    "districtId": "2503",
    "code": "250311"
  },
  {
    "id": "250312",
    "name": "Tonlung",
    "districtId": "2503",
    "code": "250312"
  },
  {
    "id": "250313",
    "name": "Tramung",
    "districtId": "2503",
    "code": "250313"
  },
  {
    "id": "250314",
    "name": "Triek",
    "districtId": "2503",
    "code": "250314"
  },
  {
    "id": "250401",
    "name": "Ampil Ta Pok",
    "districtId": "2504",
    "code": "250401"
  },
  {
    "id": "250402",
    "name": "Chak",
    "districtId": "2504",
    "code": "250402"
  },
  {
    "id": "250403",
    "name": "Damril",
    "districtId": "2504",
    "code": "250403"
  },
  {
    "id": "250404",
    "name": "Kong Chey",
    "districtId": "2504",
    "code": "250404"
  },
  {
    "id": "250405",
    "name": "Mien",
    "districtId": "2504",
    "code": "250405"
  },
  {
    "id": "250406",
    "name": "Preah Theat",
    "districtId": "2504",
    "code": "250406"
  },
  {
    "id": "250407",
    "name": "Tuol Souphi",
    "districtId": "2504",
    "code": "250407"
  },
  {
    "id": "250501",
    "name": "Dountei",
    "districtId": "2505",
    "code": "250501"
  },
  {
    "id": "250502",
    "name": "Kak",
    "districtId": "2505",
    "code": "250502"
  },
  {
    "id": "250503",
    "name": "Kandaol Chrum",
    "districtId": "2505",
    "code": "250503"
  },
  {
    "id": "250504",
    "name": "Kaong Kang",
    "districtId": "2505",
    "code": "250504"
  },
  {
    "id": "250505",
    "name": "Kraek",
    "districtId": "2505",
    "code": "250505"
  },
  {
    "id": "250506",
    "name": "Popel",
    "districtId": "2505",
    "code": "250506"
  },
  {
    "id": "250507",
    "name": "Trapeang Phlong",
    "districtId": "2505",
    "code": "250507"
  },
  {
    "id": "250508",
    "name": "Veal Mlu",
    "districtId": "2505",
    "code": "250508"
  },
  {
    "id": "250601",
    "name": "Suong",
    "districtId": "2506",
    "code": "250601"
  },
  {
    "id": "250602",
    "name": "Vihear Luong",
    "districtId": "2506",
    "code": "250602"
  },
  {
    "id": "250701",
    "name": "Anhchaeum",
    "districtId": "2507",
    "code": "250701"
  },
  {
    "id": "250702",
    "name": "Boeng Pruol",
    "districtId": "2507",
    "code": "250702"
  },
  {
    "id": "250703",
    "name": "Chikor",
    "districtId": "2507",
    "code": "250703"
  },
  {
    "id": "250704",
    "name": "Chirou Ti Muoy",
    "districtId": "2507",
    "code": "250704"
  },
  {
    "id": "250705",
    "name": "Chirou Ti Pir",
    "districtId": "2507",
    "code": "250705"
  },
  {
    "id": "250706",
    "name": "Chob",
    "districtId": "2507",
    "code": "250706"
  },
  {
    "id": "250707",
    "name": "Kor",
    "districtId": "2507",
    "code": "250707"
  },
  {
    "id": "250708",
    "name": "Lngieng",
    "districtId": "2507",
    "code": "250708"
  },
  {
    "id": "250709",
    "name": "Mong Riev",
    "districtId": "2507",
    "code": "250709"
  },
  {
    "id": "250710",
    "name": "Peam Chileang",
    "districtId": "2507",
    "code": "250710"
  },
  {
    "id": "250711",
    "name": "Roka Po Pram",
    "districtId": "2507",
    "code": "250711"
  },
  {
    "id": "250712",
    "name": "Sralab",
    "districtId": "2507",
    "code": "250712"
  },
  {
    "id": "250713",
    "name": "Thma Pech",
    "districtId": "2507",
    "code": "250713"
  },
  {
    "id": "250714",
    "name": "Tonle Bet",
    "districtId": "2507",
    "code": "250714"
  }
];