export const districts = [
  {
    "id": "102",
    "name": "Mongkol Borei",
    "provinceId": "1",
    "code": "0102"
  },
  {
    "id": "103",
    "name": "Phnum Srok",
    "provinceId": "1",
    "code": "0103"
  },
  {
    "id": "104",
    "name": "Preah Netr Preah",
    "provinceId": "1",
    "code": "0104"
  },
  {
    "id": "105",
    "name": "Ou Chrov",
    "provinceId": "1",
    "code": "0105"
  },
  {
    "id": "106",
    "name": "Serei Saophoan",
    "provinceId": "1",
    "code": "0106"
  },
  {
    "id": "107",
    "name": "Thma Puok",
    "provinceId": "1",
    "code": "0107"
  },
  {
    "id": "108",
    "name": "Svay Chek",
    "provinceId": "1",
    "code": "0108"
  },
  {
    "id": "109",
    "name": "Malai",
    "provinceId": "1",
    "code": "0109"
  },
  {
    "id": "110",
    "name": "Paoy Paet",
    "provinceId": "1",
    "code": "0110"
  },
  {
    "id": "201",
    "name": "Banan",
    "provinceId": "2",
    "code": "0201"
  },
  {
    "id": "202",
    "name": "Thma Koul",
    "provinceId": "2",
    "code": "0202"
  },
  {
    "id": "203",
    "name": "Battambang",
    "provinceId": "2",
    "code": "0203"
  },
  {
    "id": "204",
    "name": "Bavel",
    "provinceId": "2",
    "code": "0204"
  },
  {
    "id": "205",
    "name": "Aek Phnum",
    "provinceId": "2",
    "code": "0205"
  },
  {
    "id": "206",
    "name": "Moung Ruessei",
    "provinceId": "2",
    "code": "0206"
  },
  {
    "id": "207",
    "name": "Rotonak Mondol",
    "provinceId": "2",
    "code": "0207"
  },
  {
    "id": "208",
    "name": "Sangkae",
    "provinceId": "2",
    "code": "0208"
  },
  {
    "id": "209",
    "name": "Samlout",
    "provinceId": "2",
    "code": "0209"
  },
  {
    "id": "210",
    "name": "Sampov Lun",
    "provinceId": "2",
    "code": "0210"
  },
  {
    "id": "211",
    "name": "Phnum Proek",
    "provinceId": "2",
    "code": "0211"
  },
  {
    "id": "212",
    "name": "Kamrieng",
    "provinceId": "2",
    "code": "0212"
  },
  {
    "id": "213",
    "name": "Koas Krala",
    "provinceId": "2",
    "code": "0213"
  },
  {
    "id": "214",
    "name": "Rukh Kiri",
    "provinceId": "2",
    "code": "0214"
  },
  {
    "id": "301",
    "name": "Batheay",
    "provinceId": "3",
    "code": "0301"
  },
  {
    "id": "302",
    "name": "Chamkar Leu",
    "provinceId": "3",
    "code": "0302"
  },
  {
    "id": "303",
    "name": "Cheung Prey",
    "provinceId": "3",
    "code": "0303"
  },
  {
    "id": "305",
    "name": "Kampong Cham",
    "provinceId": "3",
    "code": "0305"
  },
  {
    "id": "306",
    "name": "Kampong Siem",
    "provinceId": "3",
    "code": "0306"
  },
  {
    "id": "307",
    "name": "Kang Meas",
    "provinceId": "3",
    "code": "0307"
  },
  {
    "id": "308",
    "name": "Kaoh Soutin",
    "provinceId": "3",
    "code": "0308"
  },
  {
    "id": "313",
    "name": "Prey Chhor",
    "provinceId": "3",
    "code": "0313"
  },
  {
    "id": "314",
    "name": "Srei Santhor",
    "provinceId": "3",
    "code": "0314"
  },
  {
    "id": "315",
    "name": "Stueng Trang",
    "provinceId": "3",
    "code": "0315"
  },
  {
    "id": "401",
    "name": "Baribour",
    "provinceId": "4",
    "code": "0401"
  },
  {
    "id": "402",
    "name": "Chol Kiri",
    "provinceId": "4",
    "code": "0402"
  },
  {
    "id": "403",
    "name": "Kampong Chhnang",
    "provinceId": "4",
    "code": "0403"
  },
  {
    "id": "404",
    "name": "Kampong Leaeng",
    "provinceId": "4",
    "code": "0404"
  },
  {
    "id": "405",
    "name": "Kampong Tralach",
    "provinceId": "4",
    "code": "0405"
  },
  {
    "id": "406",
    "name": "Rolea B'ier",
    "provinceId": "4",
    "code": "0406"
  },
  {
    "id": "407",
    "name": "Sameakki Mean Chey",
    "provinceId": "4",
    "code": "0407"
  },
  {
    "id": "408",
    "name": "Tuek Phos",
    "provinceId": "4",
    "code": "0408"
  },
  {
    "id": "501",
    "name": "Basedth",
    "provinceId": "5",
    "code": "0501"
  },
  {
    "id": "502",
    "name": "Chbar Mon",
    "provinceId": "5",
    "code": "0502"
  },
  {
    "id": "503",
    "name": "Kong Pisei",
    "provinceId": "5",
    "code": "0503"
  },
  {
    "id": "504",
    "name": "Aoral",
    "provinceId": "5",
    "code": "0504"
  },
  {
    "id": "505",
    "name": "Odongk",
    "provinceId": "5",
    "code": "0505"
  },
  {
    "id": "506",
    "name": "Phnum Sruoch",
    "provinceId": "5",
    "code": "0506"
  },
  {
    "id": "507",
    "name": "Samraong Tong",
    "provinceId": "5",
    "code": "0507"
  },
  {
    "id": "508",
    "name": "Thpong",
    "provinceId": "5",
    "code": "0508"
  },
  {
    "id": "601",
    "name": "Baray",
    "provinceId": "6",
    "code": "0601"
  },
  {
    "id": "602",
    "name": "Kampong Svay",
    "provinceId": "6",
    "code": "0602"
  },
  {
    "id": "603",
    "name": "Stueng Saen",
    "provinceId": "6",
    "code": "0603"
  },
  {
    "id": "604",
    "name": "Prasat Ballangk",
    "provinceId": "6",
    "code": "0604"
  },
  {
    "id": "605",
    "name": "Prasat Sambour",
    "provinceId": "6",
    "code": "0605"
  },
  {
    "id": "606",
    "name": "Sandan",
    "provinceId": "6",
    "code": "0606"
  },
  {
    "id": "607",
    "name": "Santuk",
    "provinceId": "6",
    "code": "0607"
  },
  {
    "id": "608",
    "name": "Stoung",
    "provinceId": "6",
    "code": "0608"
  },
  {
    "id": "609",
    "name": "Taing Kouk",
    "provinceId": "6",
    "code": "0609"
  },
  {
    "id": "701",
    "name": "Angkor Chey",
    "provinceId": "7",
    "code": "0701"
  },
  {
    "id": "702",
    "name": "Banteay Meas",
    "provinceId": "7",
    "code": "0702"
  },
  {
    "id": "703",
    "name": "Chhuk",
    "provinceId": "7",
    "code": "0703"
  },
  {
    "id": "704",
    "name": "Chum Kiri",
    "provinceId": "7",
    "code": "0704"
  },
  {
    "id": "705",
    "name": "Dang Tong",
    "provinceId": "7",
    "code": "0705"
  },
  {
    "id": "706",
    "name": "Kampong Trach",
    "provinceId": "7",
    "code": "0706"
  },
  {
    "id": "707",
    "name": "Tuek Chhou",
    "provinceId": "7",
    "code": "0707"
  },
  {
    "id": "708",
    "name": "Kampot",
    "provinceId": "7",
    "code": "0708"
  },
  {
    "id": "801",
    "name": "Kandal Stueng",
    "provinceId": "8",
    "code": "0801"
  },
  {
    "id": "802",
    "name": "Kien Svay",
    "provinceId": "8",
    "code": "0802"
  },
  {
    "id": "803",
    "name": "Khsach Kandal",
    "provinceId": "8",
    "code": "0803"
  },
  {
    "id": "804",
    "name": "Kaoh Thum",
    "provinceId": "8",
    "code": "0804"
  },
  {
    "id": "805",
    "name": "Leuk Daek",
    "provinceId": "8",
    "code": "0805"
  },
  {
    "id": "806",
    "name": "Lvea Aem",
    "provinceId": "8",
    "code": "0806"
  },
  {
    "id": "807",
    "name": "Mukh Kampul",
    "provinceId": "8",
    "code": "0807"
  },
  {
    "id": "808",
    "name": "Angk Snuol",
    "provinceId": "8",
    "code": "0808"
  },
  {
    "id": "809",
    "name": "Ponhea Lueu",
    "provinceId": "8",
    "code": "0809"
  },
  {
    "id": "810",
    "name": "S'ang",
    "provinceId": "8",
    "code": "0810"
  },
  {
    "id": "811",
    "name": "Ta Khmau",
    "provinceId": "8",
    "code": "0811"
  },
  {
    "id": "901",
    "name": "Botum Sakor",
    "provinceId": "9",
    "code": "0901"
  },
  {
    "id": "902",
    "name": "Kiri Sakor",
    "provinceId": "9",
    "code": "0902"
  },
  {
    "id": "903",
    "name": "Kaoh Kong",
    "provinceId": "9",
    "code": "0903"
  },
  {
    "id": "904",
    "name": "Khemara Phoumin",
    "provinceId": "9",
    "code": "0904"
  },
  {
    "id": "905",
    "name": "Mondol Seima",
    "provinceId": "9",
    "code": "0905"
  },
  {
    "id": "906",
    "name": "Srae Ambel",
    "provinceId": "9",
    "code": "0906"
  },
  {
    "id": "907",
    "name": "Thma Bang",
    "provinceId": "9",
    "code": "0907"
  },
  {
    "id": "1001",
    "name": "Chhloung",
    "provinceId": "10",
    "code": "1001"
  },
  {
    "id": "1002",
    "name": "Kracheh",
    "provinceId": "10",
    "code": "1002"
  },
  {
    "id": "1003",
    "name": "Prek Prasab",
    "provinceId": "10",
    "code": "1003"
  },
  {
    "id": "1004",
    "name": "Sambour",
    "provinceId": "10",
    "code": "1004"
  },
  {
    "id": "1005",
    "name": "Snuol",
    "provinceId": "10",
    "code": "1005"
  },
  {
    "id": "1006",
    "name": "Chetr Borei",
    "provinceId": "10",
    "code": "1006"
  },
  {
    "id": "1101",
    "name": "Kaev Seima",
    "provinceId": "11",
    "code": "1101"
  },
  {
    "id": "1102",
    "name": "Kaoh Nheaek",
    "provinceId": "11",
    "code": "1102"
  },
  {
    "id": "1103",
    "name": "Ou Reang",
    "provinceId": "11",
    "code": "1103"
  },
  {
    "id": "1104",
    "name": "Pech Chreada",
    "provinceId": "11",
    "code": "1104"
  },
  {
    "id": "1105",
    "name": "Saen Monourom",
    "provinceId": "11",
    "code": "1105"
  },
  {
    "id": "1201",
    "name": "Chamkar Mon",
    "provinceId": "12",
    "code": "1201"
  },
  {
    "id": "1202",
    "name": "Doun Penh",
    "provinceId": "12",
    "code": "1202"
  },
  {
    "id": "1203",
    "name": "Prampir Meakkakra",
    "provinceId": "12",
    "code": "1203"
  },
  {
    "id": "1204",
    "name": "Tuol Kouk",
    "provinceId": "12",
    "code": "1204"
  },
  {
    "id": "1205",
    "name": "Dangkao",
    "provinceId": "12",
    "code": "1205"
  },
  {
    "id": "1206",
    "name": "Mean Chey",
    "provinceId": "12",
    "code": "1206"
  },
  {
    "id": "1207",
    "name": "Russey Keo",
    "provinceId": "12",
    "code": "1207"
  },
  {
    "id": "1208",
    "name": "Saensokh",
    "provinceId": "12",
    "code": "1208"
  },
  {
    "id": "1209",
    "name": "Pur SenChey",
    "provinceId": "12",
    "code": "1209"
  },
  {
    "id": "1210",
    "name": "Chraoy Chongvar",
    "provinceId": "12",
    "code": "1210"
  },
  {
    "id": "1211",
    "name": "Praek Pnov",
    "provinceId": "12",
    "code": "1211"
  },
  {
    "id": "1212",
    "name": "Chbar Ampov",
    "provinceId": "12",
    "code": "1212"
  },
  {
    "id": "1213",
    "name": "Boeng Keng Kang",
    "provinceId": "12",
    "code": "1213"
  },
  {
    "id": "1214",
    "name": "Kamboul",
    "provinceId": "12",
    "code": "1214"
  },
  {
    "id": "1301",
    "name": "Chey Saen",
    "provinceId": "13",
    "code": "1301"
  },
  {
    "id": "1302",
    "name": "Chhaeb",
    "provinceId": "13",
    "code": "1302"
  },
  {
    "id": "1303",
    "name": "Choam Ksant",
    "provinceId": "13",
    "code": "1303"
  },
  {
    "id": "1304",
    "name": "Kuleaen",
    "provinceId": "13",
    "code": "1304"
  },
  {
    "id": "1305",
    "name": "Rovieng",
    "provinceId": "13",
    "code": "1305"
  },
  {
    "id": "1306",
    "name": "Sangkum Thmei",
    "provinceId": "13",
    "code": "1306"
  },
  {
    "id": "1307",
    "name": "Tbaeng Mean Chey",
    "provinceId": "13",
    "code": "1307"
  },
  {
    "id": "1308",
    "name": "Preah Vihear",
    "provinceId": "13",
    "code": "1308"
  },
  {
    "id": "1401",
    "name": "Ba Phnum",
    "provinceId": "14",
    "code": "1401"
  },
  {
    "id": "1402",
    "name": "Kamchay Mear",
    "provinceId": "14",
    "code": "1402"
  },
  {
    "id": "1403",
    "name": "Kampong Trabaek",
    "provinceId": "14",
    "code": "1403"
  },
  {
    "id": "1404",
    "name": "Kanhchriech",
    "provinceId": "14",
    "code": "1404"
  },
  {
    "id": "1405",
    "name": "Me Sang",
    "provinceId": "14",
    "code": "1405"
  },
  {
    "id": "1406",
    "name": "Peam Chor",
    "provinceId": "14",
    "code": "1406"
  },
  {
    "id": "1407",
    "name": "Peam Ro",
    "provinceId": "14",
    "code": "1407"
  },
  {
    "id": "1408",
    "name": "Pea Reang",
    "provinceId": "14",
    "code": "1408"
  },
  {
    "id": "1409",
    "name": "Preah Sdach",
    "provinceId": "14",
    "code": "1409"
  },
  {
    "id": "1410",
    "name": "Prey Veng",
    "provinceId": "14",
    "code": "1410"
  },
  {
    "id": "1411",
    "name": "Pur Rieng",
    "provinceId": "14",
    "code": "1411"
  },
  {
    "id": "1412",
    "name": "Sithor Kandal",
    "provinceId": "14",
    "code": "1412"
  },
  {
    "id": "1413",
    "name": "Svay Antor",
    "provinceId": "14",
    "code": "1413"
  },
  {
    "id": "1501",
    "name": "Bakan",
    "provinceId": "15",
    "code": "1501"
  },
  {
    "id": "1502",
    "name": "Kandieng",
    "provinceId": "15",
    "code": "1502"
  },
  {
    "id": "1503",
    "name": "Krakor",
    "provinceId": "15",
    "code": "1503"
  },
  {
    "id": "1504",
    "name": "Phnum Kravanh",
    "provinceId": "15",
    "code": "1504"
  },
  {
    "id": "1505",
    "name": "Pursat",
    "provinceId": "15",
    "code": "1505"
  },
  {
    "id": "1506",
    "name": "Veal Veaeng",
    "provinceId": "15",
    "code": "1506"
  },
  {
    "id": "1507",
    "name": "Ta Lou Senchey",
    "provinceId": "15",
    "code": "1507"
  },
  {
    "id": "1601",
    "name": "Andoung Meas",
    "provinceId": "16",
    "code": "1601"
  },
  {
    "id": "1602",
    "name": "Ban Lung",
    "provinceId": "16",
    "code": "1602"
  },
  {
    "id": "1603",
    "name": "Bar Kaev",
    "provinceId": "16",
    "code": "1603"
  },
  {
    "id": "1604",
    "name": "Koun Mom",
    "provinceId": "16",
    "code": "1604"
  },
  {
    "id": "1605",
    "name": "Lumphat",
    "provinceId": "16",
    "code": "1605"
  },
  {
    "id": "1606",
    "name": "Ou Chum",
    "provinceId": "16",
    "code": "1606"
  },
  {
    "id": "1607",
    "name": "Ou Ya Dav",
    "provinceId": "16",
    "code": "1607"
  },
  {
    "id": "1608",
    "name": "Ta Veaeng",
    "provinceId": "16",
    "code": "1608"
  },
  {
    "id": "1609",
    "name": "Veun Sai",
    "provinceId": "16",
    "code": "1609"
  },
  {
    "id": "1701",
    "name": "Angkor Chum",
    "provinceId": "17",
    "code": "1701"
  },
  {
    "id": "1702",
    "name": "Angkor Thum",
    "provinceId": "17",
    "code": "1702"
  },
  {
    "id": "1703",
    "name": "Banteay Srei",
    "provinceId": "17",
    "code": "1703"
  },
  {
    "id": "1704",
    "name": "Chi Kraeng",
    "provinceId": "17",
    "code": "1704"
  },
  {
    "id": "1706",
    "name": "Kralanh",
    "provinceId": "17",
    "code": "1706"
  },
  {
    "id": "1707",
    "name": "Puok",
    "provinceId": "17",
    "code": "1707"
  },
  {
    "id": "1709",
    "name": "Prasat Bakong",
    "provinceId": "17",
    "code": "1709"
  },
  {
    "id": "1710",
    "name": "Siem Reap",
    "provinceId": "17",
    "code": "1710"
  },
  {
    "id": "1711",
    "name": "Soutr Nikom",
    "provinceId": "17",
    "code": "1711"
  },
  {
    "id": "1712",
    "name": "Srei Snam",
    "provinceId": "17",
    "code": "1712"
  },
  {
    "id": "1713",
    "name": "Svay Leu",
    "provinceId": "17",
    "code": "1713"
  },
  {
    "id": "1714",
    "name": "Varin",
    "provinceId": "17",
    "code": "1714"
  },
  {
    "id": "1801",
    "name": "Preah Sihanouk",
    "provinceId": "18",
    "code": "1801"
  },
  {
    "id": "1802",
    "name": "Prey Nob",
    "provinceId": "18",
    "code": "1802"
  },
  {
    "id": "1803",
    "name": "Stueng Hav",
    "provinceId": "18",
    "code": "1803"
  },
  {
    "id": "1804",
    "name": "Kampong Seila",
    "provinceId": "18",
    "code": "1804"
  },
  {
    "id": "1805",
    "name": "Kaoh Rung",
    "provinceId": "18",
    "code": "1805"
  },
  {
    "id": "1901",
    "name": "Sesan",
    "provinceId": "19",
    "code": "1901"
  },
  {
    "id": "1902",
    "name": "Siem Bouk",
    "provinceId": "19",
    "code": "1902"
  },
  {
    "id": "1903",
    "name": "Siem Pang",
    "provinceId": "19",
    "code": "1903"
  },
  {
    "id": "1904",
    "name": "Stueng Traeng",
    "provinceId": "19",
    "code": "1904"
  },
  {
    "id": "1905",
    "name": "Thala Barivat",
    "provinceId": "19",
    "code": "1905"
  },
  {
    "id": "1906",
    "name": "Borei Ou Svay Senchey",
    "provinceId": "19",
    "code": "1906"
  },
  {
    "id": "2001",
    "name": "Chantrea",
    "provinceId": "20",
    "code": "2001"
  },
  {
    "id": "2002",
    "name": "Kampong Rou",
    "provinceId": "20",
    "code": "2002"
  },
  {
    "id": "2003",
    "name": "Rumduol",
    "provinceId": "20",
    "code": "2003"
  },
  {
    "id": "2004",
    "name": "Romeas Haek",
    "provinceId": "20",
    "code": "2004"
  },
  {
    "id": "2005",
    "name": "Svay Chrum",
    "provinceId": "20",
    "code": "2005"
  },
  {
    "id": "2006",
    "name": "Svay Rieng",
    "provinceId": "20",
    "code": "2006"
  },
  {
    "id": "2007",
    "name": "Svay Teab",
    "provinceId": "20",
    "code": "2007"
  },
  {
    "id": "2008",
    "name": "Bavet",
    "provinceId": "20",
    "code": "2008"
  },
  {
    "id": "2101",
    "name": "Angkor Borei",
    "provinceId": "21",
    "code": "2101"
  },
  {
    "id": "2102",
    "name": "Bati",
    "provinceId": "21",
    "code": "2102"
  },
  {
    "id": "2103",
    "name": "Borei Cholsar",
    "provinceId": "21",
    "code": "2103"
  },
  {
    "id": "2104",
    "name": "Kiri Vong",
    "provinceId": "21",
    "code": "2104"
  },
  {
    "id": "2105",
    "name": "Kaoh Andaet",
    "provinceId": "21",
    "code": "2105"
  },
  {
    "id": "2106",
    "name": "Prey Kabbas",
    "provinceId": "21",
    "code": "2106"
  },
  {
    "id": "2107",
    "name": "Samraong",
    "provinceId": "21",
    "code": "2107"
  },
  {
    "id": "2108",
    "name": "Doun Kaev",
    "provinceId": "21",
    "code": "2108"
  },
  {
    "id": "2109",
    "name": "Tram Kak",
    "provinceId": "21",
    "code": "2109"
  },
  {
    "id": "2110",
    "name": "Treang",
    "provinceId": "21",
    "code": "2110"
  },
  {
    "id": "2201",
    "name": "Anlong Veaeng",
    "provinceId": "22",
    "code": "2201"
  },
  {
    "id": "2202",
    "name": "Banteay Ampil",
    "provinceId": "22",
    "code": "2202"
  },
  {
    "id": "2203",
    "name": "Chong Kal",
    "provinceId": "22",
    "code": "2203"
  },
  {
    "id": "2204",
    "name": "Samraong",
    "provinceId": "22",
    "code": "2204"
  },
  {
    "id": "2205",
    "name": "Trapeang Prasat",
    "provinceId": "22",
    "code": "2205"
  },
  {
    "id": "2301",
    "name": "Damnak Chang'aeur",
    "provinceId": "23",
    "code": "2301"
  },
  {
    "id": "2302",
    "name": "Kaeb",
    "provinceId": "23",
    "code": "2302"
  },
  {
    "id": "2401",
    "name": "Pailin",
    "provinceId": "24",
    "code": "2401"
  },
  {
    "id": "2402",
    "name": "Sala Krau",
    "provinceId": "24",
    "code": "2402"
  },
  {
    "id": "2501",
    "name": "Dambae",
    "provinceId": "25",
    "code": "2501"
  },
  {
    "id": "2502",
    "name": "Krouch Chhmar",
    "provinceId": "25",
    "code": "2502"
  },
  {
    "id": "2503",
    "name": "Memot",
    "provinceId": "25",
    "code": "2503"
  },
  {
    "id": "2504",
    "name": "Ou Reang Ov",
    "provinceId": "25",
    "code": "2504"
  },
  {
    "id": "2505",
    "name": "Ponhea Kraek",
    "provinceId": "25",
    "code": "2505"
  },
  {
    "id": "2506",
    "name": "Suong",
    "provinceId": "25",
    "code": "2506"
  },
  {
    "id": "2507",
    "name": "Tboung Khmum",
    "provinceId": "25",
    "code": "2507"
  }
];