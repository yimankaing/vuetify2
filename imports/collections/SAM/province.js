import SimpleSchema from 'simpl-schema';
export const SAM_Province = new Mongo.Collection('sam_province');

SAM_Province.schema = new SimpleSchema({
  id: String,
  name: String,
  code: String,
});

SAM_Province.attachSchema(SAM_Province.schema);