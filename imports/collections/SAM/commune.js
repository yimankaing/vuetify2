import SimpleSchema from 'simpl-schema';
export const SAM_Commune = new Mongo.Collection('sam_commune');

SAM_Commune.schema = new SimpleSchema({
  id: String,
  name: String,
  code: String,
  districtId: String,
});

SAM_Commune.attachSchema(SAM_Commune.schema);