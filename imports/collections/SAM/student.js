import SimpleSchema from 'simpl-schema';
import moment from 'moment';

export const SAM_Student = new Mongo.Collection('sam_student');

SAM_Student.schema = new SimpleSchema({
  createdAt: {
    type: Date,
    optional: true,
    autoValue() {
      if (this.isInsert) {
        return moment().toDate();
      }
    }
  },
  updatedAt: {
    type: Date,
    optional: true,
    autoValue() {
      if (this.isUpdate) {
        return moment().toDate();
      }
    }
  },
  createdUser: {
    type: String,
    optional: true,
    autoValue() {
      if (this.isInsert) {
        return Meteor.userId();
      }
    }
  },
  updatedUser: {
    type: String,
    optional: true,
    autoValue() {
      if (this.isUpdate) {
        return Meteor.userId();
      }
    }
  },
  //general
  name: {type: String, optional: true},
  sex: {type: String, optional: true},
  age: {type: String, optional: true},
  provinceId: {type: String, optional: true},
  districtId: {type: String, optional: true},
  communeId: {type: String, optional: true},
  villageId: {type: String, optional: true},
  phone: {type: String, optional: true},
  comment: {type: String, optional: true},
});

SAM_Student.attachSchema(SAM_Student.schema);

//helper
export const SAM_StudentHelper = new Mongo.Collection('sam_studentHelper');
SAM_StudentHelper.schema = new SimpleSchema({
  id: {
    type: String
  }
});

SAM_StudentHelper.attachSchema(SAM_StudentHelper.schema);
