import SimpleSchema from 'simpl-schema';
export const SAM_District = new Mongo.Collection('sam_district');

SAM_District.schema = new SimpleSchema({
  id: String,
  name: String,
  code: String,
  provinceId: String,
});

SAM_District.attachSchema(SAM_District.schema);