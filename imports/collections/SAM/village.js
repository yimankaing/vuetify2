import SimpleSchema from 'simpl-schema';
export const SAM_Village = new Mongo.Collection('sam_village');

SAM_Village.schema = new SimpleSchema({
  id: String,
  name: String,
  code: String,
  communeId: String,
});

SAM_Village.attachSchema(SAM_Village.schema);