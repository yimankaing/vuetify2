import SimpleSchema from 'simpl-schema';
export const SAM_LevelProvince = new Mongo.Collection('sam_levelProvince');

SAM_LevelProvince.schema = new SimpleSchema({
  name: String,
  code: String,
});

SAM_LevelProvince.attachSchema(SAM_LevelProvince.schema);