import SimpleSchema from 'simpl-schema';
export const SAM_LevelSite = new Mongo.Collection('sam_levelSite');

SAM_LevelSite.schema = new SimpleSchema({
  name: String,
  code: String,
  provinceId: String,
});

SAM_LevelSite.attachSchema(SAM_LevelSite.schema);