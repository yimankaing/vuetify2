import SimpleSchema from 'simpl-schema';
import moment from 'moment';

export const CORE_OnlineUserHistory = new Mongo.Collection('core_onlineUserHistory');

const lastLoginSchema = new SimpleSchema({
  date: {type: Date, optional: true},
  ipAddr: {type: String, optional: true},
  userAgent: {type: String, optional: true}
});

const usersSchema = new SimpleSchema({
  userId: String,
  lastLogin: lastLoginSchema,
  username: String,
});

CORE_OnlineUserHistory.schema = new SimpleSchema({
  createdAt: {
    type: Date,
    optional: true,
    autoValue() {
      if (this.isInsert) {
        return moment().toDate();
      }
    }
  },
  users: [usersSchema],
});

CORE_OnlineUserHistory.attachSchema(CORE_OnlineUserHistory.schema);