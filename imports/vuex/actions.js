// https://vuex.vuejs.org/en/actions.html

export default {
  updateUser({commit, state}, payload) {
    commit("setUser", payload);
  },
  logout({commit, state}, payload) {
    state.core.loading.visible = true;
    setTimeout(() => {
      Meteor.logout(() => {
        console.log("user logged out");
        state.core.loading.visible = false;
        state.currentModule = null;
        commit("setUser", null);
      });
    }, 500);

  },
  updateModule({commit, state}, payload) {
    commit("setModule", payload);
  }
}
