// https://vuex.vuejs.org/en/modules.html
import i18n from "../../../client/i18n";

export const core = {
  namespaced: true,
  state: {
    currentLocale: "en",
    isDark: false,
    notification: {
      top: true,
      right: true,
      left: false,
      bottom: false,
      visible: false,
      timeout: 3000,
      text: "",
      icon: "notifications",
      color: "success"
    },
    loading: {
      color: "primary",
      visible: false,
      text: ""
    },
    toolbarTab:{
      items: [],
      currentTab: 0
    }
  },
  mutations: {
    setLocale(state, value) {
      state.currentLocale = value;
    },
    setNotification(state, value) {
      state.notification = value;
    },
    setLoading(state, value) {
      state.loading = value;
    },
    setDark(state, value) {
      state.isDark = value;
    },
    setToolbarTab(state, value){
      state.toolbarTab = value;
    }
  },
  actions: {
    changeLocale({commit, state}, payload) {
      i18n.locale = payload;
      commit("setLocale", payload);
    },
    showNotification({commit, state}, payload) {
      commit("setNotification", {...state.notification, ...payload});
    },
    showLoading({commit, state}, payload) {
      commit("setLoading", {...state.loading, ...payload});
    },
    changeTheme({commit, state}, payload) {
      commit("setDark", payload);
    },
    mountToolbarTab({commit, state}, payload) {
      commit("setToolbarTab", {...state.toolbarTab, ...payload});
    }
  },
  getters: {}
};