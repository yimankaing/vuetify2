// https://vuex.vuejs.org/en/mutations.html

export default {
  setUser(state, value) {
    state.currentUser = value;
  },
  setModule(state, value) {
    state.currentModule = value;
  },
};