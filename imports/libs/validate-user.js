export default class ValidateUser {
  static ifUserNotSignedIn({msg}) {
    if (!Meteor.userId()) {
      // return false;
      throw new Meteor.Error("You're not login",
        `${msg}`);
    }
  }
}