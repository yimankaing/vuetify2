export const xlsxStyle = {
  alignRightMiddle: {
    alignment: {
      horizontal: 'right',
      vertical: 'center'
    },
  },
  alignCenterMiddle: {
    alignment: {
      horizontal: 'center',
      vertical: 'center'
    },
  },
  mainHeader: {
    alignment: {
      horizontal: 'center',
      vertical: 'center'
    },
    font: {
      name: "Khmer OS Muol Light",
      color: {
        rgb: '000000'
      },
      size: 18,
      bold: false,
      underline: false
    }
  },
  subHeader: {
    alignment: {
      horizontal: 'center',
      vertical: 'center'
    },
    font: {
      name: "Khmer OS Battambang",
      color: {
        rgb: '000000'
      },
      size: 15,
      bold: true,
      underline: false
    }
  },
  header: {
    alignment: {
      horizontal: 'center',
      vertical: 'center'
    },
    border: {
      top: {
        style: 'thin',
        color: {rgb: "000000"}
      }, right: {
        style: 'thin',
        color: {rgb: "000000"}
      }, left: {
        style: 'thin',
        color: {rgb: "000000"}
      }, bottom: {
        style: 'thin',
        color: {rgb: "000000"}
      }
    },
    fill: {
      type: 'pattern',
      pattern: 'solid',
      fgColor: {
        // rgb: 'dfdfdf',
        argb: 'cccccc'
      }
    },
    font: {
      name: "Khmer OS Muol Light",
      color: {
        rgb: '000000'
      },
      size: 11,
      bold: false,
    }
  },
  numInt: {
    numFmt: "0"
  },
  percentage: {
    numFmt: "0.00%"
  },
  date: {
    numFmt: "dd-mmm-yy"
  },
  cell: {
    border: {
      top: {
        style: 'thin',
        color: {rgb: "000000"}
      }, right: {
        style: 'thin',
        color: {rgb: "000000"}
      }, left: {
        style: 'thin',
        color: {rgb: "000000"}
      }, bottom: {
        style: 'thin',
        color: {rgb: "000000"}
      }
    },
    // fill: {
    //     fgColor: {
    //         rgb: 'FF000000'
    //     }
    // },
    font: {
      name: 'Khmer OS Battambang',
      color: {
        rgb: '000000'
      },
      size: 11,
      bold: false,
    }
  },
  cellFont: {
    font: {
      name: 'Khmer OS Battambang',
      color: {
        rgb: '000000'
      },
      size: 11,
      bold: false,
    }
  }
};