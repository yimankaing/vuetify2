import {format, subDays} from 'date-fns';
import store from "../../imports/vuex/store";
//lang
import {t} from '../../imports/libs/constants';

export const dateRangeOptions = {
  maxDate: 'null',
  startDate: format(subDays(new Date(), 7), 'YYYY-MM-DD'),
  endDate: format(new Date(), 'YYYY-MM-DD'),
  format: 'MM/DD/YYYY',
  presets: [
    {
      label: t[store.state.core.currentLocale].today,
      range: [
        format(new Date(), 'YYYY-MM-DD'),
        format(new Date(), 'YYYY-MM-DD'),
      ],
    },
    {
      label: t[store.state.core.currentLocale].yesterday,
      range: [
        format(subDays(new Date(), 1), 'YYYY-MM-DD'),
        format(subDays(new Date(), 1), 'YYYY-MM-DD'),
      ],
    },
    {
      label: t[store.state.core.currentLocale].sevenDaysAgo,
      range: [
        format(subDays(new Date(), 7), 'YYYY-MM-DD'),
        format(subDays(new Date(), 1), 'YYYY-MM-DD'),
      ],
    },
    {
      label: t[store.state.core.currentLocale].fourteenDaysAgo,
      range: [
        format(subDays(new Date(), 14), 'YYYY-MM-DD'),
        format(subDays(new Date(), 1), 'YYYY-MM-DD'),
      ],
    },
    {
      label: t[store.state.core.currentLocale].thirtyDaysAgo,
      range: [
        format(subDays(new Date(), 30), 'YYYY-MM-DD'),
        format(subDays(new Date(), 1), 'YYYY-MM-DD'),
      ],
    },
  ],
};

export const dateRangeLabels = {
  start: t[store.state.core.currentLocale].start,
  end: t[store.state.core.currentLocale].end,
  preset: t[store.state.core.currentLocale].preset
};