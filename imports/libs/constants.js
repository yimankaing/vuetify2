const defaultRoles = [
  {label: 'Admin', value: 'admin'},
  {label: 'Read', value: 'read'},
  {label: 'Create', value: 'create'},
  {label: 'Edit', value: 'edit'},
  {label: 'Delete', value: 'delete'},
  {label: 'Report', value: 'report'},
  {label: 'Setting', value: 'setting'},
  {label: 'Export', value: 'export'},
];

export const lightColors = {
  // primary: '#3497db',
  // error: '#c0392b',
  success: '#27ae60',
  warning: '#f39c26',
  info: '#8e44ad',

  primary: "#6221ea",
  primaryVariant: "#3716b0",
  secondary: '#25d9c6',
  secondaryVariant:"#138785",
  background: "#ffffff",
  error: "#ae0725",
  accent: '#3497db',
};
export const darkColors = {
  // primary: '#3497db',
  // error: '#c0392b',
  success: '#27ae60',
  warning: '#f39c26',
  info: '#8e44ad',

  primary: "#ba8af9",
  primaryVariant: "#3716b0",
  secondary: '#25d9c6',
  secondaryVariant:"#138785",
  background: "#121212",
  error: "#cd677a",
  accent: '#3497db',
};

const Constants = {
  lang: [{label: "EN", value: "en"}, {label: "KM", value: "km"}],
  nameTitle: [
    {label: "Mr", value: "mr"},
    {label: "Ms", value: "ms"},
    {label: "Miss", value: "miss"},
  ],
  gender: [
    {label: "male", value: "male"},
    {label: "female", value: "female"}
  ],
  roleOptions: {
    app: [...defaultRoles],
    acc: [...defaultRoles],
    // acc: [...defaultRoles],
    // letter: [...defaultRoles],
    // school: [...defaultRoles],
  },
  // status: {
  //   pending: {label: 'Pending', value: 'pending'},
  //   approved: {label: 'Approved', value: 'approved'},
  //   processing: {label: 'Processing', value: 'processing'},
  //   finished: {label: 'Finished', value: 'finished'},
  // },
  moduleOptions: {
    app: {label: 'app', value: 'app'},
    acc: {label: 'acc', value: 'acc'},
  }
};
//lang
import {en} from "../libs/lang/en";
import {km} from "../libs/lang/km";

export const t = {en, km};

export default Constants;
