//http://kazupon.github.io/vue-i18n/en/
// Lib imports
import Vue from 'vue';
import VueI18n from 'vue-i18n';
//lang
import {en} from '../../imports/libs/lang/en';
import {km} from '../../imports/libs/lang/km';

export const messages = {en, km};

Vue.use(VueI18n);

const i18n = new VueI18n({
  locale: 'en',
  fallbackLocale: 'en',
  messages
});

export default i18n
