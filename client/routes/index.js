import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);
//components
import Auth from "../../imports/ui/auth/Auth";
import Home from "../../imports/ui/pages/Home";
import Setting from "../../imports/ui/pages/Setting/Setting";
import NotFound from "../../imports/ui/pages/NotFound";
import UnAuthenticate from "../../imports/ui/pages/UnAuthenticate";
import {SAM, SAM_REPORT} from './sam';

import {requiredUserFn} from "./requiredUserFn";

const routes = [
  {
    path: "/",
    name: "core_home",
    component: Home,
    meta: {
      pageTitle: "home",
    }
  },
  {
    path: "/core_setting",
    name: "core_setting",
    component: Setting,
    meta: {
      pageTitle: "settings",
    },
    beforeEnter: requiredUserFn,
  },
  // Not Found
  {
    path: "*",
    name: "notFound",
    component: NotFound,
    // redirect: '*',
    meta: {
      pageTitle: "notFoundPage",
    },
  },
  {
    path: "/core_permission",
    name: "core_permission",
    component: UnAuthenticate,
    meta: {
      pageTitle: " ",
      breadcrumb: {
        title: " ",
        ignore: true
      }
    },
  },
  // Auth
  {
    path: "/auth",
    name: "auth",
    component: Auth,
    meta: {
      notRequiresAuth: true,
      pageTitle: "login"
    },
  },
  ...SAM, ...SAM_REPORT
];

const router = new VueRouter({mode: "history", routes});

export default router;

