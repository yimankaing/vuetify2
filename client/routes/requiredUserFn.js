import store from "../../imports/vuex/store";
import {CheckRoles} from '../../imports/libs/checkRoles';

export const requiredUserFn = (to, from, next) => {
  if (store.state.currentUser) {
    if (!CheckRoles({roles: ['admin', 'super', 'read'], module: store.state.currentModule})) {
      return next("/core_permission");
    }
    return next();
  }
  return next("/");
};
