import {requiredUserFn} from "./requiredUserFn";
//pages
import Student from "../../imports/ui/pages/SAM/Student/Student";

export const SAM = [
  {
    path: "/sam_student",
    name: "sam_student",
    component: Student,
    meta: {
      pageTitle: "student",
    },
    beforeEnter: requiredUserFn,
  },
];
//reports
export const SAM_REPORT = [
  // {
  //   path: "/sam_studentStatusReport",
  //   name: "sam_studentStatusReport",
  //   component: StudentStatusReport,
  //   meta: {
  //     pageTitle: "studentStatus",
  //   },
  //   beforeEnter: requiredUserFn,
  // },
];