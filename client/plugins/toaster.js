import Vue from 'vue'
import Toaster from 'v-toaster'
import 'v-toaster/dist/v-toaster.css';
import '../../imports/ui/styles/toast.css';

Vue.use(Toaster, {timeout: 3500});