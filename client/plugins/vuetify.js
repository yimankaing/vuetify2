import Vue from 'vue';
import i18n from '../i18n';
import Vuetify from 'vuetify';

import 'vuetify/dist/vuetify.min.css';
import '@mdi/font/css/materialdesignicons.css';
import '../../imports/ui/styles/material-icons-outline.css';
import {lightColors, darkColors} from "../../imports/libs/constants";

//register

Vue.use(Vuetify);

export const vuetify = new Vuetify({
  theme: {
    dark: false,
    themes: {
      light: lightColors,
      dark: darkColors,
    }
  },
  icons: {
    iconfont: "md"
  }
});