import {Meteor} from 'meteor/meteor';
import Vue from 'vue';
//styles
import '../imports/ui/styles/report.css';
import '../imports/ui/styles/fonts.css';
//plugins
import i18n from './i18n';
import router from "./routes";
import store from "../imports/vuex/store";
import './plugins';
//
import {vuetify} from "./plugins/vuetify";
//components
import App from '../imports/ui/App';

Meteor.startup(() => {
  new Vue({
    vuetify,
    router,
    i18n,
    store,
    ...App
  }).$mount("#app");
});
